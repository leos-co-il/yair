-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 25, 2021 at 05:47 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yair`
--

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_actions`
--

CREATE TABLE `fmn_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_actions`
--

INSERT INTO `fmn_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(21, 'action_scheduler/migration_hook', 'complete', '2021-07-19 14:04:53', '2021-07-19 17:04:53', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1626703493;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1626703493;}', 1, 1, '2021-07-19 14:05:01', '2021-07-19 17:05:01', 0, NULL),
(22, 'wc-admin_import_customers', 'complete', '2021-07-21 07:35:14', '2021-07-21 10:35:14', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1626852914;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1626852914;}', 2, 1, '2021-07-21 07:36:12', '2021-07-21 10:36:12', 0, NULL),
(23, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 18:22:02', '2021-07-21 21:22:02', 0, NULL),
(24, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 18:23:05', '2021-07-21 21:23:05', 0, NULL),
(25, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 18:23:55', '2021-07-21 21:23:55', 0, NULL),
(26, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 18:25:33', '2021-07-21 21:25:33', 0, NULL),
(27, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 18:37:35', '2021-07-21 21:37:35', 0, NULL),
(28, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 18:48:50', '2021-07-21 21:48:50', 0, NULL),
(29, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 18:49:39', '2021-07-21 21:49:39', 0, NULL),
(30, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 18:51:10', '2021-07-21 21:51:10', 0, NULL),
(31, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:06:15', '2021-07-21 22:06:15', 0, NULL),
(32, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:07:21', '2021-07-21 22:07:21', 0, NULL),
(33, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:07:21', '2021-07-21 22:07:21', 0, NULL),
(34, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:08:29', '2021-07-21 22:08:29', 0, NULL),
(35, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:09:35', '2021-07-21 22:09:35', 0, NULL),
(36, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:09:35', '2021-07-21 22:09:35', 0, NULL),
(37, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:10:36', '2021-07-21 22:10:36', 0, NULL),
(38, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:11:37', '2021-07-21 22:11:37', 0, NULL),
(39, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-21 19:13:04', '2021-07-21 22:13:04', 0, NULL),
(40, 'wc-admin_import_customers', 'complete', '2021-07-22 05:56:11', '2021-07-22 08:56:11', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1626933371;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1626933371;}', 2, 1, '2021-07-22 05:56:41', '2021-07-22 08:56:41', 0, NULL),
(41, 'wc-admin_import_customers', 'complete', '2021-07-23 06:46:50', '2021-07-23 09:46:50', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1627022810;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1627022810;}', 2, 1, '2021-07-23 06:47:04', '2021-07-23 09:47:04', 0, NULL),
(42, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-07-23 18:19:51', '2021-07-23 21:19:51', 0, NULL),
(43, 'wc-admin_import_customers', 'complete', '2021-07-24 05:35:56', '2021-07-24 08:35:56', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1627104956;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1627104956;}', 2, 1, '2021-07-24 05:39:43', '2021-07-24 08:39:43', 0, NULL),
(44, 'wc-admin_import_customers', 'complete', '2021-07-25 06:33:05', '2021-07-25 09:33:05', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1627194785;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1627194785;}', 2, 1, '2021-07-25 06:33:10', '2021-07-25 09:33:10', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_claims`
--

CREATE TABLE `fmn_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_groups`
--

CREATE TABLE `fmn_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_groups`
--

INSERT INTO `fmn_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-data');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_logs`
--

CREATE TABLE `fmn_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_logs`
--

INSERT INTO `fmn_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 21, 'action created', '2021-07-19 14:03:53', '2021-07-19 17:03:53'),
(2, 21, 'action started via Async Request', '2021-07-19 14:05:01', '2021-07-19 17:05:01'),
(3, 21, 'action complete via Async Request', '2021-07-19 14:05:01', '2021-07-19 17:05:01'),
(4, 22, 'הפעולה נוצרה', '2021-07-21 07:35:09', '2021-07-21 10:35:09'),
(5, 22, 'הפעולה התחילה דרך WP Cron', '2021-07-21 07:36:12', '2021-07-21 10:36:12'),
(6, 22, 'הפעולה הושלמה דרך WP Cron', '2021-07-21 07:36:12', '2021-07-21 10:36:12'),
(7, 23, 'הפעולה נוצרה', '2021-07-21 18:21:10', '2021-07-21 21:21:10'),
(8, 23, 'הפעולה התחילה דרך WP Cron', '2021-07-21 18:22:01', '2021-07-21 21:22:01'),
(9, 23, 'הפעולה הושלמה דרך WP Cron', '2021-07-21 18:22:02', '2021-07-21 21:22:02'),
(10, 24, 'הפעולה נוצרה', '2021-07-21 18:22:57', '2021-07-21 21:22:57'),
(11, 24, 'הפעולה התחילה דרך Async Request', '2021-07-21 18:23:04', '2021-07-21 21:23:04'),
(12, 24, 'הפעולה הושלמה דרך Async Request', '2021-07-21 18:23:05', '2021-07-21 21:23:05'),
(13, 25, 'הפעולה נוצרה', '2021-07-21 18:23:41', '2021-07-21 21:23:41'),
(14, 25, 'הפעולה התחילה דרך WP Cron', '2021-07-21 18:23:54', '2021-07-21 21:23:54'),
(15, 25, 'הפעולה הושלמה דרך WP Cron', '2021-07-21 18:23:55', '2021-07-21 21:23:55'),
(16, 26, 'הפעולה נוצרה', '2021-07-21 18:24:29', '2021-07-21 21:24:29'),
(17, 26, 'הפעולה התחילה דרך WP Cron', '2021-07-21 18:25:33', '2021-07-21 21:25:33'),
(18, 26, 'הפעולה הושלמה דרך WP Cron', '2021-07-21 18:25:33', '2021-07-21 21:25:33'),
(19, 27, 'הפעולה נוצרה', '2021-07-21 18:36:31', '2021-07-21 21:36:31'),
(20, 27, 'הפעולה התחילה דרך WP Cron', '2021-07-21 18:37:34', '2021-07-21 21:37:34'),
(21, 27, 'הפעולה הושלמה דרך WP Cron', '2021-07-21 18:37:35', '2021-07-21 21:37:35'),
(22, 28, 'הפעולה נוצרה', '2021-07-21 18:48:46', '2021-07-21 21:48:46'),
(23, 28, 'הפעולה התחילה דרך WP Cron', '2021-07-21 18:48:50', '2021-07-21 21:48:50'),
(24, 28, 'הפעולה הושלמה דרך WP Cron', '2021-07-21 18:48:50', '2021-07-21 21:48:50'),
(25, 29, 'הפעולה נוצרה', '2021-07-21 18:49:27', '2021-07-21 21:49:27'),
(26, 29, 'הפעולה התחילה דרך Async Request', '2021-07-21 18:49:39', '2021-07-21 21:49:39'),
(27, 29, 'הפעולה הושלמה דרך Async Request', '2021-07-21 18:49:39', '2021-07-21 21:49:39'),
(28, 30, 'הפעולה נוצרה', '2021-07-21 18:50:07', '2021-07-21 21:50:07'),
(29, 30, 'הפעולה התחילה דרך WP Cron', '2021-07-21 18:51:10', '2021-07-21 21:51:10'),
(30, 30, 'הפעולה הושלמה דרך WP Cron', '2021-07-21 18:51:10', '2021-07-21 21:51:10'),
(31, 31, 'הפעולה נוצרה', '2021-07-21 19:06:14', '2021-07-21 22:06:14'),
(32, 31, 'הפעולה התחילה דרך Async Request', '2021-07-21 19:06:15', '2021-07-21 22:06:15'),
(33, 31, 'הפעולה הושלמה דרך Async Request', '2021-07-21 19:06:15', '2021-07-21 22:06:15'),
(34, 32, 'הפעולה נוצרה', '2021-07-21 19:06:51', '2021-07-21 22:06:51'),
(35, 33, 'הפעולה נוצרה', '2021-07-21 19:07:20', '2021-07-21 22:07:20'),
(36, 32, 'הפעולה התחילה דרך Async Request', '2021-07-21 19:07:20', '2021-07-21 22:07:20'),
(37, 32, 'הפעולה הושלמה דרך Async Request', '2021-07-21 19:07:21', '2021-07-21 22:07:21'),
(38, 33, 'הפעולה התחילה דרך Async Request', '2021-07-21 19:07:21', '2021-07-21 22:07:21'),
(39, 33, 'הפעולה הושלמה דרך Async Request', '2021-07-21 19:07:21', '2021-07-21 22:07:21'),
(40, 34, 'הפעולה נוצרה', '2021-07-21 19:08:28', '2021-07-21 22:08:28'),
(41, 34, 'הפעולה התחילה דרך Async Request', '2021-07-21 19:08:29', '2021-07-21 22:08:29'),
(42, 34, 'הפעולה הושלמה דרך Async Request', '2021-07-21 19:08:29', '2021-07-21 22:08:29'),
(43, 35, 'הפעולה נוצרה', '2021-07-21 19:09:06', '2021-07-21 22:09:06'),
(44, 36, 'הפעולה נוצרה', '2021-07-21 19:09:34', '2021-07-21 22:09:34'),
(45, 35, 'הפעולה התחילה דרך Async Request', '2021-07-21 19:09:35', '2021-07-21 22:09:35'),
(46, 35, 'הפעולה הושלמה דרך Async Request', '2021-07-21 19:09:35', '2021-07-21 22:09:35'),
(47, 36, 'הפעולה התחילה דרך Async Request', '2021-07-21 19:09:35', '2021-07-21 22:09:35'),
(48, 36, 'הפעולה הושלמה דרך Async Request', '2021-07-21 19:09:35', '2021-07-21 22:09:35'),
(49, 37, 'הפעולה נוצרה', '2021-07-21 19:10:11', '2021-07-21 22:10:11'),
(50, 37, 'הפעולה התחילה דרך Async Request', '2021-07-21 19:10:36', '2021-07-21 22:10:36'),
(51, 37, 'הפעולה הושלמה דרך Async Request', '2021-07-21 19:10:36', '2021-07-21 22:10:36'),
(52, 38, 'הפעולה נוצרה', '2021-07-21 19:11:15', '2021-07-21 22:11:15'),
(53, 38, 'הפעולה התחילה דרך Async Request', '2021-07-21 19:11:37', '2021-07-21 22:11:37'),
(54, 38, 'הפעולה הושלמה דרך Async Request', '2021-07-21 19:11:37', '2021-07-21 22:11:37'),
(55, 39, 'הפעולה נוצרה', '2021-07-21 19:12:00', '2021-07-21 22:12:00'),
(56, 39, 'הפעולה התחילה דרך WP Cron', '2021-07-21 19:13:04', '2021-07-21 22:13:04'),
(57, 39, 'הפעולה הושלמה דרך WP Cron', '2021-07-21 19:13:04', '2021-07-21 22:13:04'),
(58, 40, 'הפעולה נוצרה', '2021-07-22 05:56:06', '2021-07-22 08:56:06'),
(59, 40, 'הפעולה התחילה דרך Async Request', '2021-07-22 05:56:41', '2021-07-22 08:56:41'),
(60, 40, 'הפעולה הושלמה דרך Async Request', '2021-07-22 05:56:41', '2021-07-22 08:56:41'),
(61, 41, 'הפעולה נוצרה', '2021-07-23 06:46:45', '2021-07-23 09:46:45'),
(62, 41, 'הפעולה התחילה דרך WP Cron', '2021-07-23 06:47:04', '2021-07-23 09:47:04'),
(63, 41, 'הפעולה הושלמה דרך WP Cron', '2021-07-23 06:47:04', '2021-07-23 09:47:04'),
(64, 42, 'הפעולה נוצרה', '2021-07-23 18:19:35', '2021-07-23 21:19:35'),
(65, 42, 'הפעולה התחילה דרך WP Cron', '2021-07-23 18:19:51', '2021-07-23 21:19:51'),
(66, 42, 'הפעולה הושלמה דרך WP Cron', '2021-07-23 18:19:51', '2021-07-23 21:19:51'),
(67, 43, 'הפעולה נוצרה', '2021-07-24 05:35:51', '2021-07-24 08:35:51'),
(68, 43, 'הפעולה התחילה דרך WP Cron', '2021-07-24 05:39:43', '2021-07-24 08:39:43'),
(69, 43, 'הפעולה הושלמה דרך WP Cron', '2021-07-24 05:39:43', '2021-07-24 08:39:43'),
(70, 44, 'הפעולה נוצרה', '2021-07-25 06:33:00', '2021-07-25 09:33:00'),
(71, 44, 'הפעולה התחילה דרך Async Request', '2021-07-25 06:33:10', '2021-07-25 09:33:10'),
(72, 44, 'הפעולה הושלמה דרך Async Request', '2021-07-25 06:33:10', '2021-07-25 09:33:10');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_commentmeta`
--

CREATE TABLE `fmn_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_comments`
--

CREATE TABLE `fmn_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_comments`
--

INSERT INTO `fmn_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'מגיב וורדפרס', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-07-18 18:14:07', '2021-07-18 15:14:07', 'היי, זו תגובה.\nכדי לשנות, לערוך, או למחוק תגובות, יש לגשת למסך התגובות בלוח הבקרה.\nצלמית המשתמש של המגיב מגיעה מתוך <a href=\"https://gravatar.com\">גראווטר</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_links`
--

CREATE TABLE `fmn_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_options`
--

CREATE TABLE `fmn_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_options`
--

INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://yair:8888', 'yes'),
(2, 'home', 'http://yair:8888', 'yes'),
(3, 'blogname', 'yair', 'yes'),
(4, 'blogdescription', 'אתר וורדפרס חדש', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'maxf@leos.co.il', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j בF Y', 'yes'),
(24, 'time_format', 'G:i', 'yes'),
(25, 'links_updated_date_format', 'j בF Y G:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:171:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:47:\"(([^/]+/)*wishlist)(/(.*))?/page/([0-9]{1,})/?$\";s:76:\"index.php?pagename=$matches[1]&wishlist-action=$matches[4]&paged=$matches[5]\";s:30:\"(([^/]+/)*wishlist)(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&wishlist-action=$matches[4]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:47:\"artist/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?artist=$matches[1]&feed=$matches[2]\";s:42:\"artist/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?artist=$matches[1]&feed=$matches[2]\";s:23:\"artist/([^/]+)/embed/?$\";s:39:\"index.php?artist=$matches[1]&embed=true\";s:35:\"artist/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?artist=$matches[1]&paged=$matches[2]\";s:17:\"artist/([^/]+)/?$\";s:28:\"index.php?artist=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=8&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:27:\"woocommerce/woocommerce.php\";i:4;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'yair', 'yes'),
(41, 'stylesheet', 'yair', 'yes'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '49752', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Asia/Jerusalem', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '8', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1642173247', 'yes'),
(94, 'initial_db_version', '45805', 'yes'),
(95, 'fmn_user_roles', 'a:8:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:11:\"leos_client\";a:2:{s:4:\"name\";s:11:\"Leos Client\";s:12:\"capabilities\";a:124:{s:16:\"activate_plugins\";b:0;s:19:\"delete_others_pages\";b:1;s:19:\"delete_others_posts\";b:1;s:12:\"delete_pages\";b:1;s:12:\"delete_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:14:\"edit_dashboard\";b:1;s:17:\"edit_others_pages\";b:1;s:17:\"edit_others_posts\";b:1;s:10:\"edit_pages\";b:1;s:10:\"edit_posts\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_theme_options\";b:1;s:6:\"export\";b:0;s:6:\"import\";b:0;s:10:\"list_users\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:13:\"promote_users\";b:0;s:13:\"publish_pages\";b:1;s:13:\"publish_posts\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:4:\"read\";b:1;s:12:\"remove_users\";b:0;s:13:\"switch_themes\";b:0;s:12:\"upload_files\";b:1;s:11:\"update_core\";b:0;s:14:\"update_plugins\";b:0;s:13:\"update_themes\";b:0;s:15:\"install_plugins\";b:0;s:14:\"install_themes\";b:0;s:13:\"delete_themes\";b:0;s:14:\"delete_plugins\";b:0;s:12:\"edit_plugins\";b:0;s:11:\"edit_themes\";b:0;s:10:\"edit_files\";b:0;s:10:\"edit_users\";b:0;s:9:\"add_users\";b:0;s:12:\"create_users\";b:0;s:12:\"delete_users\";b:0;s:15:\"unfiltered_html\";b:1;s:18:\"manage_woocommerce\";b:1;s:25:\"manage_woocommerce_orders\";b:1;s:26:\"manage_woocommerce_coupons\";b:1;s:27:\"manage_woocommerce_products\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:10:\"copy_posts\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:17:\"edit_shop_webhook\";b:1;s:17:\"read_shop_webhook\";b:1;s:19:\"delete_shop_webhook\";b:1;s:18:\"edit_shop_webhooks\";b:1;s:25:\"edit_others_shop_webhooks\";b:1;s:21:\"publish_shop_webhooks\";b:1;s:26:\"read_private_shop_webhooks\";b:1;s:20:\"delete_shop_webhooks\";b:1;s:28:\"delete_private_shop_webhooks\";b:1;s:30:\"delete_published_shop_webhooks\";b:1;s:27:\"delete_others_shop_webhooks\";b:1;s:26:\"edit_private_shop_webhooks\";b:1;s:28:\"edit_published_shop_webhooks\";b:1;s:25:\"manage_shop_webhook_terms\";b:1;s:23:\"edit_shop_webhook_terms\";b:1;s:25:\"delete_shop_webhook_terms\";b:1;s:25:\"assign_shop_webhook_terms\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'he_IL', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:18:{i:1627224469;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1627225432;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1627225439;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1627226047;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1627226048;a:4:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1627226054;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1627226056;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1627226128;a:1:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1627226794;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1627232630;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1627242270;a:1:{s:34:\"yith_wcwl_delete_expired_wishlists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1627243430;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1627246800;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1627308232;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1627308240;a:2:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1627312528;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1627999490;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(123, '_site_transient_timeout_browser_619828cbc593f946769500632b389ffc', '1627226055', 'no'),
(124, '_site_transient_browser_619828cbc593f946769500632b389ffc', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"91.0.4472.101\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(125, '_site_transient_timeout_php_check_fb6df547cfb7d95cb9b49b8301cad3ab', '1627226056', 'no'),
(126, '_site_transient_php_check_fb6df547cfb7d95cb9b49b8301cad3ab', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(138, 'disallowed_keys', '', 'no'),
(139, 'comment_previously_approved', '1', 'yes'),
(140, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(141, 'auto_update_core_dev', 'enabled', 'yes'),
(142, 'auto_update_core_minor', 'enabled', 'yes'),
(143, 'auto_update_core_major', 'unset', 'yes'),
(144, 'finished_updating_comment_type', '1', 'yes'),
(145, 'db_upgraded', '', 'yes'),
(149, 'https_detection_errors', 'a:1:{s:20:\"https_request_failed\";a:1:{i:0;s:26:\"בקשת HTTPS נכשלה.\";}}', 'yes'),
(150, 'can_compress_scripts', '1', 'no'),
(151, 'recently_activated', 'a:0:{}', 'yes'),
(174, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.4.2\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1626679262;s:7:\"version\";s:5:\"5.4.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(175, 'acf_version', '5.9.9', 'yes'),
(176, 'theme_mods_twentytwenty', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1626679363;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(177, 'current_theme', 'Yair', 'yes'),
(178, 'theme_mods_twentytwentyone', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1626679365;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(179, 'theme_switched', '', 'yes'),
(181, 'theme_mods_yair', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:3:{s:11:\"footer-menu\";i:40;s:11:\"header-menu\";i:39;s:20:\"dropdown-header-menu\";i:41;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(194, 'action_scheduler_hybrid_store_demarkation', '20', 'yes'),
(195, 'schema-ActionScheduler_StoreSchema', '4.0.1626703429', 'yes'),
(196, 'schema-ActionScheduler_LoggerSchema', '2.0.1626703429', 'yes'),
(199, 'woocommerce_schema_version', '430', 'yes'),
(200, 'woocommerce_store_address', 'חיפה', 'yes'),
(201, 'woocommerce_store_address_2', '', 'yes'),
(202, 'woocommerce_store_city', 'חיפה', 'yes'),
(203, 'woocommerce_default_country', 'IL', 'yes'),
(204, 'woocommerce_store_postcode', '111', 'yes'),
(205, 'woocommerce_allowed_countries', 'all', 'yes'),
(206, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(207, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(208, 'woocommerce_ship_to_countries', '', 'yes'),
(209, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(210, 'woocommerce_default_customer_address', 'base', 'yes'),
(211, 'woocommerce_calc_taxes', 'no', 'yes'),
(212, 'woocommerce_enable_coupons', 'yes', 'yes'),
(213, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(214, 'woocommerce_currency', 'USD', 'yes'),
(215, 'woocommerce_currency_pos', 'left', 'yes'),
(216, 'woocommerce_price_thousand_sep', '', 'yes'),
(217, 'woocommerce_price_decimal_sep', '', 'yes'),
(218, 'woocommerce_price_num_decimals', '0', 'yes'),
(219, 'woocommerce_shop_page_id', '21', 'yes'),
(220, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(221, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(222, 'woocommerce_placeholder_image', '20', 'yes'),
(223, 'woocommerce_weight_unit', 'kg', 'yes'),
(224, 'woocommerce_dimension_unit', 'cm', 'yes'),
(225, 'woocommerce_enable_reviews', 'yes', 'yes'),
(226, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(227, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(228, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(229, 'woocommerce_review_rating_required', 'yes', 'no'),
(230, 'woocommerce_manage_stock', 'yes', 'yes'),
(231, 'woocommerce_hold_stock_minutes', '60', 'no'),
(232, 'woocommerce_notify_low_stock', 'yes', 'no'),
(233, 'woocommerce_notify_no_stock', 'yes', 'no'),
(234, 'woocommerce_stock_email_recipient', 'maxf@leos.co.il', 'no'),
(235, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(236, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(237, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(238, 'woocommerce_stock_format', '', 'yes'),
(239, 'woocommerce_file_download_method', 'force', 'no'),
(240, 'woocommerce_downloads_require_login', 'no', 'no'),
(241, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(242, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(243, 'woocommerce_prices_include_tax', 'no', 'yes'),
(244, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(245, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(246, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(247, 'woocommerce_tax_classes', '', 'yes'),
(248, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(249, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(250, 'woocommerce_price_display_suffix', '', 'yes'),
(251, 'woocommerce_tax_total_display', 'itemized', 'no'),
(252, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(253, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(254, 'woocommerce_ship_to_destination', 'billing', 'no'),
(255, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(256, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(257, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(258, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(259, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(260, 'woocommerce_registration_generate_username', 'yes', 'no'),
(261, 'woocommerce_registration_generate_password', 'yes', 'no'),
(262, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(263, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(264, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(265, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(266, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(267, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(268, 'woocommerce_trash_pending_orders', '', 'no'),
(269, 'woocommerce_trash_failed_orders', '', 'no'),
(270, 'woocommerce_trash_cancelled_orders', '', 'no'),
(271, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(272, 'woocommerce_email_from_name', 'yair', 'no'),
(273, 'woocommerce_email_from_address', 'maxf@leos.co.il', 'no'),
(274, 'woocommerce_email_header_image', '', 'no'),
(275, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(276, 'woocommerce_email_base_color', '#96588a', 'no'),
(277, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(278, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(279, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(280, 'woocommerce_merchant_email_notifications', 'no', 'no'),
(281, 'woocommerce_cart_page_id', '22', 'no'),
(282, 'woocommerce_checkout_page_id', '23', 'no'),
(283, 'woocommerce_myaccount_page_id', '24', 'no'),
(284, 'woocommerce_terms_page_id', '', 'no'),
(285, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(286, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(287, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(288, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(289, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(290, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(291, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(292, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(293, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(294, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(295, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(296, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(297, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(298, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(299, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(300, 'woocommerce_api_enabled', 'no', 'yes'),
(301, 'woocommerce_allow_tracking', 'no', 'no'),
(302, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(303, 'woocommerce_single_image_width', '600', 'yes'),
(304, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(305, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(306, 'woocommerce_demo_store', 'no', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(307, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(308, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(309, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(310, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(312, 'default_product_cat', '15', 'yes'),
(315, 'woocommerce_paypal_settings', 'a:23:{s:7:\"enabled\";s:2:\"no\";s:5:\"title\";s:6:\"PayPal\";s:11:\"description\";s:85:\"Pay via PayPal; you can pay with your credit card if you don\'t have a PayPal account.\";s:5:\"email\";s:15:\"maxf@leos.co.il\";s:8:\"advanced\";s:0:\"\";s:8:\"testmode\";s:2:\"no\";s:5:\"debug\";s:2:\"no\";s:16:\"ipn_notification\";s:3:\"yes\";s:14:\"receiver_email\";s:15:\"maxf@leos.co.il\";s:14:\"identity_token\";s:0:\"\";s:14:\"invoice_prefix\";s:3:\"WC-\";s:13:\"send_shipping\";s:3:\"yes\";s:16:\"address_override\";s:2:\"no\";s:13:\"paymentaction\";s:4:\"sale\";s:9:\"image_url\";s:0:\"\";s:11:\"api_details\";s:0:\"\";s:12:\"api_username\";s:0:\"\";s:12:\"api_password\";s:0:\"\";s:13:\"api_signature\";s:0:\"\";s:20:\"sandbox_api_username\";s:0:\"\";s:20:\"sandbox_api_password\";s:0:\"\";s:21:\"sandbox_api_signature\";s:0:\"\";s:12:\"_should_load\";s:2:\"no\";}', 'yes'),
(316, 'woocommerce_version', '5.5.1', 'yes'),
(317, 'woocommerce_db_version', '5.5.1', 'yes'),
(318, 'woocommerce_inbox_variant_assignment', '2', 'yes'),
(322, '_transient_jetpack_autoloader_plugin_paths', 'a:1:{i:0;s:29:\"{{WP_PLUGIN_DIR}}/woocommerce\";}', 'yes'),
(323, 'action_scheduler_lock_async-request-runner', '1627224460', 'yes'),
(324, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(325, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"REPFX4ScytGC4J9M2kzdV0nh0uYMUGQj\";}', 'yes'),
(326, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(327, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(328, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(329, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(330, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(331, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(332, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(333, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(334, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(335, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(336, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(337, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(338, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(341, 'woocommerce_admin_version', '2.4.1', 'yes'),
(342, 'woocommerce_admin_install_timestamp', '1626703432', 'yes'),
(343, 'wc_remote_inbox_notifications_wca_updated', '', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(344, 'wc_remote_inbox_notifications_specs', 'a:33:{s:16:\"wayflyer_q3_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:16:\"wayflyer_q3_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:55:\"Grow your revenue with Wayflyer financing and analytics\";s:7:\"content\";s:322:\"Flexible financing tailored to your needs by <a href=\"https://woocommerce.com/products/wayflyer/\">Wayflyer</a> – one fee, no interest rates, penalties, equity, or personal guarantees. Based on your store\'s performance, Wayflyer can provide the financing you need to grow and the analytical insights to help you spend it.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:16:\"wayflyer_q3_2021\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Get funded\";}}s:3:\"url\";s:42:\"https://woocommerce.com/products/wayflyer/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:5:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-07-19 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-07-31 00:00:00\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:13:\"product_count\";s:9:\"operation\";s:2:\"<=\";s:5:\"value\";s:3:\"200\";}i:3;O:8:\"stdClass\":3:{s:4:\"type\";s:11:\"order_count\";s:9:\"operation\";s:1:\">\";s:5:\"value\";s:3:\"100\";}i:4;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:7:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"AU\";}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"BE\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"CA\";}i:3;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"IE\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"NL\";}i:5;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"UK\";}i:6;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"US\";}}}}}s:19:\"eu_vat_changes_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:19:\"eu_vat_changes_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:54:\"Get your business ready for the new EU tax regulations\";s:7:\"content\";s:617:\"On July 1, 2021, new taxation rules will come into play when the <a href=\"https://ec.europa.eu/taxation_customs/business/vat/modernising-vat-cross-border-ecommerce_en\">European Union (EU) Value-Added Tax (VAT) eCommerce package</a> takes effect.<br/><br/>The new regulations will impact virtually every B2C business involved in cross-border eCommerce trade with the EU.<br/><br/>We therefore recommend <a href=\"https://woocommerce.com/posts/new-eu-vat-regulations\">familiarizing yourself with the new updates</a>, and consult with a tax professional to ensure your business is following regulations and best practice.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:19:\"eu_vat_changes_2021\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:39:\"Learn more about the EU tax regulations\";}}s:3:\"url\";s:52:\"https://woocommerce.com/posts/new-eu-vat-regulations\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-06-24 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-07-11 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:3:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:3:\"all\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;a:2:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:10:\"all_except\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:27:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"BE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"BG\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"CZ\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"DK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"DE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"EE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:6;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"IE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"EL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:8;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"ES\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:9;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"FR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:10;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"HR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:11;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"IT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:12;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"CY\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:13;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LV\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:14;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:15;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:16;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"HU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:17;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"MT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:18;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"NL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:19;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"AT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:20;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"PL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:21;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"PT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:22;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"RO\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:23;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:24;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:25;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"FI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:26;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}}}}i:2;a:3:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:3:\"all\";s:7:\"default\";b:0;s:9:\"operation\";s:2:\"!=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:10:\"all_except\";s:7:\"default\";b:0;s:9:\"operation\";s:2:\"!=\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:27:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"BE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"BG\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"CZ\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"DK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"DE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"EE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:6;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"IE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"EL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:8;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"ES\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:9;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"FR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:10;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"HR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:11;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"IT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:12;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"CY\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:13;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LV\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:14;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:15;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:16;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"HU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:17;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"MT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:18;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"NL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:19;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"AT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:20;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"PL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:21;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"PT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:22;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"RO\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:23;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:24;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:25;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"FI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:26;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}}}}}}}}s:20:\"paypal_ppcp_gtm_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:20:\"paypal_ppcp_gtm_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:38:\"Offer more options with the new PayPal\";s:7:\"content\";s:113:\"Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:36:\"open_wc_paypal_payments_product_page\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:61:\"https://woocommerce.com/products/woocommerce-paypal-payments/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-04-05 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-04-21 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:7:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:30:\"woocommerce-gateway-paypal-pro\";}}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:37:\"woocommerce-gateway-paypal-pro-hosted\";}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:35:\"woocommerce-gateway-paypal-advanced\";}}i:4;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:40:\"woocommerce-gateway-paypal-digital-goods\";}}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:31:\"woocommerce-paypal-here-gateway\";}}i:6;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:44:\"woocommerce-gateway-paypal-adaptive-payments\";}}}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:27:\"woocommerce-paypal-payments\";s:7:\"version\";s:5:\"1.2.1\";s:8:\"operator\";s:1:\"<\";}}}}}s:23:\"facebook_pixel_api_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:23:\"facebook_pixel_api_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:44:\"Improve the performance of your Facebook ads\";s:7:\"content\";s:152:\"Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:30:\"upgrade_now_facebook_pixel_api\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:11:\"Upgrade now\";}}s:3:\"url\";s:67:\"plugin-install.php?tab=plugin-information&plugin=&section=changelog\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-05-17 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-06-14 00:00:00\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:24:\"facebook-for-woocommerce\";s:7:\"version\";s:5:\"2.4.0\";s:8:\"operator\";s:2:\"<=\";}}}s:16:\"facebook_ec_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:16:\"facebook_ec_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:59:\"Sync your product catalog with Facebook to help boost sales\";s:7:\"content\";s:170:\"A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:22:\"learn_more_facebook_ec\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:42:\"https://woocommerce.com/products/facebook/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-03-01 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-03-15 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:24:\"facebook-for-woocommerce\";}}}}s:37:\"ecomm-need-help-setting-up-your-store\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"ecomm-need-help-setting-up-your-store\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:32:\"Need help setting up your Store?\";s:7:\"content\";s:350:\"Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:16:\"set-up-concierge\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:21:\"Schedule free session\";}}s:3:\"url\";s:34:\"https://wordpress.com/me/concierge\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:3:{i:0;s:35:\"woocommerce-shipping-australia-post\";i:1;s:32:\"woocommerce-shipping-canada-post\";i:2;s:30:\"woocommerce-shipping-royalmail\";}}}}s:20:\"woocommerce-services\";O:8:\"stdClass\":8:{s:4:\"slug\";s:20:\"woocommerce-services\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"WooCommerce Shipping & Tax\";s:7:\"content\";s:255:\"WooCommerce Shipping & Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:84:\"https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-services\";}}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\"<\";s:4:\"days\";i:2;}}}s:32:\"ecomm-unique-shopping-experience\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"ecomm-unique-shopping-experience\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:53:\"For a shopping experience as unique as your customers\";s:7:\"content\";s:274:\"Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:43:\"learn-more-ecomm-unique-shopping-experience\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:71:\"https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:3:{i:0;s:35:\"woocommerce-shipping-australia-post\";i:1;s:32:\"woocommerce-shipping-canada-post\";i:2;s:30:\"woocommerce-shipping-royalmail\";}}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\"<\";s:4:\"days\";i:2;}}}s:37:\"wc-admin-getting-started-in-ecommerce\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"wc-admin-getting-started-in-ecommerce\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:38:\"Getting Started in eCommerce - webinar\";s:7:\"content\";s:174:\"We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:17:\"watch-the-webinar\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:17:\"Watch the webinar\";}}s:3:\"url\";s:28:\"https://youtu.be/V_2XtCOyZ7o\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:12:\"setup_client\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:1;}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:3:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:13:\"product_count\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:1:\"0\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:7:\"revenue\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:4:\"none\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:7:\"revenue\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:10:\"up-to-2500\";}}}}}s:18:\"your-first-product\";O:8:\"stdClass\":8:{s:4:\"slug\";s:18:\"your-first-product\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:18:\"Your first product\";s:7:\"content\";s:461:\"That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br/><br/>Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:82:\"https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:12:\"stored_state\";s:5:\"index\";s:22:\"there_were_no_products\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";b:1;}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:12:\"stored_state\";s:5:\"index\";s:22:\"there_are_now_products\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";b:1;}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:13:\"product_count\";s:9:\"operation\";s:2:\">=\";s:5:\"value\";i:1;}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:13:\"product_types\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:8:\"physical\";s:7:\"default\";a:0:{}}}}s:31:\"wc-square-apple-pay-boost-sales\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"wc-square-apple-pay-boost-sales\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"Boost sales with Apple Pay\";s:7:\"content\";s:191:\"Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:27:\"boost-sales-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:97:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:9:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:18:\"woocommerce-square\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"2.3\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"wc_square_apple_pay_enabled\";s:5:\"value\";i:1;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:38:\"wc-square-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:38:\"wc-square-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:38:\"wc-square-apple-pay-grow-your-business\";O:8:\"stdClass\":8:{s:4:\"slug\";s:38:\"wc-square-apple-pay-grow-your-business\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:45:\"Grow your business with Square and Apple Pay \";s:7:\"content\";s:178:\"Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"grow-your-business-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:104:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:9:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:18:\"woocommerce-square\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"2.3\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"wc_square_apple_pay_enabled\";s:5:\"value\";i:2;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:31:\"wc-square-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:31:\"wc-square-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:32:\"wcpay-apple-pay-is-now-available\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"wcpay-apple-pay-is-now-available\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:53:\"Apple Pay is now available with WooCommerce Payments!\";s:7:\"content\";s:397:\"Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:13:\"add-apple-pay\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:13:\"Add Apple Pay\";}}s:3:\"url\";s:69:\"/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:121:\"https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:20:\"woocommerce-payments\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:5:\"2.3.0\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";b:0;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}s:27:\"wcpay-apple-pay-boost-sales\";O:8:\"stdClass\":8:{s:4:\"slug\";s:27:\"wcpay-apple-pay-boost-sales\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"Boost sales with Apple Pay\";s:7:\"content\";s:205:\"Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:27:\"boost-sales-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:96:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";i:1;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:34:\"wcpay-apple-pay-grow-your-business\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"wcpay-apple-pay-grow-your-business\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:58:\"Grow your business with WooCommerce Payments and Apple Pay\";s:7:\"content\";s:178:\"Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"grow-your-business-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:103:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";i:2;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:37:\"wc-admin-optimizing-the-checkout-flow\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"wc-admin-optimizing-the-checkout-flow\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:28:\"Optimizing the checkout flow\";s:7:\"content\";s:171:\"It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:28:\"optimizing-the-checkout-flow\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:78:\"https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\">\";s:4:\"days\";i:3;}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:8:\"payments\";s:7:\"default\";a:0:{}}}}s:39:\"wc-admin-first-five-things-to-customize\";O:8:\"stdClass\":8:{s:4:\"slug\";s:39:\"wc-admin-first-five-things-to-customize\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:45:\"The first 5 things to customize in your store\";s:7:\"content\";s:173:\"Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:82:\"https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\">\";s:4:\"days\";i:2;}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:5:\"value\";s:9:\"NOT EMPTY\";s:7:\"default\";s:9:\"NOT EMPTY\";s:9:\"operation\";s:2:\"!=\";}}}s:32:\"wc-payments-qualitative-feedback\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"wc-payments-qualitative-feedback\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:55:\"WooCommerce Payments setup - let us know what you think\";s:7:\"content\";s:146:\"Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:35:\"qualitative-feedback-from-new-users\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:14:\"Share feedback\";}}s:3:\"url\";s:39:\"https://automattic.survey.fm/wc-pay-new\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:20:\"woocommerce-payments\";s:7:\"default\";a:0:{}}}}s:29:\"share-your-feedback-on-paypal\";O:8:\"stdClass\":8:{s:4:\"slug\";s:29:\"share-your-feedback-on-paypal\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:29:\"Share your feedback on PayPal\";s:7:\"content\";s:127:\"Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:14:\"share-feedback\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:14:\"Share feedback\";}}s:3:\"url\";s:43:\"http://automattic.survey.fm/paypal-feedback\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:26:\"woocommerce-gateway-stripe\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}}}s:31:\"wcpay_instant_deposits_gtm_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"wcpay_instant_deposits_gtm_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:69:\"Get paid within minutes – Instant Deposits for WooCommerce Payments\";s:7:\"content\";s:384:\"Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:40:\"Learn about Instant Deposits eligibility\";}}s:3:\"url\";s:136:\"https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-05-18 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-06-01 00:00:00\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}s:31:\"google_listings_and_ads_install\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"google_listings_and_ads_install\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:35:\"Drive traffic and sales with Google\";s:7:\"content\";s:123:\"Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:11:\"get-started\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:11:\"Get started\";}}s:3:\"url\";s:56:\"https://woocommerce.com/products/google-listings-and-ads\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-06-09 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:23:\"google_listings_and_ads\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:11:\"order_count\";s:9:\"operation\";s:1:\">\";s:5:\"value\";i:10;}}}s:39:\"wc-subscriptions-security-update-3-0-15\";O:8:\"stdClass\":8:{s:4:\"slug\";s:39:\"wc-subscriptions-security-update-3-0-15\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:42:\"WooCommerce Subscriptions security update!\";s:7:\"content\";s:736:\"We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br/><br/>Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br/><br/>We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br/><br/>If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:30:\"update-wc-subscriptions-3-0-15\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"View latest version\";}}s:3:\"url\";s:30:\"&page=wc-addons&section=helper\";s:18:\"url_is_admin_query\";b:1;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:25:\"woocommerce-subscriptions\";s:8:\"operator\";s:1:\"<\";s:7:\"version\";s:6:\"3.0.15\";}}}s:29:\"woocommerce-core-update-5-4-0\";O:8:\"stdClass\":8:{s:4:\"slug\";s:29:\"woocommerce-core-update-5-4-0\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:31:\"Update to WooCommerce 5.4.1 now\";s:7:\"content\";s:140:\"WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:20:\"update-wc-core-5-4-0\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:25:\"How to update WooCommerce\";}}s:3:\"url\";s:64:\"https://docs.woocommerce.com/document/how-to-update-woocommerce/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.4.0\";}}}s:19:\"wcpay-promo-2020-11\";O:8:\"stdClass\":7:{s:4:\"slug\";s:19:\"wcpay-promo-2020-11\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:19:\"wcpay-promo-2020-11\";s:7:\"content\";s:19:\"wcpay-promo-2020-11\";}}s:5:\"rules\";a:0:{}}s:19:\"wcpay-promo-2020-12\";O:8:\"stdClass\":7:{s:4:\"slug\";s:19:\"wcpay-promo-2020-12\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:19:\"wcpay-promo-2020-12\";s:7:\"content\";s:19:\"wcpay-promo-2020-12\";}}s:5:\"rules\";a:0:{}}s:30:\"wcpay-promo-2021-6-incentive-1\";O:8:\"stdClass\":8:{s:4:\"slug\";s:30:\"wcpay-promo-2021-6-incentive-1\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:82:\"Simplify the payments process for you and your customers with WooCommerce Payments\";s:7:\"content\";s:704:\"With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&utm_source=product&utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies. \n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br/><br/>\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&utm_source=product&utm_campaign=wcpay601\">Terms of Service</a> \n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:25:\"get-woo-commerce-payments\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:24:\"Get WooCommerce Payments\";}}s:3:\"url\";s:57:\"admin.php?page=wc-admin&action=setup-woocommerce-payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:12:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:6:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"1\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"3\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"5\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"7\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"9\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"11\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:4:{i:0;s:17:\"crowdsignal-forms\";i:1;s:11:\"layout-grid\";i:2;s:17:\"full-site-editing\";i:3;s:13:\"page-optimize\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"woocommerce_allow_tracking\";s:5:\"value\";s:3:\"yes\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:2:\">=\";s:4:\"days\";i:31;}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.0\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:10:\"db_version\";s:5:\"value\";s:5:\"45805\";s:7:\"default\";i:0;s:9:\"operation\";s:2:\">=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:30:\"wcpay-promo-2021-6-incentive-2\";O:8:\"stdClass\":8:{s:4:\"slug\";s:30:\"wcpay-promo-2021-6-incentive-2\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:82:\"Simplify the payments process for you and your customers with WooCommerce Payments\";s:7:\"content\";s:704:\"With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&utm_source=product&utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies. \n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br/><br/>\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&utm_source=product&utm_campaign=wcpay601\">Terms of Service</a> \n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:24:\"get-woocommerce-payments\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:24:\"Get WooCommerce Payments\";}}s:3:\"url\";s:57:\"admin.php?page=wc-admin&action=setup-woocommerce-payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:12:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:6:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"2\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"4\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"6\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"8\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"10\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"12\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:4:{i:0;s:17:\"crowdsignal-forms\";i:1;s:11:\"layout-grid\";i:2;s:17:\"full-site-editing\";i:3;s:13:\"page-optimize\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"woocommerce_allow_tracking\";s:5:\"value\";s:3:\"yes\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:2:\">=\";s:4:\"days\";i:31;}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.0\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:10:\"db_version\";s:5:\"value\";s:5:\"45805\";s:7:\"default\";i:0;s:9:\"operation\";s:2:\">=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:34:\"ppxo-pps-upgrade-paypal-payments-1\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"ppxo-pps-upgrade-paypal-payments-1\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:47:\"Get the latest PayPal extension for WooCommerce\";s:7:\"content\";s:440:\"Heads up! There\'s a new PayPal on the block!<br/><br/>Now is a great time to upgrade to our latest <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension</a> to continue to receive support and updates with PayPal.<br/><br/>Get access to a full suite of PayPal payment methods, extensive currency and country coverage, and pay later options with the all-new PayPal extension for WooCommerce.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"ppxo-pps-install-paypal-payments-1\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:18:\"View upgrade guide\";}}s:3:\"url\";s:96:\"https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"woocommerce_paypal_settings\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:0;s:7:\"default\";b:0;}}}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";i:7;s:7:\"default\";i:1;s:9:\"operation\";s:1:\"<\";}}}s:34:\"ppxo-pps-upgrade-paypal-payments-2\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"ppxo-pps-upgrade-paypal-payments-2\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:31:\"Upgrade your PayPal experience!\";s:7:\"content\";s:513:\"We\'ve developed a whole new <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension for WooCommerce</a> that combines the best features of our many PayPal extensions into just one extension.<br/><br/>Get access to a full suite of PayPal payment methods, extensive currency and country coverage, offer subscription and recurring payments, and the new PayPal pay later options.<br/><br/>Start using our latest PayPal today to continue to receive support and updates.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"ppxo-pps-install-paypal-payments-2\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:18:\"View upgrade guide\";}}s:3:\"url\";s:96:\"https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"woocommerce_paypal_settings\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:0;s:7:\"default\";b:0;}}}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";i:6;s:7:\"default\";i:1;s:9:\"operation\";s:1:\">\";}}}s:46:\"woocommerce-core-sqli-july-2021-need-to-update\";O:8:\"stdClass\":8:{s:4:\"slug\";s:46:\"woocommerce-core-sqli-july-2021-need-to-update\";s:4:\"type\";s:6:\"update\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:56:\"Action required: Critical vulnerabilities in WooCommerce\";s:7:\"content\";s:570:\"In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br/><br/>Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br/><br/>For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:146:\"https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:7:\"dismiss\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:7:\"Dismiss\";}}s:3:\"url\";b:0;s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:0;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:23:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.3.6\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.4.8\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.5.9\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.6.6\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.7.2\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.8.2\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.9.4\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.0.2\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.1.2\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.2.3\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.3.4\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.4.2\";}i:12;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.5.3\";}i:13;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.6.3\";}i:14;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.7.2\";}i:15;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.8.1\";}i:16;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.9.3\";}i:17;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.0.1\";}i:18;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.1.1\";}i:19;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.2.3\";}i:20;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.3.1\";}i:21;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.4.2\";}i:22;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"<\";s:7:\"version\";s:5:\"5.5.1\";}}}s:48:\"woocommerce-blocks-sqli-july-2021-need-to-update\";O:8:\"stdClass\":8:{s:4:\"slug\";s:48:\"woocommerce-blocks-sqli-july-2021-need-to-update\";s:4:\"type\";s:6:\"update\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:63:\"Action required: Critical vulnerabilities in WooCommerce Blocks\";s:7:\"content\";s:570:\"In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br/><br/>Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br/><br/>For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:146:\"https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:7:\"dismiss\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:7:\"Dismiss\";}}s:3:\"url\";b:0;s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:0;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:31:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:6:\"2.5.16\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"2.6.2\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"2.7.2\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"2.8.1\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"2.9.1\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.0.1\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.1.1\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.2.1\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.3.1\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.4.1\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.5.1\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.6.1\";}i:12;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.7.2\";}i:13;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.8.1\";}i:14;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.9.1\";}i:15;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.0.1\";}i:16;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.1.1\";}i:17;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.2.1\";}i:18;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.3.1\";}i:19;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.4.3\";}i:20;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.5.3\";}i:21;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.6.1\";}i:22;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.7.1\";}i:23;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.8.1\";}i:24;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.9.2\";}i:25;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.0.1\";}i:26;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.1.1\";}i:27;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.2.1\";}i:28;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.3.2\";}i:29;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.4.1\";}i:30;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"<\";s:7:\"version\";s:5:\"5.5.1\";}}}s:45:\"woocommerce-core-sqli-july-2021-store-patched\";O:8:\"stdClass\":8:{s:4:\"slug\";s:45:\"woocommerce-core-sqli-july-2021-store-patched\";s:4:\"type\";s:6:\"update\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:55:\"Solved: Critical vulnerabilities patched in WooCommerce\";s:7:\"content\";s:433:\"In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br/><br/><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:146:\"https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:7:\"dismiss\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:7:\"Dismiss\";}}s:3:\"url\";b:0;s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:0;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:46:\"woocommerce-core-sqli-july-2021-need-to-update\";s:6:\"status\";s:7:\"pending\";s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:48:\"woocommerce-blocks-sqli-july-2021-need-to-update\";s:6:\"status\";s:7:\"pending\";s:9:\"operation\";s:1:\"=\";}}}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:23:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.3.6\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.4.8\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.5.9\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.6.6\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.7.2\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.8.2\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.9.4\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.0.2\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.1.2\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.2.3\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.3.4\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.4.2\";}i:12;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.5.3\";}i:13;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.6.3\";}i:14;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.7.2\";}i:15;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.8.1\";}i:16;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.9.3\";}i:17;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.0.1\";}i:18;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.1.1\";}i:19;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.2.3\";}i:20;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.3.1\";}i:21;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.4.2\";}i:22;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:5:\"5.5.1\";}}}}}s:47:\"woocommerce-blocks-sqli-july-2021-store-patched\";O:8:\"stdClass\":8:{s:4:\"slug\";s:47:\"woocommerce-blocks-sqli-july-2021-store-patched\";s:4:\"type\";s:6:\"update\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:62:\"Solved: Critical vulnerabilities patched in WooCommerce Blocks\";s:7:\"content\";s:433:\"In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br/><br/><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:146:\"https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:7:\"dismiss\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:7:\"Dismiss\";}}s:3:\"url\";b:0;s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:0;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:46:\"woocommerce-core-sqli-july-2021-need-to-update\";s:6:\"status\";s:7:\"pending\";s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:48:\"woocommerce-blocks-sqli-july-2021-need-to-update\";s:6:\"status\";s:7:\"pending\";s:9:\"operation\";s:1:\"=\";}}}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:31:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:6:\"2.5.16\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"2.6.2\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"2.7.2\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"2.8.1\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"2.9.1\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.0.1\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.1.1\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.2.1\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.3.1\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.4.1\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.5.1\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.6.1\";}i:12;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.7.2\";}i:13;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.8.1\";}i:14;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.9.1\";}i:15;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.0.1\";}i:16;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.1.1\";}i:17;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.2.1\";}i:18;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.3.1\";}i:19;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.4.3\";}i:20;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.5.3\";}i:21;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.6.1\";}i:22;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.7.1\";}i:23;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.8.1\";}i:24;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.9.2\";}i:25;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.0.1\";}i:26;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.1.1\";}i:27;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.2.1\";}i:28;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.3.2\";}i:29;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.4.1\";}i:30;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:5:\"5.5.1\";}}}}}}', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(345, 'wc_remote_inbox_notifications_stored_state', 'O:8:\"stdClass\":2:{s:22:\"there_were_no_products\";b:1;s:22:\"there_are_now_products\";b:1;}', 'no'),
(349, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(350, 'wc_blocks_db_schema_version', '260', 'yes'),
(351, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(355, '_transient_woocommerce_reports-transient-version', '1626703463', 'yes'),
(356, '_transient_timeout_orders-all-statuses', '1627308366', 'no'),
(357, '_transient_orders-all-statuses', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";a:0:{}}', 'no'),
(366, 'woocommerce_onboarding_profile', 'a:8:{s:12:\"setup_client\";b:1;s:8:\"industry\";a:1:{i:0;a:1:{s:4:\"slug\";s:21:\"home-furniture-garden\";}}s:13:\"product_types\";a:1:{i:0;s:8:\"physical\";}s:13:\"product_count\";s:8:\"101-1000\";s:14:\"selling_venues\";s:2:\"no\";s:19:\"business_extensions\";a:0:{}s:5:\"theme\";s:4:\"yair\";s:9:\"completed\";b:1;}', 'yes'),
(367, 'action_scheduler_migration_status', 'complete', 'yes'),
(369, '_transient_timeout_wc_report_orders_stats_dfcbe5a0e755fc65d01509030944d824', '1627308368', 'no'),
(370, '_transient_wc_report_orders_stats_dfcbe5a0e755fc65d01509030944d824', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-18 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-19 17:06:06\";s:12:\"date_end_gmt\";s:19:\"2021-07-19 14:06:06\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(371, '_transient_timeout_wc_report_orders_stats_d923d900f9f2f6a77a3d11d0dd877ea2', '1627308368', 'no'),
(372, '_transient_wc_report_orders_stats_d923d900f9f2f6a77a3d11d0dd877ea2', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-18 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-19 17:06:06\";s:12:\"date_end_gmt\";s:19:\"2021-07-19 14:06:06\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(373, 'woocommerce_task_list_tracked_completed_tasks', 'a:2:{i:0;s:13:\"store_details\";i:1;s:8:\"products\";}', 'yes'),
(374, '_transient_timeout_wc_report_orders_stats_749409e159eb9bca36e57c57db4c868c', '1627308368', 'no'),
(375, '_transient_wc_report_orders_stats_749409e159eb9bca36e57c57db4c868c', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-18 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-18 00:00:00\";s:8:\"date_end\";s:19:\"2021-07-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-18 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(376, '_transient_timeout_wc_report_orders_stats_de817568fea04b2fcde1c487ff6788c2', '1627308368', 'no'),
(377, '_transient_wc_report_orders_stats_de817568fea04b2fcde1c487ff6788c2', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-18 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-18 00:00:00\";s:8:\"date_end\";s:19:\"2021-07-18 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-18 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(378, 'woocommerce_task_list_welcome_modal_dismissed', 'yes', 'yes'),
(381, '_transient_timeout_wc_report_orders_stats_1f60298f77f14c4f3a66ffab40bd0b9f', '1627308496', 'no'),
(382, '_transient_wc_report_orders_stats_1f60298f77f14c4f3a66ffab40bd0b9f', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-18 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-19 17:08:14\";s:12:\"date_end_gmt\";s:19:\"2021-07-19 14:08:14\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(383, '_transient_timeout_wc_report_orders_stats_b101fb2481465441be343bfc791eddc2', '1627308496', 'no'),
(384, '_transient_wc_report_orders_stats_b101fb2481465441be343bfc791eddc2', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-19 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-18 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-19 17:08:14\";s:12:\"date_end_gmt\";s:19:\"2021-07-19 14:08:14\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(387, 'acf_pro_license', 'YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TmpRNE5USjhkSGx3WlQxd1pYSnpiMjVoYkh4a1lYUmxQVEl3TVRVdE1Ea3RNak1nTURVNk1qQTZNVFE9IjtzOjM6InVybCI7czoxNjoiaHR0cDovL3lhaXI6ODg4OCI7fQ==', 'yes'),
(401, '_transient_product_query-transient-version', '1627113680', 'yes'),
(413, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":7,\"critical\":1}', 'yes'),
(439, 'widget_block', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(442, 'wp_force_deactivated_plugins', 'a:0:{}', 'yes'),
(449, '_transient_timeout_wc_report_orders_stats_7dc2c3cf528e9b34e3d35e4017666859', '1627464217', 'no'),
(450, '_transient_wc_report_orders_stats_7dc2c3cf528e9b34e3d35e4017666859', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-20 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-21 12:23:35\";s:12:\"date_end_gmt\";s:19:\"2021-07-21 09:23:35\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(451, '_transient_timeout_wc_report_orders_stats_1a9ca3769fb474692711c5f4b1f854b3', '1627464217', 'no'),
(452, '_transient_wc_report_orders_stats_1a9ca3769fb474692711c5f4b1f854b3', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-21 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-20 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-21 12:23:35\";s:12:\"date_end_gmt\";s:19:\"2021-07-21 09:23:35\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(453, '_transient_timeout_wc_report_orders_stats_bc00c72450ba0d322306d6db23d22ba5', '1627464217', 'no'),
(454, '_transient_wc_report_orders_stats_bc00c72450ba0d322306d6db23d22ba5', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-19 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-20 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(455, '_transient_timeout_wc_report_orders_stats_961a3a67af95792b922958f77afd8742', '1627464217', 'no'),
(456, '_transient_wc_report_orders_stats_961a3a67af95792b922958f77afd8742', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-20 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-19 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-20 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-20 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(500, 'category_children', 'a:0:{}', 'yes'),
(525, 'options_tel', '050-9797-508', 'no'),
(526, '_options_tel', 'field_5dd3a33f42e0c', 'no'),
(527, 'options_mail', 'wallsyleil@gmail.com ', 'no'),
(528, '_options_mail', 'field_5dd3a35642e0d', 'no'),
(529, 'options_fax', '', 'no'),
(530, '_options_fax', 'field_5dd3a36942e0e', 'no'),
(531, 'options_address', '', 'no'),
(532, '_options_address', 'field_5dd3a4d142e0f', 'no'),
(533, 'options_map_image', '', 'no'),
(534, '_options_map_image', 'field_5ddbd67734310', 'no'),
(535, 'options_facebook', 'https://www.facebook.com/facebook', 'no'),
(536, '_options_facebook', 'field_5ddbd6b3cc0cd', 'no'),
(537, 'options_whatsapp', '050-9797-508', 'no'),
(538, '_options_whatsapp', 'field_5ddbd6cdcc0ce', 'no'),
(539, 'options_foo_title', 'עדכונים ומבצעים ', 'no'),
(540, '_options_foo_title', 'field_60f82f372e440', 'no'),
(541, 'options_foo_subtitle', 'השאירו פרטים ותקבלו עדכונים למייל ', 'no'),
(542, '_options_foo_subtitle', 'field_60f82f3f2e441', 'no'),
(543, 'options_process_block_title', 'התהליך איתנו', 'no'),
(544, '_options_process_block_title', 'field_60f82f6a2e443', 'no'),
(545, 'options_process_item', '4', 'no'),
(546, '_options_process_item', 'field_60f82f782e444', 'no'),
(547, 'options_reviews_block_title', 'לקוחות ממליצים', 'no'),
(548, '_options_reviews_block_title', 'field_60f830132e448', 'no'),
(549, 'options_review_item_0_review_name', ' שם הממליץ לורם היפסום 1', 'no'),
(550, '_options_review_item_0_review_name', 'field_60f830502e44a', 'no'),
(551, 'options_review_item_0_review_text', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר נפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת קולורס מונפרד אדנדום סילקוף\r\n\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר נפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת קולורס מונפרד אדנדום סילקוף', 'no'),
(552, '_options_review_item_0_review_text', 'field_60f830692e44b', 'no'),
(553, 'options_review_item_1_review_name', ' שם הממליץ לורם היפסום 2', 'no'),
(554, '_options_review_item_1_review_name', 'field_60f830502e44a', 'no'),
(555, 'options_review_item_1_review_text', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nצש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'no'),
(556, '_options_review_item_1_review_text', 'field_60f830692e44b', 'no'),
(557, 'options_review_item', '2', 'no'),
(558, '_options_review_item', 'field_60f830272e449', 'no'),
(559, 'options_logo', '161', 'no'),
(560, '_options_logo', 'field_5dd3a310760b9', 'no'),
(631, 'artist_children', 'a:0:{}', 'yes'),
(633, '_transient_product-transient-version', '1627064375', 'yes'),
(750, 'options_instagram', 'https://www.instagram.com/', 'no'),
(751, '_options_instagram', 'field_60f831a9129f3', 'no'),
(752, 'options_process_item_0_process_icon', '162', 'no'),
(753, '_options_process_item_0_process_icon', 'field_60f82f8f2e445', 'no'),
(754, 'options_process_item_0_process_title', 'בחירת יצירה ', 'no'),
(755, '_options_process_item_0_process_title', 'field_60f82fa32e446', 'no'),
(756, 'options_process_item_1_process_icon', '163', 'no'),
(757, '_options_process_item_1_process_icon', 'field_60f82f8f2e445', 'no'),
(758, 'options_process_item_1_process_title', 'בחירת גודל', 'no'),
(759, '_options_process_item_1_process_title', 'field_60f82fa32e446', 'no'),
(760, 'options_process_item_2_process_icon', '164', 'no'),
(761, '_options_process_item_2_process_icon', 'field_60f82f8f2e445', 'no'),
(762, 'options_process_item_2_process_title', 'אישור הזמנה', 'no'),
(763, '_options_process_item_2_process_title', 'field_60f82fa32e446', 'no'),
(764, 'options_process_item_3_process_icon', '165', 'no'),
(765, '_options_process_item_3_process_icon', 'field_60f82f8f2e445', 'no'),
(766, 'options_process_item_3_process_title', ' תיאום הדפסה', 'no'),
(767, '_options_process_item_3_process_title', 'field_60f82fa32e446', 'no'),
(1026, 'product_cat_children', 'a:0:{}', 'yes'),
(1137, 'yit_recently_activated', 'a:1:{i:0;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(1138, 'yith_wcwl_wishlist_page_id', '171', 'yes'),
(1139, 'yith_wcwl_version', '3.0.23', 'yes'),
(1140, 'yith_wcwl_db_version', '3.0.0', 'yes'),
(1141, 'yith_wcwl_ajax_enable', 'no', 'yes'),
(1142, 'yith_wfbt_enable_integration', 'yes', 'yes'),
(1143, 'yith_wcwl_after_add_to_wishlist_behaviour', 'view', 'yes'),
(1144, 'yith_wcwl_show_on_loop', 'no', 'yes'),
(1145, 'yith_wcwl_loop_position', 'after_add_to_cart', 'yes'),
(1146, 'yith_wcwl_button_position', 'thumbnails', 'yes'),
(1147, 'yith_wcwl_add_to_wishlist_text', '', 'yes'),
(1148, 'yith_wcwl_product_added_text', '', 'yes'),
(1149, 'yith_wcwl_browse_wishlist_text', '', 'yes'),
(1150, 'yith_wcwl_already_in_wishlist_text', '', 'yes'),
(1151, 'yith_wcwl_add_to_wishlist_style', 'link', 'yes'),
(1152, 'yith_wcwl_rounded_corners_radius', '16', 'yes'),
(1153, 'yith_wcwl_add_to_wishlist_icon', 'fa-heart-o', 'yes'),
(1154, 'yith_wcwl_add_to_wishlist_custom_icon', '', 'yes'),
(1155, 'yith_wcwl_added_to_wishlist_icon', 'fa-heart', 'yes'),
(1156, 'yith_wcwl_added_to_wishlist_custom_icon', '', 'yes'),
(1157, 'yith_wcwl_custom_css', '', 'yes'),
(1158, 'yith_wcwl_variation_show', '', 'yes'),
(1159, 'yith_wcwl_price_show', 'yes', 'yes'),
(1160, 'yith_wcwl_stock_show', 'yes', 'yes'),
(1161, 'yith_wcwl_show_dateadded', '', 'yes'),
(1162, 'yith_wcwl_add_to_cart_show', 'yes', 'yes'),
(1163, 'yith_wcwl_show_remove', 'yes', 'yes'),
(1164, 'yith_wcwl_repeat_remove_button', '', 'yes'),
(1165, 'yith_wcwl_redirect_cart', 'no', 'yes'),
(1166, 'yith_wcwl_remove_after_add_to_cart', 'yes', 'yes'),
(1167, 'yith_wcwl_enable_share', 'yes', 'yes'),
(1168, 'yith_wcwl_share_fb', 'yes', 'yes'),
(1169, 'yith_wcwl_share_twitter', 'yes', 'yes'),
(1170, 'yith_wcwl_share_pinterest', 'yes', 'yes'),
(1171, 'yith_wcwl_share_email', 'yes', 'yes'),
(1172, 'yith_wcwl_share_whatsapp', 'yes', 'yes'),
(1173, 'yith_wcwl_share_url', 'no', 'yes'),
(1174, 'yith_wcwl_socials_title', 'רשימת המשאלות שלי ב-yair', 'yes'),
(1175, 'yith_wcwl_socials_text', '', 'yes'),
(1176, 'yith_wcwl_socials_image_url', '', 'yes'),
(1177, 'yith_wcwl_wishlist_title', 'My wishlist', 'yes'),
(1178, 'yith_wcwl_add_to_cart_text', 'Add to cart', 'yes'),
(1179, 'yith_wcwl_add_to_cart_style', 'link', 'yes'),
(1180, 'yith_wcwl_add_to_cart_rounded_corners_radius', '16', 'yes'),
(1181, 'yith_wcwl_add_to_cart_icon', 'fa-shopping-cart', 'yes'),
(1182, 'yith_wcwl_add_to_cart_custom_icon', '', 'yes'),
(1183, 'yith_wcwl_color_headers_background', '#F4F4F4', 'yes'),
(1184, 'yith_wcwl_fb_button_icon', 'fa-facebook', 'yes'),
(1185, 'yith_wcwl_fb_button_custom_icon', '', 'yes'),
(1186, 'yith_wcwl_tw_button_icon', 'fa-twitter', 'yes'),
(1187, 'yith_wcwl_tw_button_custom_icon', '', 'yes'),
(1188, 'yith_wcwl_pr_button_icon', 'fa-pinterest', 'yes'),
(1189, 'yith_wcwl_pr_button_custom_icon', '', 'yes'),
(1190, 'yith_wcwl_em_button_icon', 'fa-envelope-o', 'yes'),
(1191, 'yith_wcwl_em_button_custom_icon', '', 'yes'),
(1192, 'yith_wcwl_wa_button_icon', 'fa-whatsapp', 'yes'),
(1193, 'yith_wcwl_wa_button_custom_icon', '', 'yes'),
(1194, 'yit_plugin_fw_panel_wc_default_options_set', 'a:1:{s:15:\"yith_wcwl_panel\";b:1;}', 'yes'),
(1195, 'yith_plugin_fw_promo_2019_bis', '1', 'yes'),
(1196, '_transient_timeout_yith_wcwl_hidden_products', '1629575010', 'no'),
(1197, '_transient_yith_wcwl_hidden_products', 'a:0:{}', 'no'),
(1198, '_site_transient_timeout_yith_promo_message', '3254063220', 'no'),
(1199, '_site_transient_yith_promo_message', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2019-12-10</expiry_date>\n    <promo>\n        <promo_id>yithblackfriday2019</promo_id>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>28th November</strong> to <strong>2nd December</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n            <border_color>#272121</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-11-27 23:59:59</start_date>\n        <end_date>2019-12-03 08:00:00</end_date>\n    </promo>\n</promotions>', 'no'),
(1204, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(1228, 'yith_wcwl_color_add_to_wishlist', 'a:6:{s:10:\"background\";s:7:\"#333333\";s:4:\"text\";s:7:\"#FFFFFF\";s:6:\"border\";s:7:\"#333333\";s:16:\"background_hover\";s:7:\"#333333\";s:10:\"text_hover\";s:7:\"#FFFFFF\";s:12:\"border_hover\";s:7:\"#333333\";}', 'yes'),
(1259, 'options_search_title', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית', 'no'),
(1260, '_options_search_title', 'field_60fa7504d591b', 'no'),
(1282, '_transient_timeout_wc_addons_sections', '1627633899', 'no'),
(1283, '_transient_wc_addons_sections', 'a:9:{i:0;O:8:\"stdClass\":2:{s:4:\"slug\";s:9:\"_featured\";s:5:\"label\";s:8:\"Featured\";}i:1;O:8:\"stdClass\":2:{s:4:\"slug\";s:4:\"_all\";s:5:\"label\";s:3:\"All\";}i:2;O:8:\"stdClass\":2:{s:4:\"slug\";s:18:\"product-extensions\";s:5:\"label\";s:12:\"Enhancements\";}i:3;O:8:\"stdClass\":2:{s:4:\"slug\";s:15:\"free-extensions\";s:5:\"label\";s:4:\"Free\";}i:4;O:8:\"stdClass\":2:{s:4:\"slug\";s:20:\"marketing-extensions\";s:5:\"label\";s:9:\"Marketing\";}i:5;O:8:\"stdClass\":2:{s:4:\"slug\";s:16:\"payment-gateways\";s:5:\"label\";s:8:\"Payments\";}i:6;O:8:\"stdClass\":2:{s:4:\"slug\";s:12:\"product-type\";s:5:\"label\";s:12:\"Product Type\";}i:7;O:8:\"stdClass\":2:{s:4:\"slug\";s:16:\"shipping-methods\";s:5:\"label\";s:8:\"Shipping\";}i:8;O:8:\"stdClass\":2:{s:4:\"slug\";s:10:\"operations\";s:5:\"label\";s:16:\"Store Management\";}}', 'no'),
(1291, '_transient_timeout_wc_report_orders_stats_9b3e88ce64691d158361f1bb29e8e40b', '1627633949', 'no'),
(1292, '_transient_wc_report_orders_stats_9b3e88ce64691d158361f1bb29e8e40b', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-22 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-23 11:32:26\";s:12:\"date_end_gmt\";s:19:\"2021-07-23 08:32:26\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1293, '_transient_timeout_wc_report_orders_stats_91ccb456682d2a8ac9d123d4318e2bf3', '1627633949', 'no'),
(1294, '_transient_wc_report_orders_stats_91ccb456682d2a8ac9d123d4318e2bf3', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-22 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-23 11:32:26\";s:12:\"date_end_gmt\";s:19:\"2021-07-23 08:32:26\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1295, '_transient_timeout_wc_report_orders_stats_00ec04dff64701b7a0cce8be21bd9df1', '1627633949', 'no'),
(1296, '_transient_wc_report_orders_stats_00ec04dff64701b7a0cce8be21bd9df1', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-21 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-22 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1297, '_transient_timeout_wc_report_orders_stats_c2d5a8ee238acd984f67bbc6149873dd', '1627633949', 'no'),
(1298, '_transient_wc_report_orders_stats_c2d5a8ee238acd984f67bbc6149873dd', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-22 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-21 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-22 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-22 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1321, '_transient_timeout_wc_report_orders_stats_a83273570f103ab4aab99be55cfef084', '1627635439', 'no'),
(1322, '_transient_wc_report_orders_stats_a83273570f103ab4aab99be55cfef084', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-22 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-23 11:57:17\";s:12:\"date_end_gmt\";s:19:\"2021-07-23 08:57:17\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1323, '_transient_timeout_wc_report_orders_stats_c345de5d8bb4143613b6d1e276c16c2e', '1627635439', 'no'),
(1324, '_transient_wc_report_orders_stats_c345de5d8bb4143613b6d1e276c16c2e', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-22 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-23 11:57:17\";s:12:\"date_end_gmt\";s:19:\"2021-07-23 08:57:17\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1325, '_transient_shipping-transient-version', '1627030667', 'yes'),
(1326, '_transient_timeout_wc_shipping_method_count', '1629622667', 'no'),
(1327, '_transient_wc_shipping_method_count', 'a:2:{s:7:\"version\";s:10:\"1627030667\";s:5:\"value\";i:0;}', 'no'),
(1329, '_transient_timeout_wc_report_orders_stats_591b253d141b8c1ef570c9b42f3e9791', '1627635499', 'no'),
(1330, '_transient_wc_report_orders_stats_591b253d141b8c1ef570c9b42f3e9791', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-22 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-23 11:58:16\";s:12:\"date_end_gmt\";s:19:\"2021-07-23 08:58:16\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1331, '_transient_timeout_wc_report_orders_stats_537f153a73dd00ed3d51f00b60644e8e', '1627635499', 'no'),
(1332, '_transient_wc_report_orders_stats_537f153a73dd00ed3d51f00b60644e8e', 'a:2:{s:7:\"version\";s:10:\"1626703463\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-30\";s:10:\"date_start\";s:19:\"2021-07-23 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-22 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-23 11:58:16\";s:12:\"date_end_gmt\";s:19:\"2021-07-23 08:58:16\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1522, '_transient_timeout_wc_term_counts', '1629744146', 'no'),
(1523, '_transient_wc_term_counts', 'a:13:{i:16;s:1:\"3\";i:18;s:1:\"3\";i:20;s:1:\"3\";i:19;s:1:\"1\";i:17;s:1:\"1\";i:21;s:1:\"5\";i:26;s:1:\"4\";i:27;s:1:\"5\";i:24;s:1:\"6\";i:23;s:1:\"2\";i:15;s:0:\"\";i:22;s:1:\"1\";i:25;s:1:\"2\";}', 'no'),
(1568, '_transient_timeout_wc_upgrade_notice_5.5.2', '1627153739', 'no'),
(1569, '_transient_wc_upgrade_notice_5.5.2', '', 'no'),
(1584, '_transient_timeout_wc_shipping_method_count_legacy', '1629664011', 'no'),
(1585, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1627030667\";s:5:\"value\";i:0;}', 'no'),
(1588, '_transient_timeout_acf_plugin_updates', '1627277753', 'no'),
(1589, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.9\";}}', 'no'),
(1607, '_transient_timeout__woocommerce_helper_updates', '1627195230', 'no'),
(1608, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1627152030;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(1639, '_transient_timeout_wc_related_125', '1627240805', 'no'),
(1640, '_transient_wc_related_125', 'a:1:{s:51:\"limit=4&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=125\";a:7:{i:0;s:3:\"138\";i:1;s:3:\"143\";i:2;s:3:\"150\";i:3;s:3:\"152\";i:4;s:3:\"137\";i:5;s:3:\"139\";i:6;s:3:\"153\";}}', 'no'),
(1645, '_site_transient_timeout_theme_roots', '1627196289', 'no'),
(1646, '_site_transient_theme_roots', 'a:2:{s:15:\"twentytwentyone\";s:7:\"/themes\";s:4:\"yair\";s:7:\"/themes\";}', 'no'),
(1649, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.8.zip\";s:6:\"locale\";s:5:\"he_IL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.8.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.8-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.8-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:3:\"5.8\";s:7:\"version\";s:3:\"5.8\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1627194492;s:15:\"version_checked\";s:3:\"5.8\";s:12:\"translations\";a:0:{}}', 'no'),
(1650, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1627194492;s:7:\"checked\";a:2:{s:15:\"twentytwentyone\";s:3:\"1.3\";s:4:\"yair\";s:3:\"1.0\";}s:8:\"response\";a:1:{s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.4.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(1651, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1627194493;s:8:\"response\";a:1:{s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"5.5.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.5.5.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2366418\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2366418\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2366418\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2366418\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.5\";s:6:\"tested\";s:5:\"5.7.2\";s:12:\"requires_php\";s:3:\"7.0\";}}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2021-02-05 18:15:06\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.4.2/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.5.1\";s:7:\"updated\";s:19:\"2021-07-18 17:53:47\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/5.5.1/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:4:\"16.7\";s:7:\"updated\";s:19:\"2021-03-22 15:34:46\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/16.7/he_IL.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:7:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:5:\"1.6.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.4.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.4.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.5\";}s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:32:\"w.org/plugins/contact-form-cfdb7\";s:4:\"slug\";s:18:\"contact-form-cfdb7\";s:6:\"plugin\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:11:\"new_version\";s:7:\"1.2.5.9\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/contact-form-cfdb7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-cfdb7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-256x256.png?rev=1619878\";s:2:\"1x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-128x128.png?rev=1619878\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/contact-form-cfdb7/assets/banner-772x250.png?rev=1619902\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.8\";}s:24:\"fancy-gallery/plugin.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:27:\"w.org/plugins/fancy-gallery\";s:4:\"slug\";s:13:\"fancy-gallery\";s:6:\"plugin\";s:24:\"fancy-gallery/plugin.php\";s:11:\"new_version\";s:6:\"1.6.56\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/fancy-gallery/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/fancy-gallery.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/fancy-gallery/assets/icon-128x128.png?rev=1723946\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/fancy-gallery/assets/banner-772x250.png?rev=1723946\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.0\";}s:33:\"jquery-updater/jquery-updater.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/jquery-updater\";s:4:\"slug\";s:14:\"jquery-updater\";s:6:\"plugin\";s:33:\"jquery-updater/jquery-updater.php\";s:11:\"new_version\";s:7:\"3.6.0.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/jquery-updater/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/jquery-updater.3.6.0.1.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:58:\"https://s.w.org/plugins/geopattern-icon/jquery-updater.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.6\";}s:34:\"yith-woocommerce-wishlist/init.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:39:\"w.org/plugins/yith-woocommerce-wishlist\";s:4:\"slug\";s:25:\"yith-woocommerce-wishlist\";s:6:\"plugin\";s:34:\"yith-woocommerce-wishlist/init.php\";s:11:\"new_version\";s:6:\"3.0.23\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/yith-woocommerce-wishlist/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/yith-woocommerce-wishlist.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:78:\"https://ps.w.org/yith-woocommerce-wishlist/assets/icon-128x128.jpg?rev=2215573\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-1544x500.jpg?rev=2209192\";s:2:\"1x\";s:80:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-772x250.jpg?rev=2209192\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.3\";}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"16.7\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.16.7.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:8:\"requires\";s:3:\"5.6\";}}s:7:\"checked\";a:14:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.9\";s:33:\"classic-editor/classic-editor.php\";s:5:\"1.6.2\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.4.2\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:7:\"1.2.5.9\";s:24:\"fancy-gallery/plugin.php\";s:6:\"1.6.56\";s:33:\"jquery-updater/jquery-updater.php\";s:7:\"3.6.0.1\";s:29:\"versionpress/versionpress.php\";s:9:\"4.0-beta2\";s:17:\"wesafe/wesafe.php\";s:5:\"1.7.2\";s:27:\"woocommerce/woocommerce.php\";s:5:\"5.5.1\";s:25:\"dummy-data/dummy-data.php\";s:3:\"1.0\";s:29:\"wp-sync-db-1.5/wp-sync-db.php\";s:3:\"1.5\";s:34:\"yith-woocommerce-wishlist/init.php\";s:6:\"3.0.23\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"16.7\";s:17:\"leosem/leosem.php\";s:3:\"2.0\";}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_postmeta`
--

CREATE TABLE `fmn_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_postmeta`
--

INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(9, 6, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-12\">\n			[text* text-532 placeholder \"שם:\"]\n	</div>\n    <div class=\"col-12\">\n			[email* email-611 placeholder \"מייל:\"]\n	</div>\n    <div class=\"col-12\">\n			[submit \"שלחו אלי דיוור\"]\n	</div>\n	    <div class=\"col-12 check-col\">\n				[checkbox* checkbox-242 use_label_element \"אישור קבלת דיוור למייל לורם\"]\n	</div>\n</div>'),
(10, 6, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@yair>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(11, 6, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@yair>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(12, 6, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(13, 6, '_additional_settings', ''),
(14, 6, '_locale', 'he_IL'),
(16, 7, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-md-6 col-12\">\n		[text* text-824 placeholder \"שם:\"]\n	</div>\n    <div class=\"col-md-6 col-12\">\n			[tel* tel-556 placeholder \"טלפון:\"]\n	</div>\n	    <div class=\"col-12\">\n				[email email-36 placeholder \"מייל:\"]\n	</div>\n	    <div class=\"col-12\">\n				[textarea textarea-551 placeholder \"תוכן הפניה:\"]\n	</div>\n    <div class=\"col-md-auto col-12\">\n			[submit \"חזרו אלי בהקדם\"]\n	</div>\n</div>'),
(17, 7, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@yair>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(18, 7, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@yair>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(19, 7, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(20, 7, '_additional_settings', ''),
(21, 7, '_locale', 'he_IL'),
(25, 8, '_edit_last', '1'),
(26, 8, '_edit_lock', '1627221430:1'),
(27, 8, '_wp_page_template', 'views/home.php'),
(28, 8, 'title_tag', ''),
(29, 8, '_title_tag', 'field_5ddbe7577e0e7'),
(30, 8, 'single_slider_seo', '2'),
(31, 8, '_single_slider_seo', 'field_5ddbde5499115'),
(32, 8, 'single_gallery', ''),
(33, 8, '_single_gallery', 'field_5ddbe5aea4275'),
(34, 8, 'single_video_slider', ''),
(35, 8, '_single_video_slider', 'field_5ddbe636a4276'),
(36, 9, 'title_tag', ''),
(37, 9, '_title_tag', 'field_5ddbe7577e0e7'),
(38, 9, 'single_slider_seo', ''),
(39, 9, '_single_slider_seo', 'field_5ddbde5499115'),
(40, 9, 'single_gallery', ''),
(41, 9, '_single_gallery', 'field_5ddbe5aea4275'),
(42, 9, 'single_video_slider', ''),
(43, 9, '_single_video_slider', 'field_5ddbe636a4276'),
(44, 10, '_edit_last', '1'),
(45, 10, '_edit_lock', '1627057188:1'),
(46, 10, '_wp_page_template', 'views/contact.php'),
(47, 10, 'title_tag', ''),
(48, 10, '_title_tag', 'field_5ddbe7577e0e7'),
(49, 10, 'single_slider_seo', ''),
(50, 10, '_single_slider_seo', 'field_5ddbde5499115'),
(51, 10, 'single_gallery', ''),
(52, 10, '_single_gallery', 'field_5ddbe5aea4275'),
(53, 10, 'single_video_slider', ''),
(54, 10, '_single_video_slider', 'field_5ddbe636a4276'),
(55, 11, 'title_tag', ''),
(56, 11, '_title_tag', 'field_5ddbe7577e0e7'),
(57, 11, 'single_slider_seo', ''),
(58, 11, '_single_slider_seo', 'field_5ddbde5499115'),
(59, 11, 'single_gallery', ''),
(60, 11, '_single_gallery', 'field_5ddbe5aea4275'),
(61, 11, 'single_video_slider', ''),
(62, 11, '_single_video_slider', 'field_5ddbe636a4276'),
(63, 12, '_edit_last', '1'),
(64, 12, '_edit_lock', '1627222826:1'),
(65, 12, '_wp_page_template', 'views/gallery.php'),
(66, 12, 'title_tag', ''),
(67, 12, '_title_tag', 'field_5ddbe7577e0e7'),
(68, 12, 'single_slider_seo', ''),
(69, 12, '_single_slider_seo', 'field_5ddbde5499115'),
(70, 12, 'single_gallery', 'a:24:{i:0;s:3:\"219\";i:1;s:3:\"220\";i:2;s:3:\"221\";i:3;s:3:\"222\";i:4;s:3:\"223\";i:5;s:3:\"224\";i:6;s:3:\"225\";i:7;s:3:\"226\";i:8;s:3:\"227\";i:9;s:3:\"228\";i:10;s:3:\"229\";i:11;s:3:\"230\";i:12;s:3:\"148\";i:13;s:3:\"147\";i:14;s:3:\"159\";i:15;s:3:\"158\";i:16;s:3:\"149\";i:17;s:3:\"157\";i:18;s:3:\"133\";i:19;s:3:\"132\";i:20;s:3:\"131\";i:21;s:3:\"103\";i:22;s:3:\"108\";i:23;s:3:\"106\";}'),
(71, 12, '_single_gallery', 'field_5ddbe5aea4275'),
(72, 12, 'single_video_slider', ''),
(73, 12, '_single_video_slider', 'field_5ddbe636a4276'),
(74, 13, 'title_tag', ''),
(75, 13, '_title_tag', 'field_5ddbe7577e0e7'),
(76, 13, 'single_slider_seo', ''),
(77, 13, '_single_slider_seo', 'field_5ddbde5499115'),
(78, 13, 'single_gallery', ''),
(79, 13, '_single_gallery', 'field_5ddbe5aea4275'),
(80, 13, 'single_video_slider', ''),
(81, 13, '_single_video_slider', 'field_5ddbe636a4276'),
(82, 14, '_edit_last', '1'),
(83, 14, '_edit_lock', '1627036071:1'),
(84, 14, '_wp_page_template', 'views/offer.php'),
(85, 14, 'title_tag', ''),
(86, 14, '_title_tag', 'field_5ddbe7577e0e7'),
(87, 14, 'single_slider_seo', ''),
(88, 14, '_single_slider_seo', 'field_5ddbde5499115'),
(89, 14, 'single_gallery', ''),
(90, 14, '_single_gallery', 'field_5ddbe5aea4275'),
(91, 14, 'single_video_slider', ''),
(92, 14, '_single_video_slider', 'field_5ddbe636a4276'),
(93, 15, 'title_tag', ''),
(94, 15, '_title_tag', 'field_5ddbe7577e0e7'),
(95, 15, 'single_slider_seo', ''),
(96, 15, '_single_slider_seo', 'field_5ddbde5499115'),
(97, 15, 'single_gallery', ''),
(98, 15, '_single_gallery', 'field_5ddbe5aea4275'),
(99, 15, 'single_video_slider', ''),
(100, 15, '_single_video_slider', 'field_5ddbe636a4276'),
(101, 16, '_edit_last', '1'),
(102, 16, '_edit_lock', '1626971959:1'),
(103, 16, '_wp_page_template', 'views/categories.php'),
(104, 16, 'title_tag', ''),
(105, 16, '_title_tag', 'field_5ddbe7577e0e7'),
(106, 16, 'single_slider_seo', ''),
(107, 16, '_single_slider_seo', 'field_5ddbde5499115'),
(108, 16, 'single_gallery', ''),
(109, 16, '_single_gallery', 'field_5ddbe5aea4275'),
(110, 16, 'single_video_slider', ''),
(111, 16, '_single_video_slider', 'field_5ddbe636a4276'),
(112, 17, 'title_tag', ''),
(113, 17, '_title_tag', 'field_5ddbe7577e0e7'),
(114, 17, 'single_slider_seo', ''),
(115, 17, '_single_slider_seo', 'field_5ddbde5499115'),
(116, 17, 'single_gallery', ''),
(117, 17, '_single_gallery', 'field_5ddbe5aea4275'),
(118, 17, 'single_video_slider', ''),
(119, 17, '_single_video_slider', 'field_5ddbe636a4276'),
(120, 18, '_edit_last', '1'),
(121, 18, '_edit_lock', '1626703078:1'),
(122, 18, '_wp_page_template', 'views/artists.php'),
(123, 18, 'title_tag', ''),
(124, 18, '_title_tag', 'field_5ddbe7577e0e7'),
(125, 18, 'single_slider_seo', ''),
(126, 18, '_single_slider_seo', 'field_5ddbde5499115'),
(127, 18, 'single_gallery', ''),
(128, 18, '_single_gallery', 'field_5ddbe5aea4275'),
(129, 18, 'single_video_slider', ''),
(130, 18, '_single_video_slider', 'field_5ddbe636a4276'),
(131, 19, 'title_tag', ''),
(132, 19, '_title_tag', 'field_5ddbe7577e0e7'),
(133, 19, 'single_slider_seo', ''),
(134, 19, '_single_slider_seo', 'field_5ddbde5499115'),
(135, 19, 'single_gallery', ''),
(136, 19, '_single_gallery', 'field_5ddbe5aea4275'),
(137, 19, 'single_video_slider', ''),
(138, 19, '_single_video_slider', 'field_5ddbe636a4276'),
(139, 20, '_wp_attached_file', 'woocommerce-placeholder.png'),
(140, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:33:\"woocommerce-placeholder-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(141, 21, '_edit_lock', '1627030488:1'),
(142, 21, '_edit_last', '1'),
(143, 21, 'title_tag', ''),
(144, 21, '_title_tag', 'field_5ddbe7577e0e7'),
(145, 21, 'single_slider_seo', ''),
(146, 21, '_single_slider_seo', 'field_5ddbde5499115'),
(147, 21, 'single_gallery', ''),
(148, 21, '_single_gallery', 'field_5ddbe5aea4275'),
(149, 21, 'single_video_slider', ''),
(150, 21, '_single_video_slider', 'field_5ddbe636a4276'),
(151, 25, 'title_tag', ''),
(152, 25, '_title_tag', 'field_5ddbe7577e0e7'),
(153, 25, 'single_slider_seo', ''),
(154, 25, '_single_slider_seo', 'field_5ddbde5499115'),
(155, 25, 'single_gallery', ''),
(156, 25, '_single_gallery', 'field_5ddbe5aea4275'),
(157, 25, 'single_video_slider', ''),
(158, 25, '_single_video_slider', 'field_5ddbe636a4276'),
(161, 41, '_edit_last', '1'),
(162, 41, '_edit_lock', '1627152261:1'),
(163, 42, '_edit_last', '1'),
(164, 42, '_edit_lock', '1627220082:1'),
(168, 70, '_edit_lock', '1627118836:1'),
(169, 70, '_edit_last', '1'),
(170, 80, '_edit_last', '1'),
(171, 80, '_edit_lock', '1627119237:1'),
(172, 83, '_edit_last', '1'),
(173, 83, '_edit_lock', '1627033067:1'),
(174, 85, '_edit_last', '1'),
(175, 85, '_edit_lock', '1627057537:1'),
(176, 87, '_edit_last', '1'),
(177, 87, '_edit_lock', '1627153472:1'),
(178, 26, '_edit_lock', '1626890765:1'),
(179, 26, '_edit_last', '1'),
(180, 102, '_wp_attached_file', '2021/07/category-1.png'),
(181, 102, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1134;s:6:\"height\";i:298;s:4:\"file\";s:22:\"2021/07/category-1.png\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"category-1-300x79.png\";s:5:\"width\";i:300;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"category-1-1024x269.png\";s:5:\"width\";i:1024;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"category-1-768x202.png\";s:5:\"width\";i:768;s:6:\"height\";i:202;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"category-1-134x35.png\";s:5:\"width\";i:134;s:6:\"height\";i:35;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-1-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"category-1-600x158.png\";s:5:\"width\";i:600;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-1-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"category-1-600x158.png\";s:5:\"width\";i:600;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(182, 103, '_wp_attached_file', '2021/07/category-2.png'),
(183, 103, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:357;s:4:\"file\";s:22:\"2021/07/category-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-2-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"category-2-134x85.png\";s:5:\"width\";i:134;s:6:\"height\";i:85;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(184, 104, '_wp_attached_file', '2021/07/category-3.png'),
(185, 104, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:357;s:4:\"file\";s:22:\"2021/07/category-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-3-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"category-3-134x85.png\";s:5:\"width\";i:134;s:6:\"height\";i:85;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(186, 105, '_wp_attached_file', '2021/07/category-4.png'),
(187, 105, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:662;s:4:\"file\";s:22:\"2021/07/category-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-4-255x300.png\";s:5:\"width\";i:255;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"category-4-80x94.png\";s:5:\"width\";i:80;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(188, 106, '_wp_attached_file', '2021/07/category-5.png'),
(189, 106, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:662;s:4:\"file\";s:22:\"2021/07/category-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-5-255x300.png\";s:5:\"width\";i:255;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"category-5-80x94.png\";s:5:\"width\";i:80;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(190, 107, '_wp_attached_file', '2021/07/category-6.png'),
(191, 107, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:357;s:4:\"file\";s:22:\"2021/07/category-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-6-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"category-6-134x85.png\";s:5:\"width\";i:134;s:6:\"height\";i:85;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(192, 108, '_wp_attached_file', '2021/07/category-7.png'),
(193, 108, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:357;s:4:\"file\";s:22:\"2021/07/category-7.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-7-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"category-7-134x85.png\";s:5:\"width\";i:134;s:6:\"height\";i:85;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(194, 109, '_wp_attached_file', '2021/07/category-8.png'),
(195, 109, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1134;s:6:\"height\";i:298;s:4:\"file\";s:22:\"2021/07/category-8.png\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"category-8-300x79.png\";s:5:\"width\";i:300;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"category-8-1024x269.png\";s:5:\"width\";i:1024;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"category-8-768x202.png\";s:5:\"width\";i:768;s:6:\"height\";i:202;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"category-8-134x35.png\";s:5:\"width\";i:134;s:6:\"height\";i:35;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-8-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"category-8-600x158.png\";s:5:\"width\";i:600;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-8-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"category-8-600x158.png\";s:5:\"width\";i:600;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(196, 110, '_wp_attached_file', '2021/07/category-9.png'),
(197, 110, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:357;s:4:\"file\";s:22:\"2021/07/category-9.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-9-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"category-9-134x85.png\";s:5:\"width\";i:134;s:6:\"height\";i:85;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(198, 111, '_wp_attached_file', '2021/07/category-10.png'),
(199, 111, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:357;s:4:\"file\";s:23:\"2021/07/category-10.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"category-10-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"category-10-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"category-10-134x85.png\";s:5:\"width\";i:134;s:6:\"height\";i:85;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"category-10-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"category-10-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"category-10-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"category-10-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(200, 112, '_wp_attached_file', '2021/07/category-11.png'),
(201, 112, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:357;s:4:\"file\";s:23:\"2021/07/category-11.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"category-11-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"category-11-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"category-11-134x85.png\";s:5:\"width\";i:134;s:6:\"height\";i:85;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"category-11-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"category-11-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"category-11-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"category-11-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(202, 113, '_wp_attached_file', '2021/07/category-12.png'),
(203, 113, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1134;s:6:\"height\";i:298;s:4:\"file\";s:23:\"2021/07/category-12.png\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-12-300x79.png\";s:5:\"width\";i:300;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"category-12-1024x269.png\";s:5:\"width\";i:1024;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"category-12-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"category-12-768x202.png\";s:5:\"width\";i:768;s:6:\"height\";i:202;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"category-12-134x35.png\";s:5:\"width\";i:134;s:6:\"height\";i:35;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"category-12-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"category-12-600x158.png\";s:5:\"width\";i:600;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"category-12-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"category-12-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:23:\"category-12-600x158.png\";s:5:\"width\";i:600;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"category-12-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(204, 114, '_edit_last', '1'),
(205, 114, '_edit_lock', '1626891157:1'),
(206, 116, '_wp_attached_file', '2021/07/artist-1.png'),
(207, 116, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:415;s:4:\"file\";s:20:\"2021/07/artist-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-1-286x300.png\";s:5:\"width\";i:286;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"artist-1-90x94.png\";s:5:\"width\";i:90;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(208, 117, '_wp_attached_file', '2021/07/artist-2.png'),
(209, 117, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:415;s:4:\"file\";s:20:\"2021/07/artist-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-2-286x300.png\";s:5:\"width\";i:286;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"artist-2-90x94.png\";s:5:\"width\";i:90;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(210, 118, '_wp_attached_file', '2021/07/artist-3.png'),
(211, 118, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:412;s:4:\"file\";s:20:\"2021/07/artist-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-3-288x300.png\";s:5:\"width\";i:288;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"artist-3-90x94.png\";s:5:\"width\";i:90;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(212, 119, '_wp_attached_file', '2021/07/artist-4.png'),
(213, 119, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:406;s:4:\"file\";s:20:\"2021/07/artist-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-4-293x300.png\";s:5:\"width\";i:293;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"artist-4-92x94.png\";s:5:\"width\";i:92;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(214, 120, '_wp_attached_file', '2021/07/artist-5.png'),
(215, 120, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:414;s:4:\"file\";s:20:\"2021/07/artist-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-5-287x300.png\";s:5:\"width\";i:287;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"artist-5-90x94.png\";s:5:\"width\";i:90;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(216, 121, '_wp_attached_file', '2021/07/artist-6.png'),
(217, 121, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:414;s:4:\"file\";s:20:\"2021/07/artist-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-6-287x300.png\";s:5:\"width\";i:287;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"artist-6-90x94.png\";s:5:\"width\";i:90;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(218, 122, '_wp_attached_file', '2021/07/artist-7.png'),
(219, 122, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:408;s:4:\"file\";s:20:\"2021/07/artist-7.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-7-291x300.png\";s:5:\"width\";i:291;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"artist-7-91x94.png\";s:5:\"width\";i:91;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(220, 123, '_wp_attached_file', '2021/07/artist-8.png'),
(221, 123, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:372;s:4:\"file\";s:20:\"2021/07/artist-8.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-8-300x282.png\";s:5:\"width\";i:300;s:6:\"height\";i:282;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"artist-8-100x94.png\";s:5:\"width\";i:100;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(222, 124, '_wp_attached_file', '2021/07/artist-9.png');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(223, 124, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:434;s:4:\"file\";s:20:\"2021/07/artist-9.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"artist-9-274x300.png\";s:5:\"width\";i:274;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"artist-9-86x94.png\";s:5:\"width\";i:86;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"artist-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"artist-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"artist-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(224, 125, '_edit_last', '1'),
(225, 125, '_edit_lock', '1627064275:1'),
(226, 125, '_regular_price', '123'),
(227, 125, '_sale_price', '100'),
(228, 125, 'total_sales', '0'),
(229, 125, '_tax_status', 'taxable'),
(230, 125, '_tax_class', ''),
(231, 125, '_manage_stock', 'no'),
(232, 125, '_backorders', 'no'),
(233, 125, '_sold_individually', 'no'),
(234, 125, '_virtual', 'no'),
(235, 125, '_downloadable', 'no'),
(236, 125, '_download_limit', '-1'),
(237, 125, '_download_expiry', '-1'),
(238, 125, '_stock', NULL),
(239, 125, '_stock_status', 'instock'),
(240, 125, '_wc_average_rating', '0'),
(241, 125, '_wc_review_count', '0'),
(242, 125, '_product_version', '5.5.1'),
(243, 125, '_price', '100'),
(244, 126, '_wp_attached_file', '2021/07/product-1.png'),
(245, 126, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:434;s:4:\"file\";s:21:\"2021/07/product-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-1-277x300.png\";s:5:\"width\";i:277;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-1-87x94.png\";s:5:\"width\";i:87;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(246, 127, '_wp_attached_file', '2021/07/product-3.png'),
(247, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:434;s:4:\"file\";s:21:\"2021/07/product-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-3-274x300.png\";s:5:\"width\";i:274;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-3-86x94.png\";s:5:\"width\";i:86;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(248, 128, '_wp_attached_file', '2021/07/product-4.png'),
(249, 128, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:434;s:4:\"file\";s:21:\"2021/07/product-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-4-277x300.png\";s:5:\"width\";i:277;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-4-87x94.png\";s:5:\"width\";i:87;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(250, 129, '_wp_attached_file', '2021/07/product-5.png'),
(251, 129, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:434;s:4:\"file\";s:21:\"2021/07/product-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-5-277x300.png\";s:5:\"width\";i:277;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-5-87x94.png\";s:5:\"width\";i:87;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(252, 130, '_wp_attached_file', '2021/07/product-6.png'),
(253, 130, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:433;s:4:\"file\";s:21:\"2021/07/product-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-6-274x300.png\";s:5:\"width\";i:274;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-6-86x94.png\";s:5:\"width\";i:86;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(254, 131, '_wp_attached_file', '2021/07/product-7.png'),
(255, 131, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:434;s:4:\"file\";s:21:\"2021/07/product-7.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-7-274x300.png\";s:5:\"width\";i:274;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-7-86x94.png\";s:5:\"width\";i:86;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(256, 132, '_wp_attached_file', '2021/07/product-8.png'),
(257, 132, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:434;s:4:\"file\";s:21:\"2021/07/product-8.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-8-277x300.png\";s:5:\"width\";i:277;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-8-87x94.png\";s:5:\"width\";i:87;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(258, 133, '_wp_attached_file', '2021/07/product-9.png'),
(259, 133, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:434;s:4:\"file\";s:21:\"2021/07/product-9.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-9-277x300.png\";s:5:\"width\";i:277;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-9-87x94.png\";s:5:\"width\";i:87;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(260, 134, '_wp_attached_file', '2021/07/product-10.png'),
(261, 134, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:434;s:4:\"file\";s:22:\"2021/07/product-10.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-10-274x300.png\";s:5:\"width\";i:274;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-10-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-10-86x94.png\";s:5:\"width\";i:86;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-10-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-10-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-10-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-10-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(262, 135, '_wp_attached_file', '2021/07/product-11.png'),
(263, 135, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:434;s:4:\"file\";s:22:\"2021/07/product-11.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-11-274x300.png\";s:5:\"width\";i:274;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-11-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-11-86x94.png\";s:5:\"width\";i:86;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-11-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-11-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-11-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-11-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(264, 136, '_wp_attached_file', '2021/07/product-12.png'),
(265, 136, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:434;s:4:\"file\";s:22:\"2021/07/product-12.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-12-277x300.png\";s:5:\"width\";i:277;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-12-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-12-87x94.png\";s:5:\"width\";i:87;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-12-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-12-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-12-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-12-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(266, 125, '_thumbnail_id', '126'),
(267, 137, '_edit_last', '1'),
(268, 137, '_thumbnail_id', '127'),
(269, 137, '_regular_price', '178'),
(270, 137, '_sale_price', '140'),
(271, 137, 'total_sales', '0'),
(272, 137, '_tax_status', 'taxable'),
(273, 137, '_tax_class', ''),
(274, 137, '_manage_stock', 'no'),
(275, 137, '_backorders', 'no'),
(276, 137, '_sold_individually', 'no'),
(277, 137, '_virtual', 'no'),
(278, 137, '_downloadable', 'no'),
(279, 137, '_download_limit', '-1'),
(280, 137, '_download_expiry', '-1'),
(281, 137, '_stock', NULL),
(282, 137, '_stock_status', 'instock'),
(283, 137, '_wc_average_rating', '0'),
(284, 137, '_wc_review_count', '0'),
(285, 137, '_product_version', '5.5.1'),
(286, 137, '_price', '140'),
(287, 137, '_edit_lock', '1626891681:1'),
(288, 138, '_edit_last', '1'),
(289, 138, '_edit_lock', '1626893142:1'),
(290, 138, '_thumbnail_id', '130'),
(291, 138, '_regular_price', '99'),
(292, 138, '_sale_price', '77'),
(293, 138, 'total_sales', '0'),
(294, 138, '_tax_status', 'taxable'),
(295, 138, '_tax_class', ''),
(296, 138, '_manage_stock', 'no'),
(297, 138, '_backorders', 'no'),
(298, 138, '_sold_individually', 'no'),
(299, 138, '_virtual', 'no'),
(300, 138, '_downloadable', 'no'),
(301, 138, '_download_limit', '-1'),
(302, 138, '_download_expiry', '-1'),
(303, 138, '_stock', NULL),
(304, 138, '_stock_status', 'instock'),
(305, 138, '_wc_average_rating', '0'),
(306, 138, '_wc_review_count', '0'),
(307, 138, '_product_version', '5.5.1'),
(308, 138, '_price', '77'),
(309, 139, '_edit_last', '1'),
(310, 139, '_edit_lock', '1626893189:1'),
(311, 139, '_thumbnail_id', '129'),
(312, 139, '_regular_price', '222'),
(313, 139, '_sale_price', '200'),
(314, 139, 'total_sales', '0'),
(315, 139, '_tax_status', 'taxable'),
(316, 139, '_tax_class', ''),
(317, 139, '_manage_stock', 'no'),
(318, 139, '_backorders', 'no'),
(319, 139, '_sold_individually', 'no'),
(320, 139, '_virtual', 'no'),
(321, 139, '_downloadable', 'no'),
(322, 139, '_download_limit', '-1'),
(323, 139, '_download_expiry', '-1'),
(324, 139, '_stock', NULL),
(325, 139, '_stock_status', 'instock'),
(326, 139, '_wc_average_rating', '0'),
(327, 139, '_wc_review_count', '0'),
(328, 139, '_product_version', '5.5.1'),
(329, 139, '_price', '200'),
(330, 140, '_edit_last', '1'),
(331, 140, '_thumbnail_id', '131'),
(332, 140, '_regular_price', '190'),
(333, 140, '_sale_price', '180'),
(334, 140, 'total_sales', '0'),
(335, 140, '_tax_status', 'taxable'),
(336, 140, '_tax_class', ''),
(337, 140, '_manage_stock', 'no'),
(338, 140, '_backorders', 'no'),
(339, 140, '_sold_individually', 'no'),
(340, 140, '_virtual', 'no'),
(341, 140, '_downloadable', 'no'),
(342, 140, '_download_limit', '-1'),
(343, 140, '_download_expiry', '-1'),
(344, 140, '_stock', NULL),
(345, 140, '_stock_status', 'instock'),
(346, 140, '_wc_average_rating', '0'),
(347, 140, '_wc_review_count', '0'),
(348, 140, '_product_version', '5.5.1'),
(349, 140, '_price', '180'),
(350, 140, '_edit_lock', '1626893227:1'),
(351, 141, '_edit_last', '1'),
(352, 141, '_thumbnail_id', '133'),
(353, 141, '_regular_price', '280'),
(354, 141, '_sale_price', '250'),
(355, 141, 'total_sales', '0'),
(356, 141, '_tax_status', 'taxable'),
(357, 141, '_tax_class', ''),
(358, 141, '_manage_stock', 'no'),
(359, 141, '_backorders', 'no'),
(360, 141, '_sold_individually', 'no'),
(361, 141, '_virtual', 'no'),
(362, 141, '_downloadable', 'no'),
(363, 141, '_download_limit', '-1'),
(364, 141, '_download_expiry', '-1'),
(365, 141, '_stock', NULL),
(366, 141, '_stock_status', 'instock'),
(367, 141, '_wc_average_rating', '0'),
(368, 141, '_wc_review_count', '0'),
(369, 141, '_product_version', '5.5.1'),
(370, 141, '_price', '250'),
(371, 141, '_edit_lock', '1626894200:1'),
(372, 142, '_edit_last', '1'),
(373, 142, '_edit_lock', '1626894237:1'),
(374, 142, '_thumbnail_id', '134'),
(375, 142, '_regular_price', '190'),
(376, 142, '_sale_price', '160'),
(377, 142, 'total_sales', '0'),
(378, 142, '_tax_status', 'taxable'),
(379, 142, '_tax_class', ''),
(380, 142, '_manage_stock', 'no'),
(381, 142, '_backorders', 'no'),
(382, 142, '_sold_individually', 'no'),
(383, 142, '_virtual', 'no'),
(384, 142, '_downloadable', 'no'),
(385, 142, '_download_limit', '-1'),
(386, 142, '_download_expiry', '-1'),
(387, 142, '_stock', NULL),
(388, 142, '_stock_status', 'instock'),
(389, 142, '_wc_average_rating', '0'),
(390, 142, '_wc_review_count', '0'),
(391, 142, '_product_version', '5.5.1'),
(392, 142, '_price', '160'),
(393, 143, '_edit_last', '1'),
(394, 143, '_edit_lock', '1626894271:1'),
(395, 143, '_thumbnail_id', '135'),
(396, 143, 'total_sales', '0'),
(397, 143, '_tax_status', 'taxable'),
(398, 143, '_tax_class', ''),
(399, 143, '_manage_stock', 'no'),
(400, 143, '_backorders', 'no'),
(401, 143, '_sold_individually', 'no'),
(402, 143, '_virtual', 'no'),
(403, 143, '_downloadable', 'no'),
(404, 143, '_download_limit', '-1'),
(405, 143, '_download_expiry', '-1'),
(406, 143, '_stock', NULL),
(407, 143, '_stock_status', 'instock'),
(408, 143, '_wc_average_rating', '0'),
(409, 143, '_wc_review_count', '0'),
(410, 143, '_product_version', '5.5.1'),
(411, 144, '_edit_last', '1'),
(412, 144, '_edit_lock', '1626894315:1'),
(413, 144, '_thumbnail_id', '136'),
(414, 144, 'total_sales', '0'),
(415, 144, '_tax_status', 'taxable'),
(416, 144, '_tax_class', ''),
(417, 144, '_manage_stock', 'no'),
(418, 144, '_backorders', 'no'),
(419, 144, '_sold_individually', 'no'),
(420, 144, '_virtual', 'no'),
(421, 144, '_downloadable', 'no'),
(422, 144, '_download_limit', '-1'),
(423, 144, '_download_expiry', '-1'),
(424, 144, '_stock', NULL),
(425, 144, '_stock_status', 'instock'),
(426, 144, '_wc_average_rating', '0'),
(427, 144, '_wc_review_count', '0'),
(428, 144, '_product_version', '5.5.1'),
(429, 145, '_edit_last', '1'),
(430, 145, '_edit_lock', '1626894370:1'),
(431, 146, '_wp_attached_file', '2021/07/product-13.png'),
(432, 146, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:427;s:6:\"height\";i:522;s:4:\"file\";s:22:\"2021/07/product-13.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-13-245x300.png\";s:5:\"width\";i:245;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-13-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-13-77x94.png\";s:5:\"width\";i:77;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-13-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-13-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-13-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-13-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(433, 147, '_wp_attached_file', '2021/07/product-14.png'),
(434, 147, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:427;s:6:\"height\";i:522;s:4:\"file\";s:22:\"2021/07/product-14.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-14-245x300.png\";s:5:\"width\";i:245;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-14-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-14-77x94.png\";s:5:\"width\";i:77;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-14-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-14-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-14-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-14-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(435, 148, '_wp_attached_file', '2021/07/product-15.png'),
(436, 148, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:427;s:6:\"height\";i:522;s:4:\"file\";s:22:\"2021/07/product-15.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-15-245x300.png\";s:5:\"width\";i:245;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-15-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-15-77x94.png\";s:5:\"width\";i:77;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-15-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-15-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-15-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-15-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(437, 149, '_wp_attached_file', '2021/07/product-16.png'),
(438, 149, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:427;s:6:\"height\";i:522;s:4:\"file\";s:22:\"2021/07/product-16.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-16-245x300.png\";s:5:\"width\";i:245;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-16-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-16-77x94.png\";s:5:\"width\";i:77;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-16-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-16-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-16-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-16-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(439, 145, '_thumbnail_id', '146'),
(440, 145, '_regular_price', '160'),
(441, 145, '_sale_price', '150'),
(442, 145, 'total_sales', '0'),
(443, 145, '_tax_status', 'taxable'),
(444, 145, '_tax_class', ''),
(445, 145, '_manage_stock', 'no'),
(446, 145, '_backorders', 'no'),
(447, 145, '_sold_individually', 'no'),
(448, 145, '_virtual', 'no'),
(449, 145, '_downloadable', 'no'),
(450, 145, '_download_limit', '-1'),
(451, 145, '_download_expiry', '-1'),
(452, 145, '_stock', NULL),
(453, 145, '_stock_status', 'instock'),
(454, 145, '_wc_average_rating', '0'),
(455, 145, '_wc_review_count', '0'),
(456, 145, '_product_version', '5.5.1'),
(457, 145, '_price', '150'),
(458, 150, '_edit_last', '1'),
(459, 150, '_edit_lock', '1626894406:1'),
(460, 150, '_thumbnail_id', '147'),
(461, 150, '_regular_price', '300'),
(462, 150, '_sale_price', '270'),
(463, 150, 'total_sales', '0'),
(464, 150, '_tax_status', 'taxable'),
(465, 150, '_tax_class', ''),
(466, 150, '_manage_stock', 'no'),
(467, 150, '_backorders', 'no'),
(468, 150, '_sold_individually', 'no'),
(469, 150, '_virtual', 'no'),
(470, 150, '_downloadable', 'no'),
(471, 150, '_download_limit', '-1'),
(472, 150, '_download_expiry', '-1'),
(473, 150, '_stock', NULL),
(474, 150, '_stock_status', 'instock'),
(475, 150, '_wc_average_rating', '0'),
(476, 150, '_wc_review_count', '0'),
(477, 150, '_product_version', '5.5.1'),
(478, 150, '_price', '270'),
(479, 151, '_edit_last', '1'),
(480, 151, '_edit_lock', '1626894436:1'),
(481, 151, '_thumbnail_id', '147'),
(482, 151, 'total_sales', '0'),
(483, 151, '_tax_status', 'taxable'),
(484, 151, '_tax_class', ''),
(485, 151, '_manage_stock', 'no'),
(486, 151, '_backorders', 'no'),
(487, 151, '_sold_individually', 'no'),
(488, 151, '_virtual', 'no'),
(489, 151, '_downloadable', 'no'),
(490, 151, '_download_limit', '-1'),
(491, 151, '_download_expiry', '-1'),
(492, 151, '_stock', NULL),
(493, 151, '_stock_status', 'instock'),
(494, 151, '_wc_average_rating', '0'),
(495, 151, '_wc_review_count', '0'),
(496, 151, '_product_version', '5.5.1'),
(497, 152, '_edit_last', '1'),
(498, 152, '_edit_lock', '1626894471:1'),
(499, 152, '_thumbnail_id', '148'),
(500, 152, '_regular_price', '176'),
(501, 152, '_sale_price', '150'),
(502, 152, 'total_sales', '0'),
(503, 152, '_tax_status', 'taxable'),
(504, 152, '_tax_class', ''),
(505, 152, '_manage_stock', 'no'),
(506, 152, '_backorders', 'no'),
(507, 152, '_sold_individually', 'no'),
(508, 152, '_virtual', 'no'),
(509, 152, '_downloadable', 'no'),
(510, 152, '_download_limit', '-1'),
(511, 152, '_download_expiry', '-1'),
(512, 152, '_stock', NULL),
(513, 152, '_stock_status', 'instock'),
(514, 152, '_wc_average_rating', '0'),
(515, 152, '_wc_review_count', '0'),
(516, 152, '_product_version', '5.5.1'),
(517, 152, '_price', '150'),
(518, 153, '_edit_last', '1'),
(519, 153, '_edit_lock', '1626894536:1'),
(520, 153, '_thumbnail_id', '149'),
(521, 153, '_regular_price', '280'),
(522, 153, '_sale_price', '200'),
(523, 153, 'total_sales', '0'),
(524, 153, '_tax_status', 'taxable'),
(525, 153, '_tax_class', ''),
(526, 153, '_manage_stock', 'no'),
(527, 153, '_backorders', 'no'),
(528, 153, '_sold_individually', 'no'),
(529, 153, '_virtual', 'no'),
(530, 153, '_downloadable', 'no'),
(531, 153, '_download_limit', '-1'),
(532, 153, '_download_expiry', '-1'),
(533, 153, '_stock', NULL),
(534, 153, '_stock_status', 'instock'),
(535, 153, '_wc_average_rating', '0'),
(536, 153, '_wc_review_count', '0'),
(537, 153, '_product_version', '5.5.1'),
(538, 153, '_price', '200'),
(539, 154, '_edit_last', '1'),
(540, 154, '_edit_lock', '1626966556:1'),
(541, 154, '_thumbnail_id', '147'),
(542, 154, '_regular_price', '210'),
(543, 154, '_sale_price', '180'),
(544, 154, 'total_sales', '0'),
(545, 154, '_tax_status', 'taxable'),
(546, 154, '_tax_class', ''),
(547, 154, '_manage_stock', 'no'),
(548, 154, '_backorders', 'no'),
(549, 154, '_sold_individually', 'no'),
(550, 154, '_virtual', 'no'),
(551, 154, '_downloadable', 'no'),
(552, 154, '_download_limit', '-1'),
(553, 154, '_download_expiry', '-1'),
(554, 154, '_stock', NULL),
(555, 154, '_stock_status', 'instock'),
(556, 154, '_wc_average_rating', '0'),
(557, 154, '_wc_review_count', '0'),
(558, 154, '_product_version', '5.5.1'),
(559, 154, '_price', '180'),
(560, 155, '_wp_attached_file', '2021/07/main-img-e1626933397959.png'),
(561, 155, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:902;s:4:\"file\";s:35:\"2021/07/main-img-e1626933397959.png\";s:5:\"sizes\";a:12:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-300x141.png\";s:5:\"width\";i:300;s:6:\"height\";i:141;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:36:\"main-img-e1626933397959-1024x481.png\";s:5:\"width\";i:1024;s:6:\"height\";i:481;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-768x361.png\";s:5:\"width\";i:768;s:6:\"height\";i:361;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:36:\"main-img-e1626933397959-1536x722.png\";s:5:\"width\";i:1536;s:6:\"height\";i:722;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:34:\"main-img-e1626933397959-134x63.png\";s:5:\"width\";i:134;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-600x282.png\";s:5:\"width\";i:600;s:6:\"height\";i:282;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-600x282.png\";s:5:\"width\";i:600;s:6:\"height\";i:282;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"main-img-e1626933397959-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(562, 156, '_wp_attached_file', '2021/07/home-gallery-1.png'),
(563, 156, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1134;s:6:\"height\";i:367;s:4:\"file\";s:26:\"2021/07/home-gallery-1.png\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"home-gallery-1-300x97.png\";s:5:\"width\";i:300;s:6:\"height\";i:97;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"home-gallery-1-1024x331.png\";s:5:\"width\";i:1024;s:6:\"height\";i:331;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-768x249.png\";s:5:\"width\";i:768;s:6:\"height\";i:249;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:25:\"home-gallery-1-134x43.png\";s:5:\"width\";i:134;s:6:\"height\";i:43;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-600x194.png\";s:5:\"width\";i:600;s:6:\"height\";i:194;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-600x194.png\";s:5:\"width\";i:600;s:6:\"height\";i:194;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(564, 157, '_wp_attached_file', '2021/07/home-gallery-2.png'),
(565, 157, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:440;s:4:\"file\";s:26:\"2021/07/home-gallery-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-300x234.png\";s:5:\"width\";i:300;s:6:\"height\";i:234;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:25:\"home-gallery-2-120x94.png\";s:5:\"width\";i:120;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(566, 158, '_wp_attached_file', '2021/07/home-gallery-3.png'),
(567, 158, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:439;s:4:\"file\";s:26:\"2021/07/home-gallery-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-300x234.png\";s:5:\"width\";i:300;s:6:\"height\";i:234;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:25:\"home-gallery-3-121x94.png\";s:5:\"width\";i:121;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(568, 159, '_wp_attached_file', '2021/07/home-gallery-4.png'),
(569, 159, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:588;s:4:\"file\";s:26:\"2021/07/home-gallery-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-287x300.png\";s:5:\"width\";i:287;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"home-gallery-4-90x94.png\";s:5:\"width\";i:90;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(570, 8, '_thumbnail_id', '155'),
(571, 8, 'home_about_text', '<h2>מי אנחנו</h2>\r\nwall style מביאה לישראל את הדבר החם בעולם עיצוב הפנים הדפסת אומנות, גרפיקה, תמונות וציורים והכל ישירות על הקיר באיכות גבוהה במיוחד אנו מקדמים אומנות ישראלית מקורית\r\n\r\nועובדים בשיתוף פעולה עם מגוון אמנים, ציירים וצלמים מקצועיים ואיכותיים. אצלינו ניתן לבחור תמונה או ציור, מתוך מאגר נרחב ומגוון המחולק לפי קטגוריות מסודרות, המקלות על השימוש והחיפוש באתר. או לחלופין הדפסה אישית לפי צרכי הלקוח, כגון: לוגו לעסק, הדפסת חזון וערכים, או גרפיקה מותאמת אישית.'),
(572, 8, '_home_about_text', 'field_60f7e7c1e8bf9'),
(573, 8, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:13:\"/sample-page/\";s:6:\"target\";s:0:\"\";}'),
(574, 8, '_home_about_link', 'field_60f7e7eae8bfa'),
(575, 8, 'home_video_back', '232'),
(576, 8, '_home_video_back', 'field_60f825195b93e'),
(577, 8, 'home_video_block_img', '166'),
(578, 8, '_home_video_block_img', 'field_60f825355b93f'),
(579, 8, 'home_video_block_title', 'אומנות שוברת מסגרות '),
(580, 8, '_home_video_block_title', 'field_60f8254d5b940'),
(581, 8, 'home_video_block_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(582, 8, '_home_video_block_text', 'field_60f825645b941'),
(583, 8, 'home_video_block_links', '2'),
(584, 8, '_home_video_block_links', 'field_60f8257f5b942'),
(585, 8, 'home_cats_title', 'בחרו בין הקטגוריות החמות'),
(586, 8, '_home_cats_title', 'field_60f8273b55681'),
(587, 8, 'home_cats_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(588, 8, '_home_cats_text', 'field_60f8274a55682'),
(589, 8, 'home_cats_chosen', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"16\";i:2;s:2:\"18\";i:3;s:2:\"20\";i:4;s:2:\"17\";}'),
(590, 8, '_home_cats_chosen', 'field_60f8275b55683'),
(591, 8, 'home_cats_link', 'a:3:{s:5:\"title\";s:25:\"לכל הקטגוריות\";s:3:\"url\";s:50:\"/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(592, 8, '_home_cats_link', 'field_60f8277455684'),
(593, 8, 'home_offer_title', 'אומנות שוברת מסגרות רק כאן!'),
(594, 8, '_home_offer_title', 'field_60f827bd55686'),
(595, 8, 'home_offer_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(596, 8, '_home_offer_text', 'field_60f827c755687'),
(597, 8, 'home_offer_link', 'a:3:{s:5:\"title\";s:32:\"לבחירת תמונה לחצו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(598, 8, '_home_offer_link', 'field_60f827d855688'),
(599, 8, 'slider_img', '168'),
(600, 8, '_slider_img', 'field_60f58b1c85127'),
(601, 8, 'home_prods_title', 'חדש באתר'),
(602, 8, '_home_prods_title', 'field_60f82b984d2af'),
(603, 8, 'home_prods', 'a:5:{i:0;s:3:\"145\";i:1;s:3:\"152\";i:2;s:3:\"153\";i:3;s:3:\"154\";i:4;s:3:\"137\";}'),
(604, 8, '_home_prods', 'field_60f82ba14d2b0'),
(605, 8, 'home_gallery_title', 'הגלריה שלנו'),
(606, 8, '_home_gallery_title', 'field_60f82bc04d2b2'),
(607, 8, 'home_gallery', 'a:4:{i:0;s:3:\"156\";i:1;s:3:\"157\";i:2;s:3:\"158\";i:3;s:3:\"159\";}'),
(608, 8, '_home_gallery', 'field_60f82bce4d2b3'),
(609, 8, 'home_gallery_link', 'a:3:{s:5:\"title\";s:19:\"לכל הגלריה\";s:3:\"url\";s:32:\"/%d7%92%d7%9c%d7%a8%d7%99%d7%94/\";s:6:\"target\";s:0:\"\";}'),
(610, 8, '_home_gallery_link', 'field_60f82be84d2b4'),
(611, 8, 'faq_title', 'שאלות נפוצות'),
(612, 8, '_faq_title', 'field_5feb2f82db93b'),
(613, 8, 'faq_text', 'שאלתם, ענינו, כל השאלות במקום אחד'),
(614, 8, '_faq_text', 'field_5feb2f95db93c'),
(615, 8, 'faq_item', '3'),
(616, 8, '_faq_item', 'field_5feb2faddb93d');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(617, 8, 'faq_link', 'a:3:{s:5:\"title\";s:19:\"לכל השאלות\";s:3:\"url\";s:75:\"/%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(618, 8, '_faq_link', 'field_60f82c8971ca0'),
(619, 160, 'title_tag', ''),
(620, 160, '_title_tag', 'field_5ddbe7577e0e7'),
(621, 160, 'single_slider_seo', ''),
(622, 160, '_single_slider_seo', 'field_5ddbde5499115'),
(623, 160, 'single_gallery', ''),
(624, 160, '_single_gallery', 'field_5ddbe5aea4275'),
(625, 160, 'single_video_slider', ''),
(626, 160, '_single_video_slider', 'field_5ddbe636a4276'),
(627, 160, 'home_about_text', '<h2>מי אנחנו</h2>\r\nwall style מביאה לישראל את הדבר החם בעולם עיצוב הפנים הדפסת אומנות, גרפיקה, תמונות וציורים והכל ישירות על הקיר באיכות גבוהה במיוחד אנו מקדמים אומנות ישראלית מקורית\r\n\r\nועובדים בשיתוף פעולה עם מגוון אמנים, ציירים וצלמים מקצועיים ואיכותיים. אצלינו ניתן לבחור תמונה או ציור, מתוך מאגר נרחב ומגוון המחולק לפי קטגוריות מסודרות, המקלות על השימוש והחיפוש באתר. או לחלופין הדפסה אישית לפי צרכי הלקוח, כגון: לוגו לעסק, הדפסת חזון וערכים, או גרפיקה מותאמת אישית.'),
(628, 160, '_home_about_text', 'field_60f7e7c1e8bf9'),
(629, 160, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:13:\"/sample-page/\";s:6:\"target\";s:0:\"\";}'),
(630, 160, '_home_about_link', 'field_60f7e7eae8bfa'),
(631, 160, 'home_video_back', ''),
(632, 160, '_home_video_back', 'field_60f825195b93e'),
(633, 160, 'home_video_block_img', ''),
(634, 160, '_home_video_block_img', 'field_60f825355b93f'),
(635, 160, 'home_video_block_title', ''),
(636, 160, '_home_video_block_title', 'field_60f8254d5b940'),
(637, 160, 'home_video_block_text', ''),
(638, 160, '_home_video_block_text', 'field_60f825645b941'),
(639, 160, 'home_video_block_links', ''),
(640, 160, '_home_video_block_links', 'field_60f8257f5b942'),
(641, 160, 'home_cats_title', ''),
(642, 160, '_home_cats_title', 'field_60f8273b55681'),
(643, 160, 'home_cats_text', ''),
(644, 160, '_home_cats_text', 'field_60f8274a55682'),
(645, 160, 'home_cats_chosen', ''),
(646, 160, '_home_cats_chosen', 'field_60f8275b55683'),
(647, 160, 'home_cats_link', ''),
(648, 160, '_home_cats_link', 'field_60f8277455684'),
(649, 160, 'home_offer_title', ''),
(650, 160, '_home_offer_title', 'field_60f827bd55686'),
(651, 160, 'home_offer_text', ''),
(652, 160, '_home_offer_text', 'field_60f827c755687'),
(653, 160, 'home_offer_link', ''),
(654, 160, '_home_offer_link', 'field_60f827d855688'),
(655, 160, 'slider_img', ''),
(656, 160, '_slider_img', 'field_60f58b1c85127'),
(657, 160, 'home_prods_title', ''),
(658, 160, '_home_prods_title', 'field_60f82b984d2af'),
(659, 160, 'home_prods', ''),
(660, 160, '_home_prods', 'field_60f82ba14d2b0'),
(661, 160, 'home_gallery_title', ''),
(662, 160, '_home_gallery_title', 'field_60f82bc04d2b2'),
(663, 160, 'home_gallery', 'a:4:{i:0;s:3:\"156\";i:1;s:3:\"157\";i:2;s:3:\"158\";i:3;s:3:\"159\";}'),
(664, 160, '_home_gallery', 'field_60f82bce4d2b3'),
(665, 160, 'home_gallery_link', 'a:3:{s:5:\"title\";s:19:\"לכל הגלריה\";s:3:\"url\";s:32:\"/%d7%92%d7%9c%d7%a8%d7%99%d7%94/\";s:6:\"target\";s:0:\"\";}'),
(666, 160, '_home_gallery_link', 'field_60f82be84d2b4'),
(667, 160, 'faq_title', ''),
(668, 160, '_faq_title', 'field_5feb2f82db93b'),
(669, 160, 'faq_text', ''),
(670, 160, '_faq_text', 'field_5feb2f95db93c'),
(671, 160, 'faq_item', ''),
(672, 160, '_faq_item', 'field_5feb2faddb93d'),
(673, 160, 'faq_link', ''),
(674, 160, '_faq_link', 'field_60f82c8971ca0'),
(675, 161, '_wp_attached_file', '2021/07/logo.png'),
(676, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:582;s:6:\"height\";i:302;s:4:\"file\";s:16:\"2021/07/logo.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"logo-300x156.png\";s:5:\"width\";i:300;s:6:\"height\";i:156;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"logo-134x70.png\";s:5:\"width\";i:134;s:6:\"height\";i:70;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"logo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:16:\"logo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(677, 162, '_wp_attached_file', '2021/07/process-1.png'),
(678, 162, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:122;s:6:\"height\";i:125;s:4:\"file\";s:21:\"2021/07/process-1.png\";s:5:\"sizes\";a:3:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"process-1-92x94.png\";s:5:\"width\";i:92;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"process-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"process-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(679, 163, '_wp_attached_file', '2021/07/process-2.png'),
(680, 163, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:143;s:6:\"height\";i:148;s:4:\"file\";s:21:\"2021/07/process-2.png\";s:5:\"sizes\";a:3:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"process-2-91x94.png\";s:5:\"width\";i:91;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"process-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"process-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(681, 164, '_wp_attached_file', '2021/07/process-3.png'),
(682, 164, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:95;s:6:\"height\";i:133;s:4:\"file\";s:21:\"2021/07/process-3.png\";s:5:\"sizes\";a:3:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"process-3-67x94.png\";s:5:\"width\";i:67;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"process-3-95x100.png\";s:5:\"width\";i:95;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"process-3-95x100.png\";s:5:\"width\";i:95;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(683, 165, '_wp_attached_file', '2021/07/process-4.png'),
(684, 165, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:124;s:6:\"height\";i:127;s:4:\"file\";s:21:\"2021/07/process-4.png\";s:5:\"sizes\";a:3:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"process-4-92x94.png\";s:5:\"width\";i:92;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"process-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"process-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(685, 155, '_wp_attachment_backup_sizes', 'a:13:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:1920;s:6:\"height\";i:952;s:4:\"file\";s:12:\"main-img.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:20:\"main-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:20:\"main-img-300x149.png\";s:5:\"width\";i:300;s:6:\"height\";i:149;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"medium_large-orig\";a:4:{s:4:\"file\";s:20:\"main-img-768x381.png\";s:5:\"width\";i:768;s:6:\"height\";i:381;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"large-orig\";a:4:{s:4:\"file\";s:21:\"main-img-1024x508.png\";s:5:\"width\";i:1024;s:6:\"height\";i:508;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"1536x1536-orig\";a:4:{s:4:\"file\";s:21:\"main-img-1536x762.png\";s:5:\"width\";i:1536;s:6:\"height\";i:762;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:19:\"main-img-134x66.png\";s:5:\"width\";i:134;s:6:\"height\";i:66;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:20:\"main-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:20:\"main-img-600x298.png\";s:5:\"width\";i:600;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:20:\"main-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:20:\"main-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:20:\"main-img-600x298.png\";s:5:\"width\";i:600;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:20:\"main-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(686, 166, '_wp_attached_file', '2021/07/home-video-block-img.png'),
(687, 166, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:787;s:6:\"height\";i:520;s:4:\"file\";s:32:\"2021/07/home-video-block-img.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"home-video-block-img-300x198.png\";s:5:\"width\";i:300;s:6:\"height\";i:198;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"home-video-block-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"home-video-block-img-768x507.png\";s:5:\"width\";i:768;s:6:\"height\";i:507;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:31:\"home-video-block-img-134x89.png\";s:5:\"width\";i:134;s:6:\"height\";i:89;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"home-video-block-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"home-video-block-img-600x396.png\";s:5:\"width\";i:600;s:6:\"height\";i:396;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"home-video-block-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:32:\"home-video-block-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"home-video-block-img-600x396.png\";s:5:\"width\";i:600;s:6:\"height\";i:396;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"home-video-block-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(688, 8, 'home_video_block_links_0_video_block_link', 'a:3:{s:5:\"title\";s:39:\"לבחירה מהירה של תמונה\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(689, 8, '_home_video_block_links_0_video_block_link', 'field_60f825965b943'),
(690, 167, 'title_tag', ''),
(691, 167, '_title_tag', 'field_5ddbe7577e0e7'),
(692, 167, 'single_slider_seo', ''),
(693, 167, '_single_slider_seo', 'field_5ddbde5499115'),
(694, 167, 'single_gallery', ''),
(695, 167, '_single_gallery', 'field_5ddbe5aea4275'),
(696, 167, 'single_video_slider', ''),
(697, 167, '_single_video_slider', 'field_5ddbe636a4276'),
(698, 167, 'home_about_text', '<h2>מי אנחנו</h2>\r\nwall style מביאה לישראל את הדבר החם בעולם עיצוב הפנים הדפסת אומנות, גרפיקה, תמונות וציורים והכל ישירות על הקיר באיכות גבוהה במיוחד אנו מקדמים אומנות ישראלית מקורית\r\n\r\nועובדים בשיתוף פעולה עם מגוון אמנים, ציירים וצלמים מקצועיים ואיכותיים. אצלינו ניתן לבחור תמונה או ציור, מתוך מאגר נרחב ומגוון המחולק לפי קטגוריות מסודרות, המקלות על השימוש והחיפוש באתר. או לחלופין הדפסה אישית לפי צרכי הלקוח, כגון: לוגו לעסק, הדפסת חזון וערכים, או גרפיקה מותאמת אישית.'),
(699, 167, '_home_about_text', 'field_60f7e7c1e8bf9'),
(700, 167, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:13:\"/sample-page/\";s:6:\"target\";s:0:\"\";}'),
(701, 167, '_home_about_link', 'field_60f7e7eae8bfa'),
(702, 167, 'home_video_back', ''),
(703, 167, '_home_video_back', 'field_60f825195b93e'),
(704, 167, 'home_video_block_img', '166'),
(705, 167, '_home_video_block_img', 'field_60f825355b93f'),
(706, 167, 'home_video_block_title', 'אומנות שוברת מסגרות '),
(707, 167, '_home_video_block_title', 'field_60f8254d5b940'),
(708, 167, 'home_video_block_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(709, 167, '_home_video_block_text', 'field_60f825645b941'),
(710, 167, 'home_video_block_links', '1'),
(711, 167, '_home_video_block_links', 'field_60f8257f5b942'),
(712, 167, 'home_cats_title', ''),
(713, 167, '_home_cats_title', 'field_60f8273b55681'),
(714, 167, 'home_cats_text', ''),
(715, 167, '_home_cats_text', 'field_60f8274a55682'),
(716, 167, 'home_cats_chosen', ''),
(717, 167, '_home_cats_chosen', 'field_60f8275b55683'),
(718, 167, 'home_cats_link', ''),
(719, 167, '_home_cats_link', 'field_60f8277455684'),
(720, 167, 'home_offer_title', ''),
(721, 167, '_home_offer_title', 'field_60f827bd55686'),
(722, 167, 'home_offer_text', ''),
(723, 167, '_home_offer_text', 'field_60f827c755687'),
(724, 167, 'home_offer_link', ''),
(725, 167, '_home_offer_link', 'field_60f827d855688'),
(726, 167, 'slider_img', ''),
(727, 167, '_slider_img', 'field_60f58b1c85127'),
(728, 167, 'home_prods_title', ''),
(729, 167, '_home_prods_title', 'field_60f82b984d2af'),
(730, 167, 'home_prods', ''),
(731, 167, '_home_prods', 'field_60f82ba14d2b0'),
(732, 167, 'home_gallery_title', ''),
(733, 167, '_home_gallery_title', 'field_60f82bc04d2b2'),
(734, 167, 'home_gallery', 'a:4:{i:0;s:3:\"156\";i:1;s:3:\"157\";i:2;s:3:\"158\";i:3;s:3:\"159\";}'),
(735, 167, '_home_gallery', 'field_60f82bce4d2b3'),
(736, 167, 'home_gallery_link', 'a:3:{s:5:\"title\";s:19:\"לכל הגלריה\";s:3:\"url\";s:32:\"/%d7%92%d7%9c%d7%a8%d7%99%d7%94/\";s:6:\"target\";s:0:\"\";}'),
(737, 167, '_home_gallery_link', 'field_60f82be84d2b4'),
(738, 167, 'faq_title', ''),
(739, 167, '_faq_title', 'field_5feb2f82db93b'),
(740, 167, 'faq_text', ''),
(741, 167, '_faq_text', 'field_5feb2f95db93c'),
(742, 167, 'faq_item', ''),
(743, 167, '_faq_item', 'field_5feb2faddb93d'),
(744, 167, 'faq_link', ''),
(745, 167, '_faq_link', 'field_60f82c8971ca0'),
(746, 167, 'home_video_block_links_0_video_block_link', ''),
(747, 167, '_home_video_block_links_0_video_block_link', 'field_60f825965b943'),
(748, 168, '_wp_attached_file', '2021/07/סליידר.png'),
(749, 168, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:944;s:6:\"height\";i:513;s:4:\"file\";s:24:\"2021/07/סליידר.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"סליידר-300x163.png\";s:5:\"width\";i:300;s:6:\"height\";i:163;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"סליידר-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"סליידר-768x417.png\";s:5:\"width\";i:768;s:6:\"height\";i:417;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:23:\"סליידר-134x73.png\";s:5:\"width\";i:134;s:6:\"height\";i:73;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"סליידר-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:24:\"סליידר-600x326.png\";s:5:\"width\";i:600;s:6:\"height\";i:326;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"סליידר-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:24:\"סליידר-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:24:\"סליידר-600x326.png\";s:5:\"width\";i:600;s:6:\"height\";i:326;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"סליידר-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(750, 8, 'home_video_block_links_1_video_block_link', 'a:3:{s:5:\"title\";s:40:\"להעלת קובץ תמונה אישי \";s:3:\"url\";s:39:\"/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/\";s:6:\"target\";s:0:\"\";}'),
(751, 8, '_home_video_block_links_1_video_block_link', 'field_60f825965b943'),
(752, 8, 'single_slider_seo_0_content', '<h2>כותרת קידום 1</h2>\r\nמדוע לבחור wall style? אנו מקדמים אומנות ישראלית מקורית. ניתן להדפיס בכל גודל, ללא סימני חיבור ותפר. ההדפסה חלה על כל סוגי הקירות, לרבות קירות \"שפריץ\", תריסים, קירות עץ וזכוכית- גם כאלו שאינם בעלי מרקם חלק. המחיר לא מושפע ממורכבות התמונה, אלא לפי גודל ההדפסה שתבחרו. תהליך ההדפסה מהיר, נקי כך שלא יפריע לפעילות השוטפת של העסק להדפסת הקיר שלנו יש עמידות ואיכות גבוהה.\r\n\r\nהיא אינה מתקלפת בניגוד למדבקות וטפטים.ההדפסה אינה פוגעת בקיר וניתן להסירה באמצעות שכבת צבע פשוטה.'),
(753, 8, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(754, 8, 'single_slider_seo_1_content', '<h2>כותרת קידום 2</h2>\r\nמדוע לבחור wall style? אנו מקדמים אומנות ישראלית מקורית. ניתן להדפיס בכל גודל, ללא סימני חיבור ותפר. ההדפסה חלה על כל סוגי הקירות, לרבות קירות \"שפריץ\", תריסים, קירות עץ וזכוכית- גם כאלו שאינם בעלי מרקם חלק.\r\n\r\nהמחיר לא מושפע ממורכבות התמונה, אלא לפי גודל ההדפסה שתבחרו. תהליך ההדפסה מהיר, נקי כך שלא יפריע לפעילות השוטפת של העסק להדפסת הקיר שלנו יש עמידות ואיכות גבוהה.'),
(755, 8, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(756, 8, 'faq_item_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם ם היפסום'),
(757, 8, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(758, 8, 'faq_item_0_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(759, 8, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(760, 8, 'faq_item_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם היפסום'),
(761, 8, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(762, 8, 'faq_item_1_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(763, 8, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(764, 8, 'faq_item_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום'),
(765, 8, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(766, 8, 'faq_item_2_faq_answer', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(767, 8, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(768, 169, 'title_tag', ''),
(769, 169, '_title_tag', 'field_5ddbe7577e0e7'),
(770, 169, 'single_slider_seo', '2'),
(771, 169, '_single_slider_seo', 'field_5ddbde5499115'),
(772, 169, 'single_gallery', ''),
(773, 169, '_single_gallery', 'field_5ddbe5aea4275'),
(774, 169, 'single_video_slider', ''),
(775, 169, '_single_video_slider', 'field_5ddbe636a4276'),
(776, 169, 'home_about_text', '<h2>מי אנחנו</h2>\r\nwall style מביאה לישראל את הדבר החם בעולם עיצוב הפנים הדפסת אומנות, גרפיקה, תמונות וציורים והכל ישירות על הקיר באיכות גבוהה במיוחד אנו מקדמים אומנות ישראלית מקורית\r\n\r\nועובדים בשיתוף פעולה עם מגוון אמנים, ציירים וצלמים מקצועיים ואיכותיים. אצלינו ניתן לבחור תמונה או ציור, מתוך מאגר נרחב ומגוון המחולק לפי קטגוריות מסודרות, המקלות על השימוש והחיפוש באתר. או לחלופין הדפסה אישית לפי צרכי הלקוח, כגון: לוגו לעסק, הדפסת חזון וערכים, או גרפיקה מותאמת אישית.'),
(777, 169, '_home_about_text', 'field_60f7e7c1e8bf9'),
(778, 169, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:13:\"/sample-page/\";s:6:\"target\";s:0:\"\";}'),
(779, 169, '_home_about_link', 'field_60f7e7eae8bfa'),
(780, 169, 'home_video_back', ''),
(781, 169, '_home_video_back', 'field_60f825195b93e'),
(782, 169, 'home_video_block_img', '166'),
(783, 169, '_home_video_block_img', 'field_60f825355b93f'),
(784, 169, 'home_video_block_title', 'אומנות שוברת מסגרות '),
(785, 169, '_home_video_block_title', 'field_60f8254d5b940'),
(786, 169, 'home_video_block_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(787, 169, '_home_video_block_text', 'field_60f825645b941'),
(788, 169, 'home_video_block_links', '2'),
(789, 169, '_home_video_block_links', 'field_60f8257f5b942'),
(790, 169, 'home_cats_title', 'בחרו בין הקטגוריות החמות'),
(791, 169, '_home_cats_title', 'field_60f8273b55681'),
(792, 169, 'home_cats_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(793, 169, '_home_cats_text', 'field_60f8274a55682'),
(794, 169, 'home_cats_chosen', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"16\";i:2;s:2:\"18\";i:3;s:2:\"20\";i:4;s:2:\"17\";}'),
(795, 169, '_home_cats_chosen', 'field_60f8275b55683'),
(796, 169, 'home_cats_link', 'a:3:{s:5:\"title\";s:25:\"לכל הקטגוריות\";s:3:\"url\";s:50:\"/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(797, 169, '_home_cats_link', 'field_60f8277455684'),
(798, 169, 'home_offer_title', 'אומנות שוברת מסגרות רק כאן!'),
(799, 169, '_home_offer_title', 'field_60f827bd55686'),
(800, 169, 'home_offer_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(801, 169, '_home_offer_text', 'field_60f827c755687'),
(802, 169, 'home_offer_link', 'a:3:{s:5:\"title\";s:32:\"לבחירת תמונה לחצו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(803, 169, '_home_offer_link', 'field_60f827d855688'),
(804, 169, 'slider_img', '168'),
(805, 169, '_slider_img', 'field_60f58b1c85127'),
(806, 169, 'home_prods_title', 'חדש באתר'),
(807, 169, '_home_prods_title', 'field_60f82b984d2af'),
(808, 169, 'home_prods', 'a:5:{i:0;s:3:\"145\";i:1;s:3:\"152\";i:2;s:3:\"153\";i:3;s:3:\"154\";i:4;s:3:\"137\";}'),
(809, 169, '_home_prods', 'field_60f82ba14d2b0'),
(810, 169, 'home_gallery_title', 'הגלריה שלנו'),
(811, 169, '_home_gallery_title', 'field_60f82bc04d2b2'),
(812, 169, 'home_gallery', 'a:4:{i:0;s:3:\"156\";i:1;s:3:\"157\";i:2;s:3:\"158\";i:3;s:3:\"159\";}'),
(813, 169, '_home_gallery', 'field_60f82bce4d2b3'),
(814, 169, 'home_gallery_link', 'a:3:{s:5:\"title\";s:19:\"לכל הגלריה\";s:3:\"url\";s:32:\"/%d7%92%d7%9c%d7%a8%d7%99%d7%94/\";s:6:\"target\";s:0:\"\";}'),
(815, 169, '_home_gallery_link', 'field_60f82be84d2b4'),
(816, 169, 'faq_title', 'שאלות נפוצות'),
(817, 169, '_faq_title', 'field_5feb2f82db93b'),
(818, 169, 'faq_text', 'שאלתם, ענינו, כל השאלות במקום אחד'),
(819, 169, '_faq_text', 'field_5feb2f95db93c'),
(820, 169, 'faq_item', '3'),
(821, 169, '_faq_item', 'field_5feb2faddb93d'),
(822, 169, 'faq_link', ''),
(823, 169, '_faq_link', 'field_60f82c8971ca0'),
(824, 169, 'home_video_block_links_0_video_block_link', 'a:3:{s:5:\"title\";s:39:\"לבחירה מהירה של תמונה\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(825, 169, '_home_video_block_links_0_video_block_link', 'field_60f825965b943'),
(826, 169, 'home_video_block_links_1_video_block_link', 'a:3:{s:5:\"title\";s:40:\"להעלת קובץ תמונה אישי \";s:3:\"url\";s:39:\"/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/\";s:6:\"target\";s:0:\"\";}'),
(827, 169, '_home_video_block_links_1_video_block_link', 'field_60f825965b943'),
(828, 169, 'single_slider_seo_0_content', '<h2>כותרת קידום 1</h2>\r\nמדוע לבחור wall style? אנו מקדמים אומנות ישראלית מקורית. ניתן להדפיס בכל גודל, ללא סימני חיבור ותפר. ההדפסה חלה על כל סוגי הקירות, לרבות קירות \"שפריץ\", תריסים, קירות עץ וזכוכית- גם כאלו שאינם בעלי מרקם חלק. המחיר לא מושפע ממורכבות התמונה, אלא לפי גודל ההדפסה שתבחרו. תהליך ההדפסה מהיר, נקי כך שלא יפריע לפעילות השוטפת של העסק להדפסת הקיר שלנו יש עמידות ואיכות גבוהה.\r\n\r\nהיא אינה מתקלפת בניגוד למדבקות וטפטים.ההדפסה אינה פוגעת בקיר וניתן להסירה באמצעות שכבת צבע פשוטה.'),
(829, 169, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(830, 169, 'single_slider_seo_1_content', '<h2>כותרת קידום 2</h2>\r\nמדוע לבחור wall style? אנו מקדמים אומנות ישראלית מקורית. ניתן להדפיס בכל גודל, ללא סימני חיבור ותפר. ההדפסה חלה על כל סוגי הקירות, לרבות קירות \"שפריץ\", תריסים, קירות עץ וזכוכית- גם כאלו שאינם בעלי מרקם חלק.\r\n\r\nהמחיר לא מושפע ממורכבות התמונה, אלא לפי גודל ההדפסה שתבחרו. תהליך ההדפסה מהיר, נקי כך שלא יפריע לפעילות השוטפת של העסק להדפסת הקיר שלנו יש עמידות ואיכות גבוהה.'),
(831, 169, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(832, 169, 'faq_item_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם ם היפסום'),
(833, 169, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(834, 169, 'faq_item_0_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(835, 169, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(836, 169, 'faq_item_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם היפסום'),
(837, 169, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(838, 169, 'faq_item_1_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(839, 169, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(840, 169, 'faq_item_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום'),
(841, 169, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(842, 169, 'faq_item_2_faq_answer', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(843, 169, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(844, 170, 'title_tag', ''),
(845, 170, '_title_tag', 'field_5ddbe7577e0e7'),
(846, 170, 'single_slider_seo', ''),
(847, 170, '_single_slider_seo', 'field_5ddbde5499115'),
(848, 170, 'single_gallery', ''),
(849, 170, '_single_gallery', 'field_5ddbe5aea4275'),
(850, 170, 'single_video_slider', ''),
(851, 170, '_single_video_slider', 'field_5ddbe636a4276'),
(852, 172, '_menu_item_type', 'post_type'),
(853, 172, '_menu_item_menu_item_parent', '0'),
(854, 172, '_menu_item_object_id', '12'),
(855, 172, '_menu_item_object', 'page'),
(856, 172, '_menu_item_target', ''),
(857, 172, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(858, 172, '_menu_item_xfn', ''),
(859, 172, '_menu_item_url', ''),
(861, 173, '_menu_item_type', 'post_type'),
(862, 173, '_menu_item_menu_item_parent', '0'),
(863, 173, '_menu_item_object_id', '10'),
(864, 173, '_menu_item_object', 'page'),
(865, 173, '_menu_item_target', ''),
(866, 173, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(867, 173, '_menu_item_xfn', ''),
(868, 173, '_menu_item_url', ''),
(870, 174, '_menu_item_type', 'post_type'),
(871, 174, '_menu_item_menu_item_parent', '0'),
(872, 174, '_menu_item_object_id', '16'),
(873, 174, '_menu_item_object', 'page'),
(874, 174, '_menu_item_target', ''),
(875, 174, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(876, 174, '_menu_item_xfn', ''),
(877, 174, '_menu_item_url', ''),
(879, 175, '_menu_item_type', 'post_type'),
(880, 175, '_menu_item_menu_item_parent', '0'),
(881, 175, '_menu_item_object_id', '8'),
(882, 175, '_menu_item_object', 'page'),
(883, 175, '_menu_item_target', ''),
(884, 175, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(885, 175, '_menu_item_xfn', ''),
(886, 175, '_menu_item_url', ''),
(888, 176, '_menu_item_type', 'post_type'),
(889, 176, '_menu_item_menu_item_parent', '0'),
(890, 176, '_menu_item_object_id', '12'),
(891, 176, '_menu_item_object', 'page'),
(892, 176, '_menu_item_target', ''),
(893, 176, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(894, 176, '_menu_item_xfn', ''),
(895, 176, '_menu_item_url', ''),
(897, 177, '_menu_item_type', 'post_type'),
(898, 177, '_menu_item_menu_item_parent', '0'),
(899, 177, '_menu_item_object_id', '18'),
(900, 177, '_menu_item_object', 'page'),
(901, 177, '_menu_item_target', ''),
(902, 177, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(903, 177, '_menu_item_xfn', ''),
(904, 177, '_menu_item_url', ''),
(906, 178, '_menu_item_type', 'post_type'),
(907, 178, '_menu_item_menu_item_parent', '0'),
(908, 178, '_menu_item_object_id', '16'),
(909, 178, '_menu_item_object', 'page'),
(910, 178, '_menu_item_target', ''),
(911, 178, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(912, 178, '_menu_item_xfn', ''),
(913, 178, '_menu_item_url', ''),
(915, 179, '_menu_item_type', 'post_type'),
(916, 179, '_menu_item_menu_item_parent', '0'),
(917, 179, '_menu_item_object_id', '14'),
(918, 179, '_menu_item_object', 'page'),
(919, 179, '_menu_item_target', ''),
(920, 179, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(921, 179, '_menu_item_xfn', ''),
(922, 179, '_menu_item_url', ''),
(924, 180, '_menu_item_type', 'post_type'),
(925, 180, '_menu_item_menu_item_parent', '0'),
(926, 180, '_menu_item_object_id', '10'),
(927, 180, '_menu_item_object', 'page'),
(928, 180, '_menu_item_target', ''),
(929, 180, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(930, 180, '_menu_item_xfn', ''),
(931, 180, '_menu_item_url', ''),
(933, 181, '_menu_item_type', 'taxonomy'),
(934, 181, '_menu_item_menu_item_parent', '0'),
(935, 181, '_menu_item_object_id', '28'),
(936, 181, '_menu_item_object', 'artist'),
(937, 181, '_menu_item_target', ''),
(938, 181, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(939, 181, '_menu_item_xfn', ''),
(940, 181, '_menu_item_url', ''),
(942, 182, '_menu_item_type', 'taxonomy'),
(943, 182, '_menu_item_menu_item_parent', '0'),
(944, 182, '_menu_item_object_id', '16'),
(945, 182, '_menu_item_object', 'product_cat'),
(946, 182, '_menu_item_target', ''),
(947, 182, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(948, 182, '_menu_item_xfn', ''),
(949, 182, '_menu_item_url', ''),
(951, 183, '_menu_item_type', 'post_type'),
(952, 183, '_menu_item_menu_item_parent', '0'),
(953, 183, '_menu_item_object_id', '125'),
(954, 183, '_menu_item_object', 'product'),
(955, 183, '_menu_item_target', ''),
(956, 183, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(957, 183, '_menu_item_xfn', ''),
(958, 183, '_menu_item_url', ''),
(960, 184, '_menu_item_type', 'taxonomy'),
(961, 184, '_menu_item_menu_item_parent', '0'),
(962, 184, '_menu_item_object_id', '16'),
(963, 184, '_menu_item_object', 'product_cat'),
(964, 184, '_menu_item_target', ''),
(965, 184, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(966, 184, '_menu_item_xfn', ''),
(967, 184, '_menu_item_url', ''),
(969, 185, '_menu_item_type', 'taxonomy'),
(970, 185, '_menu_item_menu_item_parent', '0'),
(971, 185, '_menu_item_object_id', '18'),
(972, 185, '_menu_item_object', 'product_cat'),
(973, 185, '_menu_item_target', ''),
(974, 185, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(975, 185, '_menu_item_xfn', ''),
(976, 185, '_menu_item_url', ''),
(978, 186, '_menu_item_type', 'taxonomy'),
(979, 186, '_menu_item_menu_item_parent', '0'),
(980, 186, '_menu_item_object_id', '20'),
(981, 186, '_menu_item_object', 'product_cat'),
(982, 186, '_menu_item_target', ''),
(983, 186, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(984, 186, '_menu_item_xfn', ''),
(985, 186, '_menu_item_url', ''),
(987, 187, '_menu_item_type', 'taxonomy'),
(988, 187, '_menu_item_menu_item_parent', '0'),
(989, 187, '_menu_item_object_id', '28'),
(990, 187, '_menu_item_object', 'artist'),
(991, 187, '_menu_item_target', ''),
(992, 187, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(993, 187, '_menu_item_xfn', ''),
(994, 187, '_menu_item_url', ''),
(996, 188, '_menu_item_type', 'post_type'),
(997, 188, '_menu_item_menu_item_parent', '0'),
(998, 188, '_menu_item_object_id', '21'),
(999, 188, '_menu_item_object', 'page'),
(1000, 188, '_menu_item_target', ''),
(1001, 188, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1002, 188, '_menu_item_xfn', ''),
(1003, 188, '_menu_item_url', ''),
(1004, 191, 'title_tag', ''),
(1005, 191, '_title_tag', 'field_5ddbe7577e0e7'),
(1006, 191, 'single_slider_seo', ''),
(1007, 191, '_single_slider_seo', 'field_5ddbde5499115'),
(1008, 191, 'single_gallery', ''),
(1009, 191, '_single_gallery', 'field_5ddbe5aea4275'),
(1010, 191, 'single_video_slider', ''),
(1011, 191, '_single_video_slider', 'field_5ddbe636a4276'),
(1012, 14, 'team_form_title', 'השאירו פרטים ונחזור אליכם בהקדם '),
(1013, 14, '_team_form_title', 'field_60f82dbbf03fb'),
(1014, 14, 'faq_title', 'שאלות נפוצות'),
(1015, 14, '_faq_title', 'field_5feb2f82db93b'),
(1016, 14, 'faq_text', 'שאלתם, ענינו, כל השאלות במקום אחד'),
(1017, 14, '_faq_text', 'field_5feb2f95db93c'),
(1018, 14, 'faq_item', '3'),
(1019, 14, '_faq_item', 'field_5feb2faddb93d'),
(1020, 14, 'faq_link', 'a:3:{s:5:\"title\";s:19:\"לכל השאלות\";s:3:\"url\";s:75:\"/%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1021, 14, '_faq_link', 'field_60f82c8971ca0'),
(1022, 192, 'title_tag', ''),
(1023, 192, '_title_tag', 'field_5ddbe7577e0e7'),
(1024, 192, 'single_slider_seo', ''),
(1025, 192, '_single_slider_seo', 'field_5ddbde5499115'),
(1026, 192, 'single_gallery', ''),
(1027, 192, '_single_gallery', 'field_5ddbe5aea4275'),
(1028, 192, 'single_video_slider', ''),
(1029, 192, '_single_video_slider', 'field_5ddbe636a4276'),
(1030, 192, 'team_form_title', ''),
(1031, 192, '_team_form_title', 'field_60f82dbbf03fb'),
(1032, 192, 'faq_title', ''),
(1033, 192, '_faq_title', 'field_5feb2f82db93b'),
(1034, 192, 'faq_text', ''),
(1035, 192, '_faq_text', 'field_5feb2f95db93c'),
(1036, 192, 'faq_item', ''),
(1037, 192, '_faq_item', 'field_5feb2faddb93d'),
(1038, 192, 'faq_link', ''),
(1039, 192, '_faq_link', 'field_60f82c8971ca0'),
(1040, 193, 'title_tag', ''),
(1041, 193, '_title_tag', 'field_5ddbe7577e0e7'),
(1042, 193, 'single_slider_seo', ''),
(1043, 193, '_single_slider_seo', 'field_5ddbde5499115'),
(1044, 193, 'single_gallery', ''),
(1045, 193, '_single_gallery', 'field_5ddbe5aea4275'),
(1046, 193, 'single_video_slider', ''),
(1047, 193, '_single_video_slider', 'field_5ddbe636a4276'),
(1048, 193, 'team_form_title', 'השאירו פרטים ונחזור אליכם בהקדם '),
(1049, 193, '_team_form_title', 'field_60f82dbbf03fb'),
(1050, 193, 'faq_title', ''),
(1051, 193, '_faq_title', 'field_5feb2f82db93b'),
(1052, 193, 'faq_text', ''),
(1053, 193, '_faq_text', 'field_5feb2f95db93c'),
(1054, 193, 'faq_item', ''),
(1055, 193, '_faq_item', 'field_5feb2faddb93d'),
(1056, 193, 'faq_link', ''),
(1057, 193, '_faq_link', 'field_60f82c8971ca0'),
(1058, 194, '_menu_item_type', 'post_type'),
(1059, 194, '_menu_item_menu_item_parent', '0'),
(1060, 194, '_menu_item_object_id', '14'),
(1061, 194, '_menu_item_object', 'page'),
(1062, 194, '_menu_item_target', ''),
(1063, 194, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1064, 194, '_menu_item_xfn', ''),
(1065, 194, '_menu_item_url', ''),
(1067, 188, '_wp_old_date', '2021-07-22'),
(1068, 184, '_wp_old_date', '2021-07-22'),
(1069, 185, '_wp_old_date', '2021-07-22'),
(1070, 186, '_wp_old_date', '2021-07-22'),
(1071, 187, '_wp_old_date', '2021-07-22'),
(1072, 14, 'faq_item_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם ם היפסום?'),
(1073, 14, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1074, 14, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1075, 14, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1076, 14, 'faq_item_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם היפסום?'),
(1077, 14, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1078, 14, 'faq_item_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1079, 14, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1080, 14, 'faq_item_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם?'),
(1081, 14, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1082, 14, 'faq_item_2_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1083, 14, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1084, 195, 'title_tag', ''),
(1085, 195, '_title_tag', 'field_5ddbe7577e0e7'),
(1086, 195, 'single_slider_seo', ''),
(1087, 195, '_single_slider_seo', 'field_5ddbde5499115'),
(1088, 195, 'single_gallery', ''),
(1089, 195, '_single_gallery', 'field_5ddbe5aea4275'),
(1090, 195, 'single_video_slider', ''),
(1091, 195, '_single_video_slider', 'field_5ddbe636a4276'),
(1092, 195, 'team_form_title', 'השאירו פרטים ונחזור אליכם בהקדם '),
(1093, 195, '_team_form_title', 'field_60f82dbbf03fb'),
(1094, 195, 'faq_title', 'שאלות נפוצות'),
(1095, 195, '_faq_title', 'field_5feb2f82db93b'),
(1096, 195, 'faq_text', 'שאלתם, ענינו, כל השאלות במקום אחד'),
(1097, 195, '_faq_text', 'field_5feb2f95db93c'),
(1098, 195, 'faq_item', '3'),
(1099, 195, '_faq_item', 'field_5feb2faddb93d'),
(1100, 195, 'faq_link', ''),
(1101, 195, '_faq_link', 'field_60f82c8971ca0'),
(1102, 195, 'faq_item_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם ם היפסום?'),
(1103, 195, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1104, 195, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1105, 195, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1106, 195, 'faq_item_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם היפסום?'),
(1107, 195, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1108, 195, 'faq_item_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1109, 195, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1110, 195, 'faq_item_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם?'),
(1111, 195, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1112, 195, 'faq_item_2_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1113, 195, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1114, 196, '_edit_last', '1'),
(1115, 196, '_edit_lock', '1627120206:1'),
(1116, 196, '_wp_page_template', 'views/faq.php'),
(1117, 196, 'title_tag', ''),
(1118, 196, '_title_tag', 'field_5ddbe7577e0e7'),
(1119, 196, 'page_faq_block', '3'),
(1120, 196, '_page_faq_block', 'field_60f82ce3088ce'),
(1121, 197, 'title_tag', ''),
(1122, 197, '_title_tag', 'field_5ddbe7577e0e7'),
(1123, 197, 'page_faq_block', ''),
(1124, 197, '_page_faq_block', 'field_60f82ce3088ce'),
(1125, 198, 'title_tag', ''),
(1126, 198, '_title_tag', 'field_5ddbe7577e0e7'),
(1127, 198, 'single_slider_seo', ''),
(1128, 198, '_single_slider_seo', 'field_5ddbde5499115'),
(1129, 198, 'single_gallery', ''),
(1130, 198, '_single_gallery', 'field_5ddbe5aea4275'),
(1131, 198, 'single_video_slider', ''),
(1132, 198, '_single_video_slider', 'field_5ddbe636a4276'),
(1133, 198, 'team_form_title', 'השאירו פרטים ונחזור אליכם בהקדם '),
(1134, 198, '_team_form_title', 'field_60f82dbbf03fb'),
(1135, 198, 'faq_title', 'שאלות נפוצות'),
(1136, 198, '_faq_title', 'field_5feb2f82db93b'),
(1137, 198, 'faq_text', 'שאלתם, ענינו, כל השאלות במקום אחד'),
(1138, 198, '_faq_text', 'field_5feb2f95db93c'),
(1139, 198, 'faq_item', '3'),
(1140, 198, '_faq_item', 'field_5feb2faddb93d'),
(1141, 198, 'faq_link', 'a:3:{s:5:\"title\";s:19:\"לכל השאלות\";s:3:\"url\";s:75:\"/%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1142, 198, '_faq_link', 'field_60f82c8971ca0'),
(1143, 198, 'faq_item_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם ם היפסום?'),
(1144, 198, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1145, 198, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1146, 198, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1147, 198, 'faq_item_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם היפסום?'),
(1148, 198, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1149, 198, 'faq_item_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1150, 198, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1151, 198, 'faq_item_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם?'),
(1152, 198, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1153, 198, 'faq_item_2_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1154, 198, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1155, 199, 'title_tag', ''),
(1156, 199, '_title_tag', 'field_5ddbe7577e0e7'),
(1157, 199, 'single_slider_seo', '2'),
(1158, 199, '_single_slider_seo', 'field_5ddbde5499115'),
(1159, 199, 'single_gallery', ''),
(1160, 199, '_single_gallery', 'field_5ddbe5aea4275'),
(1161, 199, 'single_video_slider', ''),
(1162, 199, '_single_video_slider', 'field_5ddbe636a4276'),
(1163, 199, 'home_about_text', '<h2>מי אנחנו</h2>\r\nwall style מביאה לישראל את הדבר החם בעולם עיצוב הפנים הדפסת אומנות, גרפיקה, תמונות וציורים והכל ישירות על הקיר באיכות גבוהה במיוחד אנו מקדמים אומנות ישראלית מקורית\r\n\r\nועובדים בשיתוף פעולה עם מגוון אמנים, ציירים וצלמים מקצועיים ואיכותיים. אצלינו ניתן לבחור תמונה או ציור, מתוך מאגר נרחב ומגוון המחולק לפי קטגוריות מסודרות, המקלות על השימוש והחיפוש באתר. או לחלופין הדפסה אישית לפי צרכי הלקוח, כגון: לוגו לעסק, הדפסת חזון וערכים, או גרפיקה מותאמת אישית.'),
(1164, 199, '_home_about_text', 'field_60f7e7c1e8bf9'),
(1165, 199, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:13:\"/sample-page/\";s:6:\"target\";s:0:\"\";}'),
(1166, 199, '_home_about_link', 'field_60f7e7eae8bfa'),
(1167, 199, 'home_video_back', ''),
(1168, 199, '_home_video_back', 'field_60f825195b93e'),
(1169, 199, 'home_video_block_img', '166'),
(1170, 199, '_home_video_block_img', 'field_60f825355b93f'),
(1171, 199, 'home_video_block_title', 'אומנות שוברת מסגרות ');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1172, 199, '_home_video_block_title', 'field_60f8254d5b940'),
(1173, 199, 'home_video_block_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(1174, 199, '_home_video_block_text', 'field_60f825645b941'),
(1175, 199, 'home_video_block_links', '2'),
(1176, 199, '_home_video_block_links', 'field_60f8257f5b942'),
(1177, 199, 'home_cats_title', 'בחרו בין הקטגוריות החמות'),
(1178, 199, '_home_cats_title', 'field_60f8273b55681'),
(1179, 199, 'home_cats_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(1180, 199, '_home_cats_text', 'field_60f8274a55682'),
(1181, 199, 'home_cats_chosen', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"16\";i:2;s:2:\"18\";i:3;s:2:\"20\";i:4;s:2:\"17\";}'),
(1182, 199, '_home_cats_chosen', 'field_60f8275b55683'),
(1183, 199, 'home_cats_link', 'a:3:{s:5:\"title\";s:25:\"לכל הקטגוריות\";s:3:\"url\";s:50:\"/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1184, 199, '_home_cats_link', 'field_60f8277455684'),
(1185, 199, 'home_offer_title', 'אומנות שוברת מסגרות רק כאן!'),
(1186, 199, '_home_offer_title', 'field_60f827bd55686'),
(1187, 199, 'home_offer_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(1188, 199, '_home_offer_text', 'field_60f827c755687'),
(1189, 199, 'home_offer_link', 'a:3:{s:5:\"title\";s:32:\"לבחירת תמונה לחצו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1190, 199, '_home_offer_link', 'field_60f827d855688'),
(1191, 199, 'slider_img', '168'),
(1192, 199, '_slider_img', 'field_60f58b1c85127'),
(1193, 199, 'home_prods_title', 'חדש באתר'),
(1194, 199, '_home_prods_title', 'field_60f82b984d2af'),
(1195, 199, 'home_prods', 'a:5:{i:0;s:3:\"145\";i:1;s:3:\"152\";i:2;s:3:\"153\";i:3;s:3:\"154\";i:4;s:3:\"137\";}'),
(1196, 199, '_home_prods', 'field_60f82ba14d2b0'),
(1197, 199, 'home_gallery_title', 'הגלריה שלנו'),
(1198, 199, '_home_gallery_title', 'field_60f82bc04d2b2'),
(1199, 199, 'home_gallery', 'a:4:{i:0;s:3:\"156\";i:1;s:3:\"157\";i:2;s:3:\"158\";i:3;s:3:\"159\";}'),
(1200, 199, '_home_gallery', 'field_60f82bce4d2b3'),
(1201, 199, 'home_gallery_link', 'a:3:{s:5:\"title\";s:19:\"לכל הגלריה\";s:3:\"url\";s:32:\"/%d7%92%d7%9c%d7%a8%d7%99%d7%94/\";s:6:\"target\";s:0:\"\";}'),
(1202, 199, '_home_gallery_link', 'field_60f82be84d2b4'),
(1203, 199, 'faq_title', 'שאלות נפוצות'),
(1204, 199, '_faq_title', 'field_5feb2f82db93b'),
(1205, 199, 'faq_text', 'שאלתם, ענינו, כל השאלות במקום אחד'),
(1206, 199, '_faq_text', 'field_5feb2f95db93c'),
(1207, 199, 'faq_item', '3'),
(1208, 199, '_faq_item', 'field_5feb2faddb93d'),
(1209, 199, 'faq_link', 'a:3:{s:5:\"title\";s:19:\"לכל השאלות\";s:3:\"url\";s:75:\"/%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1210, 199, '_faq_link', 'field_60f82c8971ca0'),
(1211, 199, 'home_video_block_links_0_video_block_link', 'a:3:{s:5:\"title\";s:39:\"לבחירה מהירה של תמונה\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1212, 199, '_home_video_block_links_0_video_block_link', 'field_60f825965b943'),
(1213, 199, 'home_video_block_links_1_video_block_link', 'a:3:{s:5:\"title\";s:40:\"להעלת קובץ תמונה אישי \";s:3:\"url\";s:39:\"/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/\";s:6:\"target\";s:0:\"\";}'),
(1214, 199, '_home_video_block_links_1_video_block_link', 'field_60f825965b943'),
(1215, 199, 'single_slider_seo_0_content', '<h2>כותרת קידום 1</h2>\r\nמדוע לבחור wall style? אנו מקדמים אומנות ישראלית מקורית. ניתן להדפיס בכל גודל, ללא סימני חיבור ותפר. ההדפסה חלה על כל סוגי הקירות, לרבות קירות \"שפריץ\", תריסים, קירות עץ וזכוכית- גם כאלו שאינם בעלי מרקם חלק. המחיר לא מושפע ממורכבות התמונה, אלא לפי גודל ההדפסה שתבחרו. תהליך ההדפסה מהיר, נקי כך שלא יפריע לפעילות השוטפת של העסק להדפסת הקיר שלנו יש עמידות ואיכות גבוהה.\r\n\r\nהיא אינה מתקלפת בניגוד למדבקות וטפטים.ההדפסה אינה פוגעת בקיר וניתן להסירה באמצעות שכבת צבע פשוטה.'),
(1216, 199, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1217, 199, 'single_slider_seo_1_content', '<h2>כותרת קידום 2</h2>\r\nמדוע לבחור wall style? אנו מקדמים אומנות ישראלית מקורית. ניתן להדפיס בכל גודל, ללא סימני חיבור ותפר. ההדפסה חלה על כל סוגי הקירות, לרבות קירות \"שפריץ\", תריסים, קירות עץ וזכוכית- גם כאלו שאינם בעלי מרקם חלק.\r\n\r\nהמחיר לא מושפע ממורכבות התמונה, אלא לפי גודל ההדפסה שתבחרו. תהליך ההדפסה מהיר, נקי כך שלא יפריע לפעילות השוטפת של העסק להדפסת הקיר שלנו יש עמידות ואיכות גבוהה.'),
(1218, 199, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1219, 199, 'faq_item_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם ם היפסום'),
(1220, 199, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1221, 199, 'faq_item_0_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1222, 199, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1223, 199, 'faq_item_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם היפסום'),
(1224, 199, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1225, 199, 'faq_item_1_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1226, 199, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1227, 199, 'faq_item_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום'),
(1228, 199, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1229, 199, 'faq_item_2_faq_answer', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1230, 199, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1231, 200, '_edit_last', '1'),
(1232, 200, '_edit_lock', '1627044486:1'),
(1233, 10, 'contact_block_title', 'דברו איתנו'),
(1234, 10, '_contact_block_title', 'field_60fab67f4dfa1'),
(1235, 10, 'contact_form_title', 'השאירו פרטים ונחזור אליכם בהקדם '),
(1236, 10, '_contact_form_title', 'field_60fab6984dfa2'),
(1237, 203, 'title_tag', ''),
(1238, 203, '_title_tag', 'field_5ddbe7577e0e7'),
(1239, 203, 'single_slider_seo', ''),
(1240, 203, '_single_slider_seo', 'field_5ddbde5499115'),
(1241, 203, 'single_gallery', ''),
(1242, 203, '_single_gallery', 'field_5ddbe5aea4275'),
(1243, 203, 'single_video_slider', ''),
(1244, 203, '_single_video_slider', 'field_5ddbe636a4276'),
(1245, 203, 'contact_block_title', 'דברו איתנו'),
(1246, 203, '_contact_block_title', 'field_60fab67f4dfa1'),
(1247, 203, 'contact_form_title', 'השאירו פרטים ונחזור אליכם בהקדם '),
(1248, 203, '_contact_form_title', 'field_60fab6984dfa2'),
(1250, 7, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(1251, 10, 'facebook_name', 'שם בפייסבוק לורם לורם היפסום'),
(1252, 10, '_facebook_name', 'field_60fabada89aa4'),
(1253, 10, 'instagram_name', 'שם באינסטגרם לורם לורם היפסום'),
(1254, 10, '_instagram_name', 'field_60fabaee89aa5'),
(1255, 206, 'title_tag', ''),
(1256, 206, '_title_tag', 'field_5ddbe7577e0e7'),
(1257, 206, 'single_slider_seo', ''),
(1258, 206, '_single_slider_seo', 'field_5ddbde5499115'),
(1259, 206, 'single_gallery', ''),
(1260, 206, '_single_gallery', 'field_5ddbe5aea4275'),
(1261, 206, 'single_video_slider', ''),
(1262, 206, '_single_video_slider', 'field_5ddbe636a4276'),
(1263, 206, 'contact_block_title', 'דברו איתנו'),
(1264, 206, '_contact_block_title', 'field_60fab67f4dfa1'),
(1265, 206, 'contact_form_title', 'השאירו פרטים ונחזור אליכם בהקדם '),
(1266, 206, '_contact_form_title', 'field_60fab6984dfa2'),
(1267, 206, 'facebook_name', 'שם בפייסבוק לורם לורם היפסום'),
(1268, 206, '_facebook_name', 'field_60fabada89aa4'),
(1269, 206, 'instagram_name', 'שם באינסטגרם לורם לורם היפסום'),
(1270, 206, '_instagram_name', 'field_60fabaee89aa5'),
(1271, 207, '_edit_last', '1'),
(1272, 207, '_edit_lock', '1627064220:1'),
(1273, 207, '_wp_page_template', 'views/artists.php'),
(1274, 207, 'artists_link_page', 'a:3:{s:5:\"title\";s:36:\"הצטרפו לאומנים שלנו\";s:3:\"url\";s:39:\"/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/\";s:6:\"target\";s:0:\"\";}'),
(1275, 207, '_artists_link_page', 'field_60f82ee1e2245'),
(1276, 207, 'title_tag', ''),
(1277, 207, '_title_tag', 'field_5ddbe7577e0e7'),
(1278, 208, 'artists_link_page', ''),
(1279, 208, '_artists_link_page', 'field_60f82ee1e2245'),
(1280, 208, 'title_tag', ''),
(1281, 208, '_title_tag', 'field_5ddbe7577e0e7'),
(1282, 209, 'artists_link_page', 'a:3:{s:5:\"title\";s:36:\"הצטרפו לאומנים שלנו\";s:3:\"url\";s:39:\"/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/\";s:6:\"target\";s:0:\"\";}'),
(1283, 209, '_artists_link_page', 'field_60f82ee1e2245'),
(1284, 209, 'title_tag', ''),
(1285, 209, '_title_tag', 'field_5ddbe7577e0e7'),
(1286, 210, '_menu_item_type', 'post_type'),
(1287, 210, '_menu_item_menu_item_parent', '0'),
(1288, 210, '_menu_item_object_id', '207'),
(1289, 210, '_menu_item_object', 'page'),
(1290, 210, '_menu_item_target', ''),
(1291, 210, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1292, 210, '_menu_item_xfn', ''),
(1293, 210, '_menu_item_url', ''),
(1295, 211, 'artists_link_page', 'a:3:{s:5:\"title\";s:36:\"הצטרפו לאומנים שלנו\";s:3:\"url\";s:39:\"/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/\";s:6:\"target\";s:0:\"\";}'),
(1296, 211, '_artists_link_page', 'field_60f82ee1e2245'),
(1297, 211, 'title_tag', ''),
(1298, 211, '_title_tag', 'field_5ddbe7577e0e7'),
(1299, 212, 'artists_link_page', 'a:3:{s:5:\"title\";s:36:\"הצטרפו לאומנים שלנו\";s:3:\"url\";s:39:\"/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/\";s:6:\"target\";s:0:\"\";}'),
(1300, 212, '_artists_link_page', 'field_60f82ee1e2245'),
(1301, 212, 'title_tag', ''),
(1302, 212, '_title_tag', 'field_5ddbe7577e0e7'),
(1303, 213, '_menu_item_type', 'post_type'),
(1304, 213, '_menu_item_menu_item_parent', '0'),
(1305, 213, '_menu_item_object_id', '125'),
(1306, 213, '_menu_item_object', 'product'),
(1307, 213, '_menu_item_target', ''),
(1308, 213, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1309, 213, '_menu_item_xfn', ''),
(1310, 213, '_menu_item_url', ''),
(1311, 214, '_menu_item_type', 'post_type'),
(1312, 214, '_menu_item_menu_item_parent', '0'),
(1313, 214, '_menu_item_object_id', '196'),
(1314, 214, '_menu_item_object', 'page'),
(1315, 214, '_menu_item_target', ''),
(1316, 214, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1317, 214, '_menu_item_xfn', ''),
(1318, 214, '_menu_item_url', ''),
(1320, 210, '_wp_old_date', '2021-07-23'),
(1321, 188, '_wp_old_date', '2021-07-23'),
(1322, 194, '_wp_old_date', '2021-07-23'),
(1323, 184, '_wp_old_date', '2021-07-23'),
(1324, 185, '_wp_old_date', '2021-07-23'),
(1325, 186, '_wp_old_date', '2021-07-23'),
(1326, 187, '_wp_old_date', '2021-07-23'),
(1327, 213, '_wp_old_date', '2021-07-23'),
(1328, 196, 'page_faq_block_0_faq_page_block_title', ' תת נושא לשאלות 1'),
(1329, 196, '_page_faq_block_0_faq_page_block_title', 'field_60f82cfe088cf'),
(1330, 196, 'page_faq_block_0_faq_item_page_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 1? '),
(1331, 196, '_page_faq_block_0_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1332, 196, 'page_faq_block_0_faq_item_page_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1333, 196, '_page_faq_block_0_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1334, 196, 'page_faq_block_0_faq_item_page_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2? '),
(1335, 196, '_page_faq_block_0_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1336, 196, 'page_faq_block_0_faq_item_page_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1337, 196, '_page_faq_block_0_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1338, 196, 'page_faq_block_0_faq_item_page_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3? '),
(1339, 196, '_page_faq_block_0_faq_item_page_2_faq_question', 'field_60f82cb738ed6'),
(1340, 196, 'page_faq_block_0_faq_item_page_2_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1341, 196, '_page_faq_block_0_faq_item_page_2_faq_answer', 'field_60f82cb738ed7'),
(1342, 196, 'page_faq_block_0_faq_item_page', '3'),
(1343, 196, '_page_faq_block_0_faq_item_page', 'field_60f82cb738ed5'),
(1344, 196, 'page_faq_block_1_faq_page_block_title', ' תת נושא לשאלות 2'),
(1345, 196, '_page_faq_block_1_faq_page_block_title', 'field_60f82cfe088cf'),
(1346, 196, 'page_faq_block_1_faq_item_page_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 1?'),
(1347, 196, '_page_faq_block_1_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1348, 196, 'page_faq_block_1_faq_item_page_0_faq_answer', ''),
(1349, 196, '_page_faq_block_1_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1350, 196, 'page_faq_block_1_faq_item_page_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 2?'),
(1351, 196, '_page_faq_block_1_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1352, 196, 'page_faq_block_1_faq_item_page_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nמוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1353, 196, '_page_faq_block_1_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1354, 196, 'page_faq_block_1_faq_item_page_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 3?'),
(1355, 196, '_page_faq_block_1_faq_item_page_2_faq_question', 'field_60f82cb738ed6'),
(1356, 196, 'page_faq_block_1_faq_item_page_2_faq_answer', 'אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1357, 196, '_page_faq_block_1_faq_item_page_2_faq_answer', 'field_60f82cb738ed7'),
(1358, 196, 'page_faq_block_1_faq_item_page', '3'),
(1359, 196, '_page_faq_block_1_faq_item_page', 'field_60f82cb738ed5'),
(1360, 196, 'page_faq_block_2_faq_page_block_title', ' תת נושא לשאלות 3'),
(1361, 196, '_page_faq_block_2_faq_page_block_title', 'field_60f82cfe088cf'),
(1362, 196, 'page_faq_block_2_faq_item_page_0_faq_question', 'קולורס מונפרד אדנדום סילקוף 1?'),
(1363, 196, '_page_faq_block_2_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1364, 196, 'page_faq_block_2_faq_item_page_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1365, 196, '_page_faq_block_2_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1366, 196, 'page_faq_block_2_faq_item_page_1_faq_question', 'קולורס מונפרד אדנדום סילקוף 2?'),
(1367, 196, '_page_faq_block_2_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1368, 196, 'page_faq_block_2_faq_item_page_1_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1369, 196, '_page_faq_block_2_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1370, 196, 'page_faq_block_2_faq_item_page', '2'),
(1371, 196, '_page_faq_block_2_faq_item_page', 'field_60f82cb738ed5'),
(1372, 215, 'title_tag', ''),
(1373, 215, '_title_tag', 'field_5ddbe7577e0e7'),
(1374, 215, 'page_faq_block', '3'),
(1375, 215, '_page_faq_block', 'field_60f82ce3088ce'),
(1376, 215, 'page_faq_block_0_faq_page_block_title', ' תת נושא לשאלות 1'),
(1377, 215, '_page_faq_block_0_faq_page_block_title', 'field_60f82cfe088cf'),
(1378, 215, 'page_faq_block_0_faq_item_page_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 1? '),
(1379, 215, '_page_faq_block_0_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1380, 215, 'page_faq_block_0_faq_item_page_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1381, 215, '_page_faq_block_0_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1382, 215, 'page_faq_block_0_faq_item_page_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2? '),
(1383, 215, '_page_faq_block_0_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1384, 215, 'page_faq_block_0_faq_item_page_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1385, 215, '_page_faq_block_0_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1386, 215, 'page_faq_block_0_faq_item_page_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3? '),
(1387, 215, '_page_faq_block_0_faq_item_page_2_faq_question', 'field_60f82cb738ed6'),
(1388, 215, 'page_faq_block_0_faq_item_page_2_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1389, 215, '_page_faq_block_0_faq_item_page_2_faq_answer', 'field_60f82cb738ed7'),
(1390, 215, 'page_faq_block_0_faq_item_page', '3'),
(1391, 215, '_page_faq_block_0_faq_item_page', 'field_60f82cb738ed5'),
(1392, 215, 'page_faq_block_1_faq_page_block_title', ' תת נושא לשאלות 2'),
(1393, 215, '_page_faq_block_1_faq_page_block_title', 'field_60f82cfe088cf'),
(1394, 215, 'page_faq_block_1_faq_item_page_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 1?'),
(1395, 215, '_page_faq_block_1_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1396, 215, 'page_faq_block_1_faq_item_page_0_faq_answer', ''),
(1397, 215, '_page_faq_block_1_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1398, 215, 'page_faq_block_1_faq_item_page_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 2?'),
(1399, 215, '_page_faq_block_1_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1400, 215, 'page_faq_block_1_faq_item_page_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nמוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1401, 215, '_page_faq_block_1_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1402, 215, 'page_faq_block_1_faq_item_page_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 3?'),
(1403, 215, '_page_faq_block_1_faq_item_page_2_faq_question', 'field_60f82cb738ed6'),
(1404, 215, 'page_faq_block_1_faq_item_page_2_faq_answer', 'אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1405, 215, '_page_faq_block_1_faq_item_page_2_faq_answer', 'field_60f82cb738ed7'),
(1406, 215, 'page_faq_block_1_faq_item_page', '3'),
(1407, 215, '_page_faq_block_1_faq_item_page', 'field_60f82cb738ed5'),
(1408, 215, 'page_faq_block_2_faq_page_block_title', ' תת נושא לשאלות 3'),
(1409, 215, '_page_faq_block_2_faq_page_block_title', 'field_60f82cfe088cf'),
(1410, 215, 'page_faq_block_2_faq_item_page_0_faq_question', 'קולורס מונפרד אדנדום סילקוף 1?'),
(1411, 215, '_page_faq_block_2_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1412, 215, 'page_faq_block_2_faq_item_page_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1413, 215, '_page_faq_block_2_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1414, 215, 'page_faq_block_2_faq_item_page_1_faq_question', 'קולורס מונפרד אדנדום סילקוף 2?'),
(1415, 215, '_page_faq_block_2_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1416, 215, 'page_faq_block_2_faq_item_page_1_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1417, 215, '_page_faq_block_2_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1418, 215, 'page_faq_block_2_faq_item_page', '2'),
(1419, 215, '_page_faq_block_2_faq_item_page', 'field_60f82cb738ed5'),
(1420, 216, 'title_tag', ''),
(1421, 216, '_title_tag', 'field_5ddbe7577e0e7'),
(1422, 216, 'page_faq_block', '3'),
(1423, 216, '_page_faq_block', 'field_60f82ce3088ce'),
(1424, 216, 'page_faq_block_0_faq_page_block_title', ' תת נושא לשאלות 1'),
(1425, 216, '_page_faq_block_0_faq_page_block_title', 'field_60f82cfe088cf'),
(1426, 216, 'page_faq_block_0_faq_item_page_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 1? '),
(1427, 216, '_page_faq_block_0_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1428, 216, 'page_faq_block_0_faq_item_page_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1429, 216, '_page_faq_block_0_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1430, 216, 'page_faq_block_0_faq_item_page_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2? '),
(1431, 216, '_page_faq_block_0_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1432, 216, 'page_faq_block_0_faq_item_page_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1433, 216, '_page_faq_block_0_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1434, 216, 'page_faq_block_0_faq_item_page_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3? '),
(1435, 216, '_page_faq_block_0_faq_item_page_2_faq_question', 'field_60f82cb738ed6'),
(1436, 216, 'page_faq_block_0_faq_item_page_2_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1437, 216, '_page_faq_block_0_faq_item_page_2_faq_answer', 'field_60f82cb738ed7'),
(1438, 216, 'page_faq_block_0_faq_item_page', '3'),
(1439, 216, '_page_faq_block_0_faq_item_page', 'field_60f82cb738ed5'),
(1440, 216, 'page_faq_block_1_faq_page_block_title', ' תת נושא לשאלות 2'),
(1441, 216, '_page_faq_block_1_faq_page_block_title', 'field_60f82cfe088cf'),
(1442, 216, 'page_faq_block_1_faq_item_page_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 1?'),
(1443, 216, '_page_faq_block_1_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1444, 216, 'page_faq_block_1_faq_item_page_0_faq_answer', ''),
(1445, 216, '_page_faq_block_1_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1446, 216, 'page_faq_block_1_faq_item_page_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 2?'),
(1447, 216, '_page_faq_block_1_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1448, 216, 'page_faq_block_1_faq_item_page_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nמוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1449, 216, '_page_faq_block_1_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1450, 216, 'page_faq_block_1_faq_item_page_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 3?'),
(1451, 216, '_page_faq_block_1_faq_item_page_2_faq_question', 'field_60f82cb738ed6'),
(1452, 216, 'page_faq_block_1_faq_item_page_2_faq_answer', 'אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1453, 216, '_page_faq_block_1_faq_item_page_2_faq_answer', 'field_60f82cb738ed7'),
(1454, 216, 'page_faq_block_1_faq_item_page', '3'),
(1455, 216, '_page_faq_block_1_faq_item_page', 'field_60f82cb738ed5'),
(1456, 216, 'page_faq_block_2_faq_page_block_title', ' תת נושא לשאלות 3'),
(1457, 216, '_page_faq_block_2_faq_page_block_title', 'field_60f82cfe088cf'),
(1458, 216, 'page_faq_block_2_faq_item_page_0_faq_question', 'קולורס מונפרד אדנדום סילקוף 1?'),
(1459, 216, '_page_faq_block_2_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1460, 216, 'page_faq_block_2_faq_item_page_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1461, 216, '_page_faq_block_2_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1462, 216, 'page_faq_block_2_faq_item_page_1_faq_question', 'קולורס מונפרד אדנדום סילקוף 2?'),
(1463, 216, '_page_faq_block_2_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1464, 216, 'page_faq_block_2_faq_item_page_1_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1465, 216, '_page_faq_block_2_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1466, 216, 'page_faq_block_2_faq_item_page', '2'),
(1467, 216, '_page_faq_block_2_faq_item_page', 'field_60f82cb738ed5'),
(1468, 217, 'title_tag', ''),
(1469, 217, '_title_tag', 'field_5ddbe7577e0e7'),
(1470, 217, 'page_faq_block', '3'),
(1471, 217, '_page_faq_block', 'field_60f82ce3088ce'),
(1472, 217, 'page_faq_block_0_faq_page_block_title', ' תת נושא לשאלות 1'),
(1473, 217, '_page_faq_block_0_faq_page_block_title', 'field_60f82cfe088cf'),
(1474, 217, 'page_faq_block_0_faq_item_page_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 1? '),
(1475, 217, '_page_faq_block_0_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1476, 217, 'page_faq_block_0_faq_item_page_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1477, 217, '_page_faq_block_0_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1478, 217, 'page_faq_block_0_faq_item_page_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2? '),
(1479, 217, '_page_faq_block_0_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1480, 217, 'page_faq_block_0_faq_item_page_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1481, 217, '_page_faq_block_0_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1482, 217, 'page_faq_block_0_faq_item_page_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3? '),
(1483, 217, '_page_faq_block_0_faq_item_page_2_faq_question', 'field_60f82cb738ed6'),
(1484, 217, 'page_faq_block_0_faq_item_page_2_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1485, 217, '_page_faq_block_0_faq_item_page_2_faq_answer', 'field_60f82cb738ed7'),
(1486, 217, 'page_faq_block_0_faq_item_page', '3'),
(1487, 217, '_page_faq_block_0_faq_item_page', 'field_60f82cb738ed5'),
(1488, 217, 'page_faq_block_1_faq_page_block_title', ' תת נושא לשאלות 2'),
(1489, 217, '_page_faq_block_1_faq_page_block_title', 'field_60f82cfe088cf'),
(1490, 217, 'page_faq_block_1_faq_item_page_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 1?'),
(1491, 217, '_page_faq_block_1_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1492, 217, 'page_faq_block_1_faq_item_page_0_faq_answer', ''),
(1493, 217, '_page_faq_block_1_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1494, 217, 'page_faq_block_1_faq_item_page_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 2?'),
(1495, 217, '_page_faq_block_1_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1496, 217, 'page_faq_block_1_faq_item_page_1_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nמוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1497, 217, '_page_faq_block_1_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1498, 217, 'page_faq_block_1_faq_item_page_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום 3?'),
(1499, 217, '_page_faq_block_1_faq_item_page_2_faq_question', 'field_60f82cb738ed6'),
(1500, 217, 'page_faq_block_1_faq_item_page_2_faq_answer', 'אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1501, 217, '_page_faq_block_1_faq_item_page_2_faq_answer', 'field_60f82cb738ed7'),
(1502, 217, 'page_faq_block_1_faq_item_page', '3'),
(1503, 217, '_page_faq_block_1_faq_item_page', 'field_60f82cb738ed5'),
(1504, 217, 'page_faq_block_2_faq_page_block_title', ' תת נושא לשאלות 3'),
(1505, 217, '_page_faq_block_2_faq_page_block_title', 'field_60f82cfe088cf'),
(1506, 217, 'page_faq_block_2_faq_item_page_0_faq_question', 'קולורס מונפרד אדנדום סילקוף 1?'),
(1507, 217, '_page_faq_block_2_faq_item_page_0_faq_question', 'field_60f82cb738ed6'),
(1508, 217, 'page_faq_block_2_faq_item_page_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1509, 217, '_page_faq_block_2_faq_item_page_0_faq_answer', 'field_60f82cb738ed7'),
(1510, 217, 'page_faq_block_2_faq_item_page_1_faq_question', 'קולורס מונפרד אדנדום סילקוף 2?'),
(1511, 217, '_page_faq_block_2_faq_item_page_1_faq_question', 'field_60f82cb738ed6'),
(1512, 217, 'page_faq_block_2_faq_item_page_1_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1513, 217, '_page_faq_block_2_faq_item_page_1_faq_answer', 'field_60f82cb738ed7'),
(1514, 217, 'page_faq_block_2_faq_item_page', '2'),
(1515, 217, '_page_faq_block_2_faq_item_page', 'field_60f82cb738ed5'),
(1516, 218, 'title_tag', ''),
(1517, 218, '_title_tag', 'field_5ddbe7577e0e7'),
(1518, 218, 'single_slider_seo', ''),
(1519, 218, '_single_slider_seo', 'field_5ddbde5499115'),
(1520, 218, 'single_gallery', ''),
(1521, 218, '_single_gallery', 'field_5ddbe5aea4275'),
(1522, 218, 'single_video_slider', ''),
(1523, 218, '_single_video_slider', 'field_5ddbe636a4276'),
(1524, 219, '_wp_attached_file', '2021/07/gallery-img-1.png'),
(1525, 219, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:211;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-1-150x300.png\";s:5:\"width\";i:150;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:23:\"gallery-img-1-47x94.png\";s:5:\"width\";i:47;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-1-211x300.png\";s:5:\"width\";i:211;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-1-211x300.png\";s:5:\"width\";i:211;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1526, 220, '_wp_attached_file', '2021/07/gallery-img-2.png'),
(1527, 220, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:581;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-2-300x217.png\";s:5:\"width\";i:300;s:6:\"height\";i:217;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"gallery-img-2-130x94.png\";s:5:\"width\";i:130;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1528, 221, '_wp_attached_file', '2021/07/gallery-img-3.png'),
(1529, 221, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:581;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-3-300x217.png\";s:5:\"width\";i:300;s:6:\"height\";i:217;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"gallery-img-3-130x94.png\";s:5:\"width\";i:130;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1530, 222, '_wp_attached_file', '2021/07/gallery-img-4.png'),
(1531, 222, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:523;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-4-300x241.png\";s:5:\"width\";i:300;s:6:\"height\";i:241;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"gallery-img-4-117x94.png\";s:5:\"width\";i:117;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1532, 223, '_wp_attached_file', '2021/07/gallery-img-5.png');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1533, 223, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:434;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-5-300x291.png\";s:5:\"width\";i:300;s:6:\"height\";i:291;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:23:\"gallery-img-5-97x94.png\";s:5:\"width\";i:97;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1534, 224, '_wp_attached_file', '2021/07/gallery-img-6.png'),
(1535, 224, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:581;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-6-300x217.png\";s:5:\"width\";i:300;s:6:\"height\";i:217;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"gallery-img-6-130x94.png\";s:5:\"width\";i:130;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1536, 225, '_wp_attached_file', '2021/07/gallery-img-7.png'),
(1537, 225, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:581;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-7.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-7-300x217.png\";s:5:\"width\";i:300;s:6:\"height\";i:217;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"gallery-img-7-130x94.png\";s:5:\"width\";i:130;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1538, 226, '_wp_attached_file', '2021/07/gallery-img-8.png'),
(1539, 226, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-8.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-8-214x300.png\";s:5:\"width\";i:214;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:23:\"gallery-img-8-67x94.png\";s:5:\"width\";i:67;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1540, 227, '_wp_attached_file', '2021/07/gallery-img-9.png'),
(1541, 227, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:224;s:6:\"height\";i:421;s:4:\"file\";s:25:\"2021/07/gallery-img-9.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gallery-img-9-160x300.png\";s:5:\"width\";i:160;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:23:\"gallery-img-9-50x94.png\";s:5:\"width\";i:50;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"gallery-img-9-224x300.png\";s:5:\"width\";i:224;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"gallery-img-9-224x300.png\";s:5:\"width\";i:224;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gallery-img-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1542, 228, '_wp_attached_file', '2021/07/gallery-img-10.png'),
(1543, 228, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:581;s:6:\"height\";i:421;s:4:\"file\";s:26:\"2021/07/gallery-img-10.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"gallery-img-10-300x217.png\";s:5:\"width\";i:300;s:6:\"height\";i:217;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-10-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:25:\"gallery-img-10-130x94.png\";s:5:\"width\";i:130;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"gallery-img-10-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-10-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"gallery-img-10-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-10-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1544, 229, '_wp_attached_file', '2021/07/gallery-img-11.png'),
(1545, 229, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:581;s:6:\"height\";i:421;s:4:\"file\";s:26:\"2021/07/gallery-img-11.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"gallery-img-11-300x217.png\";s:5:\"width\";i:300;s:6:\"height\";i:217;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-11-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:25:\"gallery-img-11-130x94.png\";s:5:\"width\";i:130;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"gallery-img-11-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-11-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"gallery-img-11-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-11-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1546, 230, '_wp_attached_file', '2021/07/gallery-img-12.png'),
(1547, 230, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:510;s:6:\"height\";i:421;s:4:\"file\";s:26:\"2021/07/gallery-img-12.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"gallery-img-12-300x248.png\";s:5:\"width\";i:300;s:6:\"height\";i:248;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-12-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:25:\"gallery-img-12-114x94.png\";s:5:\"width\";i:114;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"gallery-img-12-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-12-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"gallery-img-12-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"gallery-img-12-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1548, 231, 'title_tag', ''),
(1549, 231, '_title_tag', 'field_5ddbe7577e0e7'),
(1550, 231, 'single_slider_seo', ''),
(1551, 231, '_single_slider_seo', 'field_5ddbde5499115'),
(1552, 231, 'single_gallery', 'a:16:{i:0;s:3:\"219\";i:1;s:3:\"220\";i:2;s:3:\"221\";i:3;s:3:\"222\";i:4;s:3:\"223\";i:5;s:3:\"224\";i:6;s:3:\"225\";i:7;s:3:\"226\";i:8;s:3:\"227\";i:9;s:3:\"228\";i:10;s:3:\"229\";i:11;s:3:\"230\";i:12;s:3:\"148\";i:13;s:3:\"147\";i:14;s:3:\"159\";i:15;s:3:\"158\";}'),
(1553, 231, '_single_gallery', 'field_5ddbe5aea4275'),
(1554, 231, 'single_video_slider', ''),
(1555, 231, '_single_video_slider', 'field_5ddbe636a4276'),
(1557, 6, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(1558, 232, '_wp_attached_file', '2021/07/sample-video.mp4'),
(1559, 232, '_wp_attachment_metadata', 'a:10:{s:8:\"filesize\";i:3114374;s:9:\"mime_type\";s:9:\"video/mp4\";s:6:\"length\";i:31;s:16:\"length_formatted\";s:4:\"0:31\";s:5:\"width\";i:640;s:6:\"height\";i:360;s:10:\"fileformat\";s:3:\"mp4\";s:10:\"dataformat\";s:9:\"quicktime\";s:5:\"audio\";a:7:{s:10:\"dataformat\";s:3:\"mp4\";s:5:\"codec\";s:19:\"ISO/IEC 14496-3 AAC\";s:11:\"sample_rate\";d:48000;s:8:\"channels\";i:2;s:15:\"bits_per_sample\";i:16;s:8:\"lossless\";b:0;s:11:\"channelmode\";s:6:\"stereo\";}s:17:\"created_timestamp\";i:1438938785;}'),
(1560, 233, 'title_tag', ''),
(1561, 233, '_title_tag', 'field_5ddbe7577e0e7'),
(1562, 233, 'single_slider_seo', '2'),
(1563, 233, '_single_slider_seo', 'field_5ddbde5499115'),
(1564, 233, 'single_gallery', ''),
(1565, 233, '_single_gallery', 'field_5ddbe5aea4275'),
(1566, 233, 'single_video_slider', ''),
(1567, 233, '_single_video_slider', 'field_5ddbe636a4276'),
(1568, 233, 'home_about_text', '<h2>מי אנחנו</h2>\r\nwall style מביאה לישראל את הדבר החם בעולם עיצוב הפנים הדפסת אומנות, גרפיקה, תמונות וציורים והכל ישירות על הקיר באיכות גבוהה במיוחד אנו מקדמים אומנות ישראלית מקורית\r\n\r\nועובדים בשיתוף פעולה עם מגוון אמנים, ציירים וצלמים מקצועיים ואיכותיים. אצלינו ניתן לבחור תמונה או ציור, מתוך מאגר נרחב ומגוון המחולק לפי קטגוריות מסודרות, המקלות על השימוש והחיפוש באתר. או לחלופין הדפסה אישית לפי צרכי הלקוח, כגון: לוגו לעסק, הדפסת חזון וערכים, או גרפיקה מותאמת אישית.'),
(1569, 233, '_home_about_text', 'field_60f7e7c1e8bf9'),
(1570, 233, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:13:\"/sample-page/\";s:6:\"target\";s:0:\"\";}'),
(1571, 233, '_home_about_link', 'field_60f7e7eae8bfa'),
(1572, 233, 'home_video_back', '232'),
(1573, 233, '_home_video_back', 'field_60f825195b93e'),
(1574, 233, 'home_video_block_img', '166'),
(1575, 233, '_home_video_block_img', 'field_60f825355b93f'),
(1576, 233, 'home_video_block_title', 'אומנות שוברת מסגרות '),
(1577, 233, '_home_video_block_title', 'field_60f8254d5b940'),
(1578, 233, 'home_video_block_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(1579, 233, '_home_video_block_text', 'field_60f825645b941'),
(1580, 233, 'home_video_block_links', '2'),
(1581, 233, '_home_video_block_links', 'field_60f8257f5b942'),
(1582, 233, 'home_cats_title', 'בחרו בין הקטגוריות החמות'),
(1583, 233, '_home_cats_title', 'field_60f8273b55681'),
(1584, 233, 'home_cats_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(1585, 233, '_home_cats_text', 'field_60f8274a55682'),
(1586, 233, 'home_cats_chosen', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"16\";i:2;s:2:\"18\";i:3;s:2:\"20\";i:4;s:2:\"17\";}'),
(1587, 233, '_home_cats_chosen', 'field_60f8275b55683'),
(1588, 233, 'home_cats_link', 'a:3:{s:5:\"title\";s:25:\"לכל הקטגוריות\";s:3:\"url\";s:50:\"/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1589, 233, '_home_cats_link', 'field_60f8277455684'),
(1590, 233, 'home_offer_title', 'אומנות שוברת מסגרות רק כאן!'),
(1591, 233, '_home_offer_title', 'field_60f827bd55686'),
(1592, 233, 'home_offer_text', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים '),
(1593, 233, '_home_offer_text', 'field_60f827c755687'),
(1594, 233, 'home_offer_link', 'a:3:{s:5:\"title\";s:32:\"לבחירת תמונה לחצו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1595, 233, '_home_offer_link', 'field_60f827d855688'),
(1596, 233, 'slider_img', '168'),
(1597, 233, '_slider_img', 'field_60f58b1c85127'),
(1598, 233, 'home_prods_title', 'חדש באתר'),
(1599, 233, '_home_prods_title', 'field_60f82b984d2af'),
(1600, 233, 'home_prods', 'a:5:{i:0;s:3:\"145\";i:1;s:3:\"152\";i:2;s:3:\"153\";i:3;s:3:\"154\";i:4;s:3:\"137\";}'),
(1601, 233, '_home_prods', 'field_60f82ba14d2b0'),
(1602, 233, 'home_gallery_title', 'הגלריה שלנו'),
(1603, 233, '_home_gallery_title', 'field_60f82bc04d2b2'),
(1604, 233, 'home_gallery', 'a:4:{i:0;s:3:\"156\";i:1;s:3:\"157\";i:2;s:3:\"158\";i:3;s:3:\"159\";}'),
(1605, 233, '_home_gallery', 'field_60f82bce4d2b3'),
(1606, 233, 'home_gallery_link', 'a:3:{s:5:\"title\";s:19:\"לכל הגלריה\";s:3:\"url\";s:32:\"/%d7%92%d7%9c%d7%a8%d7%99%d7%94/\";s:6:\"target\";s:0:\"\";}'),
(1607, 233, '_home_gallery_link', 'field_60f82be84d2b4'),
(1608, 233, 'faq_title', 'שאלות נפוצות'),
(1609, 233, '_faq_title', 'field_5feb2f82db93b'),
(1610, 233, 'faq_text', 'שאלתם, ענינו, כל השאלות במקום אחד'),
(1611, 233, '_faq_text', 'field_5feb2f95db93c'),
(1612, 233, 'faq_item', '3'),
(1613, 233, '_faq_item', 'field_5feb2faddb93d'),
(1614, 233, 'faq_link', 'a:3:{s:5:\"title\";s:19:\"לכל השאלות\";s:3:\"url\";s:75:\"/%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1615, 233, '_faq_link', 'field_60f82c8971ca0'),
(1616, 233, 'home_video_block_links_0_video_block_link', 'a:3:{s:5:\"title\";s:39:\"לבחירה מהירה של תמונה\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1617, 233, '_home_video_block_links_0_video_block_link', 'field_60f825965b943'),
(1618, 233, 'home_video_block_links_1_video_block_link', 'a:3:{s:5:\"title\";s:40:\"להעלת קובץ תמונה אישי \";s:3:\"url\";s:39:\"/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/\";s:6:\"target\";s:0:\"\";}'),
(1619, 233, '_home_video_block_links_1_video_block_link', 'field_60f825965b943'),
(1620, 233, 'single_slider_seo_0_content', '<h2>כותרת קידום 1</h2>\r\nמדוע לבחור wall style? אנו מקדמים אומנות ישראלית מקורית. ניתן להדפיס בכל גודל, ללא סימני חיבור ותפר. ההדפסה חלה על כל סוגי הקירות, לרבות קירות \"שפריץ\", תריסים, קירות עץ וזכוכית- גם כאלו שאינם בעלי מרקם חלק. המחיר לא מושפע ממורכבות התמונה, אלא לפי גודל ההדפסה שתבחרו. תהליך ההדפסה מהיר, נקי כך שלא יפריע לפעילות השוטפת של העסק להדפסת הקיר שלנו יש עמידות ואיכות גבוהה.\r\n\r\nהיא אינה מתקלפת בניגוד למדבקות וטפטים.ההדפסה אינה פוגעת בקיר וניתן להסירה באמצעות שכבת צבע פשוטה.'),
(1621, 233, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1622, 233, 'single_slider_seo_1_content', '<h2>כותרת קידום 2</h2>\r\nמדוע לבחור wall style? אנו מקדמים אומנות ישראלית מקורית. ניתן להדפיס בכל גודל, ללא סימני חיבור ותפר. ההדפסה חלה על כל סוגי הקירות, לרבות קירות \"שפריץ\", תריסים, קירות עץ וזכוכית- גם כאלו שאינם בעלי מרקם חלק.\r\n\r\nהמחיר לא מושפע ממורכבות התמונה, אלא לפי גודל ההדפסה שתבחרו. תהליך ההדפסה מהיר, נקי כך שלא יפריע לפעילות השוטפת של העסק להדפסת הקיר שלנו יש עמידות ואיכות גבוהה.'),
(1623, 233, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1624, 233, 'faq_item_0_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם ם היפסום'),
(1625, 233, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1626, 233, 'faq_item_0_faq_answer', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1627, 233, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1628, 233, 'faq_item_1_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם היפסוםלורם היפסום'),
(1629, 233, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1630, 233, 'faq_item_1_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1631, 233, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1632, 233, 'faq_item_2_faq_question', 'שאלה לדוגמא, לורם היפסום לורם לטורם לורם לורם היוםלורם היפסום'),
(1633, 233, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1634, 233, 'faq_item_2_faq_answer', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1635, 233, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1636, 234, 'title_tag', ''),
(1637, 234, '_title_tag', 'field_5ddbe7577e0e7'),
(1638, 234, 'single_slider_seo', ''),
(1639, 234, '_single_slider_seo', 'field_5ddbde5499115'),
(1640, 234, 'single_gallery', 'a:24:{i:0;s:3:\"219\";i:1;s:3:\"220\";i:2;s:3:\"221\";i:3;s:3:\"222\";i:4;s:3:\"223\";i:5;s:3:\"224\";i:6;s:3:\"225\";i:7;s:3:\"226\";i:8;s:3:\"227\";i:9;s:3:\"228\";i:10;s:3:\"229\";i:11;s:3:\"230\";i:12;s:3:\"148\";i:13;s:3:\"147\";i:14;s:3:\"159\";i:15;s:3:\"158\";i:16;s:3:\"149\";i:17;s:3:\"157\";i:18;s:3:\"133\";i:19;s:3:\"132\";i:20;s:3:\"131\";i:21;s:3:\"103\";i:22;s:3:\"108\";i:23;s:3:\"106\";}'),
(1641, 234, '_single_gallery', 'field_5ddbe5aea4275'),
(1642, 234, 'single_video_slider', ''),
(1643, 234, '_single_video_slider', 'field_5ddbe636a4276');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_posts`
--

CREATE TABLE `fmn_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_posts`
--

INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-07-18 18:14:07', '2021-07-18 15:14:07', '<!-- wp:paragraph -->\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.</p>\n<!-- /wp:paragraph -->', 'שלום עולם!', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d', '', '', '2021-07-18 18:14:07', '2021-07-18 15:14:07', '', 0, 'http://yair:8888/?p=1', 0, 'post', '', 1),
(2, 1, '2021-07-18 18:14:07', '2021-07-18 15:14:07', '<!-- wp:paragraph -->\n<p>זהו עמוד לדוגמה. הוא שונה מפוסט רגיל בבלוג מכיוון שהוא יישאר במקום אחד ויופיע בתפריט הניווט באתר (ברוב התבניות). רוב האנשים מתחילים עם עמוד אודות המציג אותם למבקרים חדשים באתר. הנה תוכן לדוגמה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>אהלן! אני סטודנט לכלכלה ביום, אך מבלה את הלילות בהגשמת החלום שלי להיות שחקן תאטרון. אני גר ביפו, יש לי כלב נהדר בשם שוקו, אני אוהב ערק אשכוליות בשישי בצהריים (במיוחד תוך כדי משחק שש-בש על חוף הים). ברוכים הבאים לאתר שלי!</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>... או משהו כזה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>החברה א.א. שומדברים נוסדה בשנת 1971, והיא מספקת שומדברים איכותיים לציבור מאז. א.א. שומדברים ממוקמת בעיר תקוות-ים, מעסיקה מעל 2,000 אנשים ועושה כל מיני דברים מדהים עבור הקהילה התקוות-ימית.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>כמשתמש וורדפרס חדש, כדאי לבקר <a href=\"http://yair:8888/wp-admin/\">בלוח הבקרה שלך</a> כדי למחוק את העמוד הזה, וליצור עמודים חדשים עבור התוכן שלך. שיהיה בכיף!</p>\n<!-- /wp:paragraph -->', 'עמוד לדוגמא', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2021-07-18 18:14:07', '2021-07-18 15:14:07', '', 0, 'http://yair:8888/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-07-18 18:14:07', '2021-07-18 15:14:07', '<!-- wp:heading --><h2>מי אנחנו</h2><!-- /wp:heading --><!-- wp:paragraph --><p>כתובת האתר שלנו היא: http://yair:8888.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>מה המידע האישי שאנו אוספים ומדוע אנחנו אוספים אותו</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>תגובות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כאשר המבקרים משאירים תגובות באתר אנו אוספים את הנתונים המוצגים בטופס התגובה, ובנוסף גם את כתובת ה-IP של המבקר, ואת מחרוזת ה-user agent של הדפדפן שלו כדי לסייע בזיהוי תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>יתכן ונעביר מחרוזת אנונימית שנוצרה מכתובת הדואר האלקטרוני שלך (הנקראת גם hash) לשירות Gravatar כדי לראות אם הנך חבר/ה בשירות. מדיניות הפרטיות של שירות Gravatar זמינה כאן: https://automattic.com/privacy/. לאחר אישור התגובה שלך, תמונת הפרופיל שלך גלויה לציבור בהקשר של התגובה שלך.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>מדיה</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בהעלאה של תמונות לאתר, מומלץ להימנע מהעלאת תמונות עם נתוני מיקום מוטבעים (EXIF GPS). המבקרים באתר יכולים להוריד ולחלץ את כל נתוני מיקום מהתמונות באתר.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>טפסי יצירת קשר</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>עוגיות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בכתיבת תגובה באתר שלנו, באפשרותך להחליט אם לאפשר לנו לשמור את השם שלך, כתובת האימייל שלך וכתובת האתר שלך בקבצי עוגיות (cookies). השמירה תתבצע לנוחיותך, על מנת שלא יהיה צורך למלא את הפרטים שלך שוב בכתיבת תגובה נוספת. קבצי העוגיות ישמרו לשנה.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה מבקר בעמוד ההתחברות של האתר, נגדיר קובץ עוגיה זמני על מנת לקבוע האם הדפדפן שלך מקבל קבצי עוגיות. קובץ עוגיה זה אינו מכיל נתונים אישיים והוא נמחק בעת סגירת הדפדפן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>כאשר תתחבר, אנחנו גם נגדיר מספר \'עוגיות\' על מנת לשמור את פרטי ההתחברות שלך ואת בחירות התצוגה שלך. עוגיות התחברות תקפות ליומיים, ועוגיות אפשרויות מסך תקפות לשנה. אם תבחר באפשרות &quot;זכור אותי&quot;, פרטי ההתחברות שלך יהיו תקפים למשך שבועיים. אם תתנתק מהחשבון שלך, עוגיות ההתחברות יימחקו.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה עורך או מפרסם מאמר, קובץ \'עוגיה\' נוסף יישמר בדפדפן שלך. קובץ \'עוגיה\' זה אינו כולל נתונים אישיים ופשוט מציין את מזהה הפוסט של המאמר. הוא יפוג לאחר יום אחד.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>תוכן מוטמע מאתרים אחרים</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כתבות או פוסטים באתר זה עשויים לכלול תוכן מוטבע (לדוגמה, קטעי וידאו, תמונות, מאמרים, וכו\'). תוכן מוטבע מאתרי אינטרנט אחרים דינו כביקור הקורא באתרי האינטרנט מהם מוטבע התוכן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אתרים אלו עשויים לאסוף נתונים אודותיך, להשתמש בקבצי \'עוגיות\', להטמיע מעקב של צד שלישי נוסף, ולנטר את האינטראקציה שלך עם תוכן מוטמע זה, לרבות מעקב אחר האינטראקציה שלך עם התוכן המוטמע, אם יש לך חשבון ואתה מחובר לאתר זה.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>אנליטיקה</h3><!-- /wp:heading --><!-- wp:heading --><h2>עם מי אנו חולקים את המידע שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>משך הזמן בו נשמור את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>במידה ותגיב/י על תוכן באתר, התגובה והנתונים אודותיה יישמרו ללא הגבלת זמן, כדי שנוכל לזהות ולאשר את כל התגובות העוקבות באופן אוטומטי.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>עבור משתמשים רשומים באתר (במידה ויש כאלה) אנו מאחסנים גם את המידע האישי שהם מספקים בפרופיל המשתמש שלהם. כל המשתמשים יכולים לראות, לערוך או למחוק את המידע האישי שלהם בכל עת (פרט לשם המשתמש אותו לא ניתן לשנות). גם מנהלי האתר יכולים לראות ולערוך מידע זה.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>אילו זכויות יש לך על המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>אם יש לך חשבון באתר זה, או שהשארת תגובות באתר, באפשרותך לבקש לקבל קובץ של הנתונים האישיים שאנו מחזיקים לגביך, כולל כל הנתונים שסיפקת לנו. באפשרותך גם לבקש שנמחק כל מידע אישי שאנו מחזיקים לגביך. הדבר אינו כולל נתונים שאנו מחויבים לשמור למטרות מנהליות, משפטיות או ביטחוניות.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>להיכן אנו שולחים את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>תגובות מבקרים עלולות להיבדק על ידי שירות אוטומטי למניעת תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>פרטי ההתקשרות שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>מידע נוסף</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>כיצד אנו מגינים על המידע שלך</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>אילו הליכי פרצת נתונים יש לנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>מאילו גופי צד-שלישי אנו מקבלים מידע</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>קבלת החלטות אוטומטיות ו/או יצירת פרופילים שאנו עושים עם נתוני המשתמשים שלנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>דרישות גילוי רגולטוריות של התעשייה </h3><!-- /wp:heading -->', 'מדיניות פרטיות', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-07-18 18:14:07', '2021-07-18 15:14:07', '', 0, 'http://yair:8888/?page_id=3', 0, 'page', '', 0),
(4, 1, '2021-07-18 18:14:16', '0000-00-00 00:00:00', '', 'טיוטה משמירה אוטומטית', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-07-18 18:14:16', '0000-00-00 00:00:00', '', 0, 'http://yair:8888/?p=4', 0, 'post', '', 0),
(6, 1, '2021-07-19 10:24:00', '2021-07-19 07:24:00', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-12\">\r\n			[text* text-532 placeholder \"שם:\"]\r\n	</div>\r\n    <div class=\"col-12\">\r\n			[email* email-611 placeholder \"מייל:\"]\r\n	</div>\r\n    <div class=\"col-12\">\r\n			[submit \"שלחו אלי דיוור\"]\r\n	</div>\r\n	    <div class=\"col-12 check-col\">\r\n				[checkbox* checkbox-242 use_label_element \"אישור קבלת דיוור למייל לורם\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@yair>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@yair>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'פוטר', '', 'publish', 'closed', 'closed', '', '%d7%a4%d7%95%d7%98%d7%a8', '', '', '2021-07-24 21:59:58', '2021-07-24 18:59:58', '', 0, 'http://yair:8888/?post_type=wpcf7_contact_form&#038;p=6', 0, 'wpcf7_contact_form', '', 0),
(7, 1, '2021-07-19 10:25:52', '2021-07-19 07:25:52', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-md-6 col-12\">\r\n		[text* text-824 placeholder \"שם:\"]\r\n	</div>\r\n    <div class=\"col-md-6 col-12\">\r\n			[tel* tel-556 placeholder \"טלפון:\"]\r\n	</div>\r\n	    <div class=\"col-12\">\r\n				[email email-36 placeholder \"מייל:\"]\r\n	</div>\r\n	    <div class=\"col-12\">\r\n				[textarea textarea-551 placeholder \"תוכן הפניה:\"]\r\n	</div>\r\n    <div class=\"col-md-auto col-12\">\r\n			[submit \"חזרו אלי בהקדם\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@yair>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@yair>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'צור קשר, מכירה לאומן', '', 'publish', 'closed', 'closed', '', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', '', '', '2021-07-23 15:45:18', '2021-07-23 12:45:18', '', 0, 'http://yair:8888/?post_type=wpcf7_contact_form&#038;p=7', 0, 'wpcf7_contact_form', '', 0),
(8, 1, '2021-07-19 16:50:40', '2021-07-19 13:50:40', '', 'דף הבית', '', 'publish', 'closed', 'closed', '', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', '', '', '2021-07-25 16:38:24', '2021-07-25 13:38:24', '', 0, 'http://yair:8888/?page_id=8', 0, 'page', '', 0),
(9, 1, '2021-07-19 16:50:40', '2021-07-19 13:50:40', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2021-07-19 16:50:40', '2021-07-19 13:50:40', '', 8, 'http://yair:8888/?p=9', 0, 'revision', '', 0),
(10, 1, '2021-07-19 16:51:00', '2021-07-19 13:51:00', '', 'צור קשר', '', 'publish', 'closed', 'closed', '', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', '', '', '2021-07-23 15:50:53', '2021-07-23 12:50:53', '', 0, 'http://yair:8888/?page_id=10', 0, 'page', '', 0),
(11, 1, '2021-07-19 16:51:00', '2021-07-19 13:51:00', '', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2021-07-19 16:51:00', '2021-07-19 13:51:00', '', 10, 'http://yair:8888/?p=11', 0, 'revision', '', 0),
(12, 1, '2021-07-19 16:57:04', '2021-07-19 13:57:04', 'לורם היפסום בוחרים מזמינים ואנחנו על הקיר מדפיסים', 'גלריה', '', 'publish', 'closed', 'closed', '', '%d7%92%d7%9c%d7%a8%d7%99%d7%94', '', '', '2021-07-25 17:20:25', '2021-07-25 14:20:25', '', 0, 'http://yair:8888/?page_id=12', 0, 'page', '', 0),
(13, 1, '2021-07-19 16:57:04', '2021-07-19 13:57:04', '', 'גלריה', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-07-19 16:57:04', '2021-07-19 13:57:04', '', 12, 'http://yair:8888/?p=13', 0, 'revision', '', 0),
(14, 1, '2021-07-19 16:57:24', '2021-07-19 13:57:24', '<h2>הצטרפו אלינו</h2>\r\n<h4>וקבלו במה להדפסת יצירות שלכם על הקיר</h4>\r\nחוצים. קלאצי סחטיר בלובק. איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר נפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.\r\n\r\nלפמעט מוסן מנת קולורס מונפרד אדנדום סילקוף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג', 'מכירה לאומן', '', 'publish', 'closed', 'closed', '', '%d7%9e%d7%9b%d7%99%d7%a8%d7%94-%d7%9c%d7%90%d7%95%d7%9e%d7%9f', '', '', '2021-07-23 13:30:07', '2021-07-23 10:30:07', '', 0, 'http://yair:8888/?page_id=14', 0, 'page', '', 0),
(15, 1, '2021-07-19 16:57:24', '2021-07-19 13:57:24', '', 'מכירה לאומן', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2021-07-19 16:57:24', '2021-07-19 13:57:24', '', 14, 'http://yair:8888/?p=15', 0, 'revision', '', 0),
(16, 1, '2021-07-19 16:57:39', '2021-07-19 13:57:39', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים', 'כל הקטגוריות אצלנו', '', 'publish', 'closed', 'closed', '', '%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa', '', '', '2021-07-22 18:43:50', '2021-07-22 15:43:50', '', 0, 'http://yair:8888/?page_id=16', 0, 'page', '', 0),
(17, 1, '2021-07-19 16:57:39', '2021-07-19 13:57:39', '', 'קטגוריות', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2021-07-19 16:57:39', '2021-07-19 13:57:39', '', 16, 'http://yair:8888/?p=17', 0, 'revision', '', 0),
(18, 1, '2021-07-19 16:58:20', '2021-07-19 13:58:20', '', 'האומנים שלנו', '', 'publish', 'closed', 'closed', '', '%d7%94%d7%90%d7%95%d7%9e%d7%a0%d7%99%d7%9d-%d7%a9%d7%9c%d7%a0%d7%95', '', '', '2021-07-19 16:58:20', '2021-07-19 13:58:20', '', 0, 'http://yair:8888/?page_id=18', 0, 'page', '', 0),
(19, 1, '2021-07-19 16:58:20', '2021-07-19 13:58:20', '', 'האומנים שלנו', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2021-07-19 16:58:20', '2021-07-19 13:58:20', '', 18, 'http://yair:8888/?p=19', 0, 'revision', '', 0),
(20, 1, '2021-07-19 17:03:50', '2021-07-19 14:03:50', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2021-07-19 17:03:50', '2021-07-19 14:03:50', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2021-07-19 17:03:51', '2021-07-19 14:03:51', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים', 'מוצרים', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2021-07-23 11:44:26', '2021-07-23 08:44:26', '', 0, 'http://yair:8888/shop/', 0, 'page', '', 0),
(22, 1, '2021-07-19 17:03:51', '2021-07-19 14:03:51', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2021-07-19 17:03:51', '2021-07-19 14:03:51', '', 0, 'http://yair:8888/cart/', 0, 'page', '', 0),
(23, 1, '2021-07-19 17:03:51', '2021-07-19 14:03:51', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2021-07-19 17:03:51', '2021-07-19 14:03:51', '', 0, 'http://yair:8888/checkout/', 0, 'page', '', 0),
(24, 1, '2021-07-19 17:03:51', '2021-07-19 14:03:51', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2021-07-19 17:03:51', '2021-07-19 14:03:51', '', 0, 'http://yair:8888/my-account/', 0, 'page', '', 0),
(25, 1, '2021-07-19 17:08:28', '2021-07-19 14:08:28', '', 'מוצרים', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-07-19 17:08:28', '2021-07-19 14:08:28', '', 21, 'http://yair:8888/?p=25', 0, 'revision', '', 0),
(26, 1, '2021-07-19 17:15:10', '2021-07-19 14:15:10', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'פרטי קשר', '%d7%a4%d7%a8%d7%98%d7%99-%d7%a7%d7%a9%d7%a8', 'publish', 'closed', 'closed', '', 'group_5dd3a32b38e68', '', '', '2021-07-21 17:39:55', '2021-07-21 14:39:55', '', 0, 'http://yair:8888/?p=26', 0, 'acf-field-group', '', 0),
(27, 1, '2021-07-19 17:15:10', '2021-07-19 14:15:10', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'טלפון', 'tel', 'publish', 'closed', 'closed', '', 'field_5dd3a33f42e0c', '', '', '2021-07-19 17:15:10', '2021-07-19 14:15:10', '', 26, 'http://yair:8888/?post_type=acf-field&p=27', 0, 'acf-field', '', 0),
(28, 1, '2021-07-19 17:15:10', '2021-07-19 14:15:10', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'מייל', 'mail', 'publish', 'closed', 'closed', '', 'field_5dd3a35642e0d', '', '', '2021-07-19 17:15:10', '2021-07-19 14:15:10', '', 26, 'http://yair:8888/?post_type=acf-field&p=28', 1, 'acf-field', '', 0),
(29, 1, '2021-07-19 17:15:10', '2021-07-19 14:15:10', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'פקס', 'fax', 'publish', 'closed', 'closed', '', 'field_5dd3a36942e0e', '', '', '2021-07-19 17:15:10', '2021-07-19 14:15:10', '', 26, 'http://yair:8888/?post_type=acf-field&p=29', 2, 'acf-field', '', 0),
(31, 1, '2021-07-19 17:15:10', '2021-07-19 14:15:10', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'מפה (תמונה)', 'map_image', 'publish', 'closed', 'closed', '', 'field_5ddbd67734310', '', '', '2021-07-21 17:39:55', '2021-07-21 14:39:55', '', 26, 'http://yair:8888/?post_type=acf-field&#038;p=31', 3, 'acf-field', '', 0),
(32, 1, '2021-07-19 17:15:10', '2021-07-19 14:15:10', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'פייסבוק', 'facebook', 'publish', 'closed', 'closed', '', 'field_5ddbd6b3cc0cd', '', '', '2021-07-21 17:39:55', '2021-07-21 14:39:55', '', 26, 'http://yair:8888/?post_type=acf-field&#038;p=32', 5, 'acf-field', '', 0),
(33, 1, '2021-07-19 17:15:10', '2021-07-19 14:15:10', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Whatsapp', 'whatsapp', 'publish', 'closed', 'closed', '', 'field_5ddbd6cdcc0ce', '', '', '2021-07-21 17:39:55', '2021-07-21 14:39:55', '', 26, 'http://yair:8888/?post_type=acf-field&#038;p=33', 6, 'acf-field', '', 0),
(35, 1, '2021-07-19 17:23:55', '2021-07-19 14:23:55', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר SEO', 'single_slider_seo', 'publish', 'closed', 'closed', '', 'field_5ddbde5499115', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=35', 19, 'acf-field', '', 0),
(36, 1, '2021-07-19 17:23:55', '2021-07-19 14:23:55', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'content', 'publish', 'closed', 'closed', '', 'field_5ddbde7399116', '', '', '2021-07-19 17:23:55', '2021-07-19 14:23:55', '', 35, 'http://yair:8888/?post_type=acf-field&p=36', 0, 'acf-field', '', 0),
(37, 1, '2021-07-19 17:23:55', '2021-07-19 14:23:55', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'גלריה תמונות', 'single_gallery', 'publish', 'closed', 'closed', '', 'field_5ddbe5aea4275', '', '', '2021-07-19 17:25:29', '2021-07-19 14:25:29', '', 41, 'http://yair:8888/?post_type=acf-field&#038;p=37', 1, 'acf-field', '', 0),
(40, 1, '2021-07-19 17:25:01', '2021-07-19 14:25:01', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'slider_img', 'publish', 'closed', 'closed', '', 'field_60f58b1c85127', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=40', 20, 'acf-field', '', 0),
(41, 1, '2021-07-19 17:25:18', '2021-07-19 14:25:18', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"views/gallery.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'גלריה', '%d7%92%d7%9c%d7%a8%d7%99%d7%94', 'publish', 'closed', 'closed', '', 'group_60f58b422e2f1', '', '', '2021-07-19 17:25:18', '2021-07-19 14:25:18', '', 0, 'http://yair:8888/?post_type=acf-field-group&#038;p=41', 0, 'acf-field-group', '', 0),
(42, 1, '2021-07-20 15:13:51', '2021-07-20 12:13:51', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"views/home.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'דף הבית', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', 'publish', 'closed', 'closed', '', 'group_60f7e7a37b751', '', '', '2021-07-21 17:16:39', '2021-07-21 14:16:39', '', 0, 'http://yair:8888/?post_type=acf-field-group&#038;p=42', 0, 'acf-field-group', '', 0),
(43, 1, '2021-07-21 12:25:27', '2021-07-21 09:25:27', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'אודות', 'אודות', 'publish', 'closed', 'closed', '', 'field_60f7e7b4e8bf8', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=43', 0, 'acf-field', '', 0),
(44, 1, '2021-07-21 12:25:27', '2021-07-21 09:25:27', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"75\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'home_about_text', 'publish', 'closed', 'closed', '', 'field_60f7e7c1e8bf9', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=44', 1, 'acf-field', '', 0),
(45, 1, '2021-07-21 12:25:27', '2021-07-21 09:25:27', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'home_about_link', 'publish', 'closed', 'closed', '', 'field_60f7e7eae8bfa', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=45', 2, 'acf-field', '', 0),
(46, 1, '2021-07-21 16:48:17', '2021-07-21 13:48:17', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם וידאו ותמונה', 'בלוק_עם_וידאו_ותמונה', 'publish', 'closed', 'closed', '', 'field_60f824ed5b93d', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=46', 3, 'acf-field', '', 0),
(47, 1, '2021-07-21 16:48:17', '2021-07-21 13:48:17', 'a:10:{s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:7:\"library\";s:3:\"all\";s:8:\"min_size\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'סרטון רקע', 'home_video_back', 'publish', 'closed', 'closed', '', 'field_60f825195b93e', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=47', 4, 'acf-field', '', 0),
(48, 1, '2021-07-21 16:48:17', '2021-07-21 13:48:17', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'home_video_block_img', 'publish', 'closed', 'closed', '', 'field_60f825355b93f', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=48', 5, 'acf-field', '', 0),
(49, 1, '2021-07-21 16:48:17', '2021-07-21 13:48:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_video_block_title', 'publish', 'closed', 'closed', '', 'field_60f8254d5b940', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=49', 6, 'acf-field', '', 0),
(50, 1, '2021-07-21 16:48:18', '2021-07-21 13:48:18', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'home_video_block_text', 'publish', 'closed', 'closed', '', 'field_60f825645b941', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=50', 7, 'acf-field', '', 0),
(51, 1, '2021-07-21 16:48:18', '2021-07-21 13:48:18', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'קישורים', 'home_video_block_links', 'publish', 'closed', 'closed', '', 'field_60f8257f5b942', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=51', 8, 'acf-field', '', 0),
(52, 1, '2021-07-21 16:48:18', '2021-07-21 13:48:18', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'video_block_link', 'publish', 'closed', 'closed', '', 'field_60f825965b943', '', '', '2021-07-21 16:48:18', '2021-07-21 13:48:18', '', 51, 'http://yair:8888/?post_type=acf-field&p=52', 0, 'acf-field', '', 0),
(53, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם קטגוריות', 'בלוק_עם_קטגוריות', 'publish', 'closed', 'closed', '', 'field_60f8271d55680', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=53', 9, 'acf-field', '', 0),
(54, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_cats_title', 'publish', 'closed', 'closed', '', 'field_60f8273b55681', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=54', 10, 'acf-field', '', 0),
(55, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'home_cats_text', 'publish', 'closed', 'closed', '', 'field_60f8274a55682', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=55', 11, 'acf-field', '', 0),
(56, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:13:{s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:8:\"taxonomy\";s:11:\"product_cat\";s:10:\"field_type\";s:8:\"checkbox\";s:8:\"add_term\";i:1;s:10:\"save_terms\";i:0;s:10:\"load_terms\";i:0;s:13:\"return_format\";s:6:\"object\";s:8:\"multiple\";i:0;s:10:\"allow_null\";i:0;}', 'קטגוריות', 'home_cats_chosen', 'publish', 'closed', 'closed', '', 'field_60f8275b55683', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=56', 12, 'acf-field', '', 0),
(57, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל הקטגוריות', 'home_cats_link', 'publish', 'closed', 'closed', '', 'field_60f8277455684', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=57', 13, 'acf-field', '', 0),
(58, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם הצעה', 'בלוק_עם_', 'publish', 'closed', 'closed', '', 'field_60f8278855685', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=58', 14, 'acf-field', '', 0),
(59, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_offer_title', 'publish', 'closed', 'closed', '', 'field_60f827bd55686', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=59', 15, 'acf-field', '', 0),
(60, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'home_offer_text', 'publish', 'closed', 'closed', '', 'field_60f827c755687', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=60', 16, 'acf-field', '', 0),
(61, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'home_offer_link', 'publish', 'closed', 'closed', '', 'field_60f827d855688', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=61', 17, 'acf-field', '', 0),
(62, 1, '2021-07-21 16:58:19', '2021-07-21 13:58:19', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'סליידר עם תוכן', 'סליידר_עם_תוכן', 'publish', 'closed', 'closed', '', 'field_60f827e555689', '', '', '2021-07-21 16:59:13', '2021-07-21 13:59:13', '', 42, 'http://yair:8888/?post_type=acf-field&#038;p=62', 18, 'acf-field', '', 0),
(63, 1, '2021-07-21 17:16:39', '2021-07-21 14:16:39', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'מוצרים', 'מוצרים', 'publish', 'closed', 'closed', '', 'field_60f82b8c4d2ae', '', '', '2021-07-21 17:16:39', '2021-07-21 14:16:39', '', 42, 'http://yair:8888/?post_type=acf-field&p=63', 21, 'acf-field', '', 0),
(64, 1, '2021-07-21 17:16:39', '2021-07-21 14:16:39', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_prods_title', 'publish', 'closed', 'closed', '', 'field_60f82b984d2af', '', '', '2021-07-21 17:16:39', '2021-07-21 14:16:39', '', 42, 'http://yair:8888/?post_type=acf-field&p=64', 22, 'acf-field', '', 0),
(65, 1, '2021-07-21 17:16:39', '2021-07-21 14:16:39', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מוצרים', 'home_prods', 'publish', 'closed', 'closed', '', 'field_60f82ba14d2b0', '', '', '2021-07-21 17:16:39', '2021-07-21 14:16:39', '', 42, 'http://yair:8888/?post_type=acf-field&p=65', 23, 'acf-field', '', 0),
(66, 1, '2021-07-21 17:16:39', '2021-07-21 14:16:39', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'גלריה', 'גלריה', 'publish', 'closed', 'closed', '', 'field_60f82bb34d2b1', '', '', '2021-07-21 17:16:39', '2021-07-21 14:16:39', '', 42, 'http://yair:8888/?post_type=acf-field&p=66', 24, 'acf-field', '', 0),
(67, 1, '2021-07-21 17:16:39', '2021-07-21 14:16:39', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_gallery_title', 'publish', 'closed', 'closed', '', 'field_60f82bc04d2b2', '', '', '2021-07-21 17:16:39', '2021-07-21 14:16:39', '', 42, 'http://yair:8888/?post_type=acf-field&p=67', 25, 'acf-field', '', 0),
(68, 1, '2021-07-21 17:16:39', '2021-07-21 14:16:39', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";i:4;s:3:\"max\";i:4;s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'גלריה', 'home_gallery', 'publish', 'closed', 'closed', '', 'field_60f82bce4d2b3', '', '', '2021-07-21 17:16:39', '2021-07-21 14:16:39', '', 42, 'http://yair:8888/?post_type=acf-field&p=68', 26, 'acf-field', '', 0),
(69, 1, '2021-07-21 17:16:39', '2021-07-21 14:16:39', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל התמונות', 'home_gallery_link', 'publish', 'closed', 'closed', '', 'field_60f82be84d2b4', '', '', '2021-07-21 17:16:39', '2021-07-21 14:16:39', '', 42, 'http://yair:8888/?post_type=acf-field&p=69', 27, 'acf-field', '', 0),
(70, 1, '2021-07-21 17:17:21', '2021-07-21 14:17:21', 'a:7:{s:8:\"location\";a:2:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"views/home.php\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"views/offer.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'שאלות ותשובות', '%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_5feb2f4e471ee', '', '', '2021-07-21 17:20:10', '2021-07-21 14:20:10', '', 0, 'http://yair:8888/?p=70', 10, 'acf-field-group', '', 0),
(71, 1, '2021-07-21 17:17:21', '2021-07-21 14:17:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'faq_title', 'publish', 'closed', 'closed', '', 'field_5feb2f82db93b', '', '', '2021-07-21 17:17:21', '2021-07-21 14:17:21', '', 70, 'http://yair:8888/?post_type=acf-field&p=71', 0, 'acf-field', '', 0),
(72, 1, '2021-07-21 17:17:21', '2021-07-21 14:17:21', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'faq_text', 'publish', 'closed', 'closed', '', 'field_5feb2f95db93c', '', '', '2021-07-21 17:17:21', '2021-07-21 14:17:21', '', 70, 'http://yair:8888/?post_type=acf-field&p=72', 1, 'acf-field', '', 0),
(73, 1, '2021-07-21 17:17:21', '2021-07-21 14:17:21', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'שאלות ותשובות', 'faq_item', 'publish', 'closed', 'closed', '', 'field_5feb2faddb93d', '', '', '2021-07-21 17:17:21', '2021-07-21 14:17:21', '', 70, 'http://yair:8888/?post_type=acf-field&p=73', 2, 'acf-field', '', 0),
(74, 1, '2021-07-21 17:17:21', '2021-07-21 14:17:21', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'שאלה', 'faq_question', 'publish', 'closed', 'closed', '', 'field_5feb2fc0db93e', '', '', '2021-07-21 17:17:21', '2021-07-21 14:17:21', '', 73, 'http://yair:8888/?post_type=acf-field&p=74', 0, 'acf-field', '', 0),
(75, 1, '2021-07-21 17:17:21', '2021-07-21 14:17:21', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תשובה', 'faq_answer', 'publish', 'closed', 'closed', '', 'field_5feb2fd3db93f', '', '', '2021-07-21 17:17:21', '2021-07-21 14:17:21', '', 73, 'http://yair:8888/?post_type=acf-field&p=75', 1, 'acf-field', '', 0),
(76, 1, '2021-07-21 17:18:15', '2021-07-21 14:18:15', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל השאלות ותשובות', 'faq_link', 'publish', 'closed', 'closed', '', 'field_60f82c8971ca0', '', '', '2021-07-21 17:20:10', '2021-07-21 14:20:10', '', 70, 'http://yair:8888/?post_type=acf-field&#038;p=76', 3, 'acf-field', '', 0),
(77, 1, '2021-07-21 17:18:47', '2021-07-21 14:18:47', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'שאלות ותשובות', 'faq_item_page', 'publish', 'closed', 'closed', '', 'field_60f82cb738ed5', '', '', '2021-07-21 17:20:33', '2021-07-21 14:20:33', '', 81, 'http://yair:8888/?post_type=acf-field&#038;p=77', 1, 'acf-field', '', 0),
(78, 1, '2021-07-21 17:18:47', '2021-07-21 14:18:47', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'שאלה', 'faq_question', 'publish', 'closed', 'closed', '', 'field_60f82cb738ed6', '', '', '2021-07-21 17:18:47', '2021-07-21 14:18:47', '', 77, 'http://yair:8888/?post_type=acf-field&p=78', 0, 'acf-field', '', 0),
(79, 1, '2021-07-21 17:18:47', '2021-07-21 14:18:47', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תשובה', 'faq_answer', 'publish', 'closed', 'closed', '', 'field_60f82cb738ed7', '', '', '2021-07-21 17:18:47', '2021-07-21 14:18:47', '', 77, 'http://yair:8888/?post_type=acf-field&p=79', 1, 'acf-field', '', 0),
(80, 1, '2021-07-21 17:19:57', '2021-07-21 14:19:57', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:13:\"views/faq.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'עמוד עם שאלות ותשובות', '%d7%a2%d7%9e%d7%95%d7%93-%d7%a2%d7%9d-%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_60f82ccfcb1f4', '', '', '2021-07-24 12:29:48', '2021-07-24 09:29:48', '', 0, 'http://yair:8888/?post_type=acf-field-group&#038;p=80', 0, 'acf-field-group', '', 0),
(81, 1, '2021-07-21 17:19:57', '2021-07-21 14:19:57', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'בלוק עם שאלות ותשובות', 'page_faq_block', 'publish', 'closed', 'closed', '', 'field_60f82ce3088ce', '', '', '2021-07-24 12:29:48', '2021-07-24 09:29:48', '', 80, 'http://yair:8888/?post_type=acf-field&#038;p=81', 0, 'acf-field', '', 0),
(82, 1, '2021-07-21 17:19:57', '2021-07-21 14:19:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'faq_page_block_title', 'publish', 'closed', 'closed', '', 'field_60f82cfe088cf', '', '', '2021-07-21 17:19:57', '2021-07-21 14:19:57', '', 81, 'http://yair:8888/?post_type=acf-field&p=82', 0, 'acf-field', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(83, 1, '2021-07-21 17:23:10', '2021-07-21 14:23:10', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"views/offer.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'מכירה לאומן', '%d7%9e%d7%9b%d7%99%d7%a8%d7%94-%d7%9c%d7%90%d7%95%d7%9e%d7%9f', 'publish', 'closed', 'closed', '', 'group_60f82da5e15a4', '', '', '2021-07-21 17:23:10', '2021-07-21 14:23:10', '', 0, 'http://yair:8888/?post_type=acf-field-group&#038;p=83', 0, 'acf-field-group', '', 0),
(84, 1, '2021-07-21 17:23:10', '2021-07-21 14:23:10', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת של טופס', 'team_form_title', 'publish', 'closed', 'closed', '', 'field_60f82dbbf03fb', '', '', '2021-07-21 17:23:10', '2021-07-21 14:23:10', '', 83, 'http://yair:8888/?post_type=acf-field&p=84', 0, 'acf-field', '', 0),
(85, 1, '2021-07-21 17:28:16', '2021-07-21 14:28:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"views/artists.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'אומנים', '%d7%90%d7%95%d7%9e%d7%a0%d7%99%d7%9d', 'publish', 'closed', 'closed', '', 'group_60f82ec7bf032', '', '', '2021-07-21 17:28:16', '2021-07-21 14:28:16', '', 0, 'http://yair:8888/?post_type=acf-field-group&#038;p=85', 0, 'acf-field-group', '', 0),
(86, 1, '2021-07-21 17:28:16', '2021-07-21 14:28:16', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'הצטרפו לאומנים שלנו', 'artists_link_page', 'publish', 'closed', 'closed', '', 'field_60f82ee1e2245', '', '', '2021-07-21 17:28:16', '2021-07-21 14:28:16', '', 85, 'http://yair:8888/?post_type=acf-field&p=86', 0, 'acf-field', '', 0),
(87, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תוספות', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_60f82f1c8871f', '', '', '2021-07-23 10:51:40', '2021-07-23 07:51:40', '', 0, 'http://yair:8888/?post_type=acf-field-group&#038;p=87', 0, 'acf-field-group', '', 0),
(88, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'טופס בפוטר', 'טופס_בפוטר', 'publish', 'closed', 'closed', '', 'field_60f82f282e43f', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=88', 0, 'acf-field', '', 0),
(89, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'foo_title', 'publish', 'closed', 'closed', '', 'field_60f82f372e440', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=89', 1, 'acf-field', '', 0),
(90, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'כותרת משנה', 'foo_subtitle', 'publish', 'closed', 'closed', '', 'field_60f82f3f2e441', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=90', 2, 'acf-field', '', 0),
(91, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'תהליך', 'תהליך', 'publish', 'closed', 'closed', '', 'field_60f82f502e442', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=91', 3, 'acf-field', '', 0),
(92, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'process_block_title', 'publish', 'closed', 'closed', '', 'field_60f82f6a2e443', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=92', 4, 'acf-field', '', 0),
(93, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'שלב', 'process_item', 'publish', 'closed', 'closed', '', 'field_60f82f782e444', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=93', 5, 'acf-field', '', 0),
(94, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'אייקון', 'process_icon', 'publish', 'closed', 'closed', '', 'field_60f82f8f2e445', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 93, 'http://yair:8888/?post_type=acf-field&p=94', 0, 'acf-field', '', 0),
(95, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'process_title', 'publish', 'closed', 'closed', '', 'field_60f82fa32e446', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 93, 'http://yair:8888/?post_type=acf-field&p=95', 1, 'acf-field', '', 0),
(96, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'המלצות', 'המלצות', 'publish', 'closed', 'closed', '', 'field_60f82fad2e447', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=96', 6, 'acf-field', '', 0),
(97, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'reviews_block_title', 'publish', 'closed', 'closed', '', 'field_60f830132e448', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=97', 7, 'acf-field', '', 0),
(98, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'המלצה', 'review_item', 'publish', 'closed', 'closed', '', 'field_60f830272e449', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 87, 'http://yair:8888/?post_type=acf-field&p=98', 8, 'acf-field', '', 0),
(99, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'שם', 'review_name', 'publish', 'closed', 'closed', '', 'field_60f830502e44a', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 98, 'http://yair:8888/?post_type=acf-field&p=99', 0, 'acf-field', '', 0),
(100, 1, '2021-07-21 17:34:35', '2021-07-21 14:34:35', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'review_text', 'publish', 'closed', 'closed', '', 'field_60f830692e44b', '', '', '2021-07-21 17:34:35', '2021-07-21 14:34:35', '', 98, 'http://yair:8888/?post_type=acf-field&p=100', 1, 'acf-field', '', 0),
(101, 1, '2021-07-21 17:39:55', '2021-07-21 14:39:55', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Instagram', 'instagram', 'publish', 'closed', 'closed', '', 'field_60f831a9129f3', '', '', '2021-07-21 17:39:55', '2021-07-21 14:39:55', '', 26, 'http://yair:8888/?post_type=acf-field&p=101', 4, 'acf-field', '', 0),
(102, 1, '2021-07-21 21:09:16', '2021-07-21 18:09:16', '', 'category-1', '', 'inherit', 'open', 'closed', '', 'category-1', '', '', '2021-07-21 21:09:16', '2021-07-21 18:09:16', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-1.png', 0, 'attachment', 'image/png', 0),
(103, 1, '2021-07-21 21:09:18', '2021-07-21 18:09:18', '', 'category-2', '', 'inherit', 'open', 'closed', '', 'category-2', '', '', '2021-07-21 21:09:18', '2021-07-21 18:09:18', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-2.png', 0, 'attachment', 'image/png', 0),
(104, 1, '2021-07-21 21:09:19', '2021-07-21 18:09:19', '', 'category-3', '', 'inherit', 'open', 'closed', '', 'category-3', '', '', '2021-07-21 21:09:19', '2021-07-21 18:09:19', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-3.png', 0, 'attachment', 'image/png', 0),
(105, 1, '2021-07-21 21:09:20', '2021-07-21 18:09:20', '', 'category-4', '', 'inherit', 'open', 'closed', '', 'category-4', '', '', '2021-07-21 21:09:20', '2021-07-21 18:09:20', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-4.png', 0, 'attachment', 'image/png', 0),
(106, 1, '2021-07-21 21:09:21', '2021-07-21 18:09:21', '', 'category-5', '', 'inherit', 'open', 'closed', '', 'category-5', '', '', '2021-07-25 17:20:20', '2021-07-25 14:20:20', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-5.png', 0, 'attachment', 'image/png', 0),
(107, 1, '2021-07-21 21:09:22', '2021-07-21 18:09:22', '', 'category-6', '', 'inherit', 'open', 'closed', '', 'category-6', '', '', '2021-07-21 21:09:22', '2021-07-21 18:09:22', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-6.png', 0, 'attachment', 'image/png', 0),
(108, 1, '2021-07-21 21:09:24', '2021-07-21 18:09:24', '', 'category-7', '', 'inherit', 'open', 'closed', '', 'category-7', '', '', '2021-07-21 21:09:24', '2021-07-21 18:09:24', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-7.png', 0, 'attachment', 'image/png', 0),
(109, 1, '2021-07-21 21:09:25', '2021-07-21 18:09:25', '', 'category-8', '', 'inherit', 'open', 'closed', '', 'category-8', '', '', '2021-07-21 21:09:25', '2021-07-21 18:09:25', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-8.png', 0, 'attachment', 'image/png', 0),
(110, 1, '2021-07-21 21:09:26', '2021-07-21 18:09:26', '', 'category-9', '', 'inherit', 'open', 'closed', '', 'category-9', '', '', '2021-07-21 21:09:26', '2021-07-21 18:09:26', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-9.png', 0, 'attachment', 'image/png', 0),
(111, 1, '2021-07-21 21:09:28', '2021-07-21 18:09:28', '', 'category-10', '', 'inherit', 'open', 'closed', '', 'category-10', '', '', '2021-07-21 21:09:28', '2021-07-21 18:09:28', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-10.png', 0, 'attachment', 'image/png', 0),
(112, 1, '2021-07-21 21:09:29', '2021-07-21 18:09:29', '', 'category-11', '', 'inherit', 'open', 'closed', '', 'category-11', '', '', '2021-07-21 21:09:29', '2021-07-21 18:09:29', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-11.png', 0, 'attachment', 'image/png', 0),
(113, 1, '2021-07-21 21:11:42', '2021-07-21 18:11:42', '', 'category-12', '', 'inherit', 'open', 'closed', '', 'category-12', '', '', '2021-07-21 21:11:42', '2021-07-21 18:11:42', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/category-12.png', 0, 'attachment', 'image/png', 0),
(114, 1, '2021-07-21 21:14:52', '2021-07-21 18:14:52', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"artist\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'אומן', '%d7%90%d7%95%d7%9e%d7%9f', 'publish', 'closed', 'closed', '', 'group_60f863fbbee7c', '', '', '2021-07-21 21:14:52', '2021-07-21 18:14:52', '', 0, 'http://yair:8888/?post_type=acf-field-group&#038;p=114', 0, 'acf-field-group', '', 0),
(115, 1, '2021-07-21 21:14:52', '2021-07-21 18:14:52', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'cat_img', 'publish', 'closed', 'closed', '', 'field_60f864066a163', '', '', '2021-07-21 21:14:52', '2021-07-21 18:14:52', '', 114, 'http://yair:8888/?post_type=acf-field&p=115', 0, 'acf-field', '', 0),
(116, 1, '2021-07-21 21:16:04', '2021-07-21 18:16:04', '', 'artist-1', '', 'inherit', 'open', 'closed', '', 'artist-1', '', '', '2021-07-21 21:16:04', '2021-07-21 18:16:04', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-1.png', 0, 'attachment', 'image/png', 0),
(117, 1, '2021-07-21 21:16:05', '2021-07-21 18:16:05', '', 'artist-2', '', 'inherit', 'open', 'closed', '', 'artist-2', '', '', '2021-07-21 21:16:05', '2021-07-21 18:16:05', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-2.png', 0, 'attachment', 'image/png', 0),
(118, 1, '2021-07-21 21:16:07', '2021-07-21 18:16:07', '', 'artist-3', '', 'inherit', 'open', 'closed', '', 'artist-3', '', '', '2021-07-21 21:16:07', '2021-07-21 18:16:07', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-3.png', 0, 'attachment', 'image/png', 0),
(119, 1, '2021-07-21 21:16:08', '2021-07-21 18:16:08', '', 'artist-4', '', 'inherit', 'open', 'closed', '', 'artist-4', '', '', '2021-07-21 21:16:08', '2021-07-21 18:16:08', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-4.png', 0, 'attachment', 'image/png', 0),
(120, 1, '2021-07-21 21:16:09', '2021-07-21 18:16:09', '', 'artist-5', '', 'inherit', 'open', 'closed', '', 'artist-5', '', '', '2021-07-21 21:16:09', '2021-07-21 18:16:09', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-5.png', 0, 'attachment', 'image/png', 0),
(121, 1, '2021-07-21 21:16:10', '2021-07-21 18:16:10', '', 'artist-6', '', 'inherit', 'open', 'closed', '', 'artist-6', '', '', '2021-07-21 21:16:10', '2021-07-21 18:16:10', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-6.png', 0, 'attachment', 'image/png', 0),
(122, 1, '2021-07-21 21:16:11', '2021-07-21 18:16:11', '', 'artist-7', '', 'inherit', 'open', 'closed', '', 'artist-7', '', '', '2021-07-21 21:16:11', '2021-07-21 18:16:11', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-7.png', 0, 'attachment', 'image/png', 0),
(123, 1, '2021-07-21 21:16:13', '2021-07-21 18:16:13', '', 'artist-8', '', 'inherit', 'open', 'closed', '', 'artist-8', '', '', '2021-07-21 21:16:13', '2021-07-21 18:16:13', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-8.png', 0, 'attachment', 'image/png', 0),
(124, 1, '2021-07-21 21:16:14', '2021-07-21 18:16:14', '', 'artist-9', '', 'inherit', 'open', 'closed', '', 'artist-9', '', '', '2021-07-21 21:16:14', '2021-07-21 18:16:14', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/artist-9.png', 0, 'attachment', 'image/png', 0),
(125, 1, '2021-07-21 21:21:10', '2021-07-21 18:21:10', '', 'שם המוצר לורם איפסום 1 TEST', 'להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', '', '', '2021-07-23 21:19:35', '2021-07-23 18:19:35', '', 0, 'http://yair:8888/?post_type=product&#038;p=125', 0, 'product', '', 0),
(126, 1, '2021-07-21 21:22:02', '2021-07-21 18:22:02', '', 'product-1', '', 'inherit', 'open', 'closed', '', 'product-1', '', '', '2021-07-21 21:22:02', '2021-07-21 18:22:02', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-1.png', 0, 'attachment', 'image/png', 0),
(127, 1, '2021-07-21 21:22:04', '2021-07-21 18:22:04', '', 'product-3', '', 'inherit', 'open', 'closed', '', 'product-3', '', '', '2021-07-21 21:22:04', '2021-07-21 18:22:04', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-3.png', 0, 'attachment', 'image/png', 0),
(128, 1, '2021-07-21 21:22:06', '2021-07-21 18:22:06', '', 'product-4', '', 'inherit', 'open', 'closed', '', 'product-4', '', '', '2021-07-21 21:22:06', '2021-07-21 18:22:06', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-4.png', 0, 'attachment', 'image/png', 0),
(129, 1, '2021-07-21 21:22:08', '2021-07-21 18:22:08', '', 'product-5', '', 'inherit', 'open', 'closed', '', 'product-5', '', '', '2021-07-21 21:22:08', '2021-07-21 18:22:08', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-5.png', 0, 'attachment', 'image/png', 0),
(130, 1, '2021-07-21 21:22:10', '2021-07-21 18:22:10', '', 'product-6', '', 'inherit', 'open', 'closed', '', 'product-6', '', '', '2021-07-21 21:22:10', '2021-07-21 18:22:10', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-6.png', 0, 'attachment', 'image/png', 0),
(131, 1, '2021-07-21 21:22:12', '2021-07-21 18:22:12', '', 'product-7', '', 'inherit', 'open', 'closed', '', 'product-7', '', '', '2021-07-21 21:22:12', '2021-07-21 18:22:12', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-7.png', 0, 'attachment', 'image/png', 0),
(132, 1, '2021-07-21 21:22:14', '2021-07-21 18:22:14', '', 'product-8', '', 'inherit', 'open', 'closed', '', 'product-8', '', '', '2021-07-21 21:22:14', '2021-07-21 18:22:14', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-8.png', 0, 'attachment', 'image/png', 0),
(133, 1, '2021-07-21 21:22:16', '2021-07-21 18:22:16', '', 'product-9', '', 'inherit', 'open', 'closed', '', 'product-9', '', '', '2021-07-21 21:22:16', '2021-07-21 18:22:16', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-9.png', 0, 'attachment', 'image/png', 0),
(134, 1, '2021-07-21 21:22:18', '2021-07-21 18:22:18', '', 'product-10', '', 'inherit', 'open', 'closed', '', 'product-10', '', '', '2021-07-21 21:22:18', '2021-07-21 18:22:18', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-10.png', 0, 'attachment', 'image/png', 0),
(135, 1, '2021-07-21 21:22:20', '2021-07-21 18:22:20', '', 'product-11', '', 'inherit', 'open', 'closed', '', 'product-11', '', '', '2021-07-21 21:22:20', '2021-07-21 18:22:20', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-11.png', 0, 'attachment', 'image/png', 0),
(136, 1, '2021-07-21 21:22:22', '2021-07-21 18:22:22', '', 'product-12', '', 'inherit', 'open', 'closed', '', 'product-12', '', '', '2021-07-21 21:22:22', '2021-07-21 18:22:22', '', 125, 'http://yair:8888/wp-content/uploads/2021/07/product-12.png', 0, 'attachment', 'image/png', 0),
(137, 1, '2021-07-21 21:23:41', '2021-07-21 18:23:41', '', 'שם המוצר לורם איפסום 2', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', '', '', '2021-07-21 21:23:41', '2021-07-21 18:23:41', '', 0, 'http://yair:8888/?post_type=product&#038;p=137', 0, 'product', '', 0),
(138, 1, '2021-07-21 21:24:29', '2021-07-21 18:24:29', '', 'שם המוצר לורם איפסום 3', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', '', '', '2021-07-21 21:36:31', '2021-07-21 18:36:31', '', 0, 'http://yair:8888/?post_type=product&#038;p=138', 0, 'product', '', 0),
(139, 1, '2021-07-21 21:48:46', '2021-07-21 18:48:46', '', 'שם המוצר לורם איפסום 4', 'להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', '', '', '2021-07-21 21:48:46', '2021-07-21 18:48:46', '', 0, 'http://yair:8888/?post_type=product&#038;p=139', 0, 'product', '', 0),
(140, 1, '2021-07-21 21:49:27', '2021-07-21 18:49:27', '', 'שם המוצר לורם איפסום 5', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', '', '', '2021-07-21 21:49:27', '2021-07-21 18:49:27', '', 0, 'http://yair:8888/?post_type=product&#038;p=140', 0, 'product', '', 0),
(141, 1, '2021-07-21 21:50:07', '2021-07-21 18:50:07', '', 'שם המוצר לורם איפסום 6', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6', '', '', '2021-07-21 21:50:07', '2021-07-21 18:50:07', '', 0, 'http://yair:8888/?post_type=product&#038;p=141', 0, 'product', '', 0),
(142, 1, '2021-07-21 22:06:14', '2021-07-21 19:06:14', '', 'שם המוצר לורם איפסום 7', 'להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-7', '', '', '2021-07-21 22:06:14', '2021-07-21 19:06:14', '', 0, 'http://yair:8888/?post_type=product&#038;p=142', 0, 'product', '', 0),
(143, 1, '2021-07-21 22:06:51', '2021-07-21 19:06:51', '', 'שם המוצר לורם איפסום 8', 'קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-8', '', '', '2021-07-21 22:06:51', '2021-07-21 19:06:51', '', 0, 'http://yair:8888/?post_type=product&#038;p=143', 0, 'product', '', 0),
(144, 1, '2021-07-21 22:07:20', '2021-07-21 19:07:20', '', 'שם המוצר לורם איפסום 9', 'להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-9', '', '', '2021-07-21 22:07:20', '2021-07-21 19:07:20', '', 0, 'http://yair:8888/?post_type=product&#038;p=144', 0, 'product', '', 0),
(145, 1, '2021-07-21 22:08:28', '2021-07-21 19:08:28', '', 'שם המוצר לורם איפסום 10', 'גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-10', '', '', '2021-07-21 22:08:28', '2021-07-21 19:08:28', '', 0, 'http://yair:8888/?post_type=product&#038;p=145', 0, 'product', '', 0),
(146, 1, '2021-07-21 22:08:02', '2021-07-21 19:08:02', '', 'product-13', '', 'inherit', 'open', 'closed', '', 'product-13', '', '', '2021-07-21 22:08:02', '2021-07-21 19:08:02', '', 145, 'http://yair:8888/wp-content/uploads/2021/07/product-13.png', 0, 'attachment', 'image/png', 0),
(147, 1, '2021-07-21 22:08:03', '2021-07-21 19:08:03', '', 'product-14', '', 'inherit', 'open', 'closed', '', 'product-14', '', '', '2021-07-21 22:08:03', '2021-07-21 19:08:03', '', 145, 'http://yair:8888/wp-content/uploads/2021/07/product-14.png', 0, 'attachment', 'image/png', 0),
(148, 1, '2021-07-21 22:08:04', '2021-07-21 19:08:04', '', 'product-15', '', 'inherit', 'open', 'closed', '', 'product-15', '', '', '2021-07-21 22:08:04', '2021-07-21 19:08:04', '', 145, 'http://yair:8888/wp-content/uploads/2021/07/product-15.png', 0, 'attachment', 'image/png', 0),
(149, 1, '2021-07-21 22:08:05', '2021-07-21 19:08:05', '', 'product-16', '', 'inherit', 'open', 'closed', '', 'product-16', '', '', '2021-07-21 22:08:05', '2021-07-21 19:08:05', '', 145, 'http://yair:8888/wp-content/uploads/2021/07/product-16.png', 0, 'attachment', 'image/png', 0),
(150, 1, '2021-07-21 22:09:05', '2021-07-21 19:09:05', '', 'שם המוצר לורם איפסום 11', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-11', '', '', '2021-07-21 22:09:06', '2021-07-21 19:09:06', '', 0, 'http://yair:8888/?post_type=product&#038;p=150', 0, 'product', '', 0),
(151, 1, '2021-07-21 22:09:34', '2021-07-21 19:09:34', '', 'שם המוצר לורם איפסום 12', 'גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-12', '', '', '2021-07-21 22:09:34', '2021-07-21 19:09:34', '', 0, 'http://yair:8888/?post_type=product&#038;p=151', 0, 'product', '', 0),
(152, 1, '2021-07-21 22:10:11', '2021-07-21 19:10:11', '', 'שם המוצר לורם איפסום 13', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-13', '', '', '2021-07-21 22:10:11', '2021-07-21 19:10:11', '', 0, 'http://yair:8888/?post_type=product&#038;p=152', 0, 'product', '', 0),
(153, 1, '2021-07-21 22:11:14', '2021-07-21 19:11:14', '', 'שם המוצר לורם איפסום 14', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-14', '', '', '2021-07-21 22:11:15', '2021-07-21 19:11:15', '', 0, 'http://yair:8888/?post_type=product&#038;p=153', 0, 'product', '', 0),
(154, 1, '2021-07-21 22:12:00', '2021-07-21 19:12:00', '', 'שם המוצר לורם איפסום 15', 'גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-15', '', '', '2021-07-21 22:12:00', '2021-07-21 19:12:00', '', 0, 'http://yair:8888/?post_type=product&#038;p=154', 0, 'product', '', 0),
(155, 1, '2021-07-21 22:45:44', '2021-07-21 19:45:44', '', 'main-img', '', 'inherit', 'open', 'closed', '', 'main-img', '', '', '2021-07-21 22:45:44', '2021-07-21 19:45:44', '', 8, 'http://yair:8888/wp-content/uploads/2021/07/main-img.png', 0, 'attachment', 'image/png', 0),
(156, 1, '2021-07-21 22:48:31', '2021-07-21 19:48:31', '', 'home-gallery-1', '', 'inherit', 'open', 'closed', '', 'home-gallery-1', '', '', '2021-07-21 22:48:31', '2021-07-21 19:48:31', '', 8, 'http://yair:8888/wp-content/uploads/2021/07/home-gallery-1.png', 0, 'attachment', 'image/png', 0),
(157, 1, '2021-07-21 22:48:33', '2021-07-21 19:48:33', '', 'home-gallery-2', '', 'inherit', 'open', 'closed', '', 'home-gallery-2', '', '', '2021-07-21 22:48:33', '2021-07-21 19:48:33', '', 8, 'http://yair:8888/wp-content/uploads/2021/07/home-gallery-2.png', 0, 'attachment', 'image/png', 0),
(158, 1, '2021-07-21 22:48:34', '2021-07-21 19:48:34', '', 'home-gallery-3', '', 'inherit', 'open', 'closed', '', 'home-gallery-3', '', '', '2021-07-24 21:45:36', '2021-07-24 18:45:36', '', 8, 'http://yair:8888/wp-content/uploads/2021/07/home-gallery-3.png', 0, 'attachment', 'image/png', 0),
(159, 1, '2021-07-21 22:48:35', '2021-07-21 19:48:35', '', 'home-gallery-4', '', 'inherit', 'open', 'closed', '', 'home-gallery-4', '', '', '2021-07-21 22:48:35', '2021-07-21 19:48:35', '', 8, 'http://yair:8888/wp-content/uploads/2021/07/home-gallery-4.png', 0, 'attachment', 'image/png', 0),
(160, 1, '2021-07-21 22:49:02', '2021-07-21 19:49:02', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2021-07-21 22:49:02', '2021-07-21 19:49:02', '', 8, 'http://yair:8888/?p=160', 0, 'revision', '', 0),
(161, 1, '2021-07-21 23:20:25', '2021-07-21 20:20:25', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2021-07-21 23:20:25', '2021-07-21 20:20:25', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/logo.png', 0, 'attachment', 'image/png', 0),
(162, 1, '2021-07-21 23:21:04', '2021-07-21 20:21:04', '', 'process-1', '', 'inherit', 'open', 'closed', '', 'process-1', '', '', '2021-07-21 23:21:04', '2021-07-21 20:21:04', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/process-1.png', 0, 'attachment', 'image/png', 0),
(163, 1, '2021-07-21 23:21:05', '2021-07-21 20:21:05', '', 'process-2', '', 'inherit', 'open', 'closed', '', 'process-2', '', '', '2021-07-21 23:21:05', '2021-07-21 20:21:05', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/process-2.png', 0, 'attachment', 'image/png', 0),
(164, 1, '2021-07-21 23:21:06', '2021-07-21 20:21:06', '', 'process-3', '', 'inherit', 'open', 'closed', '', 'process-3', '', '', '2021-07-21 23:21:06', '2021-07-21 20:21:06', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/process-3.png', 0, 'attachment', 'image/png', 0),
(165, 1, '2021-07-21 23:21:07', '2021-07-21 20:21:07', '', 'process-4', '', 'inherit', 'open', 'closed', '', 'process-4', '', '', '2021-07-21 23:21:07', '2021-07-21 20:21:07', '', 0, 'http://yair:8888/wp-content/uploads/2021/07/process-4.png', 0, 'attachment', 'image/png', 0),
(166, 1, '2021-07-22 08:58:02', '2021-07-22 05:58:02', '', 'home-video-block-img', '', 'inherit', 'open', 'closed', '', 'home-video-block-img', '', '', '2021-07-22 08:58:02', '2021-07-22 05:58:02', '', 8, 'http://yair:8888/wp-content/uploads/2021/07/home-video-block-img.png', 0, 'attachment', 'image/png', 0),
(167, 1, '2021-07-22 08:59:10', '2021-07-22 05:59:10', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2021-07-22 08:59:10', '2021-07-22 05:59:10', '', 8, 'http://yair:8888/?p=167', 0, 'revision', '', 0),
(168, 1, '2021-07-22 09:04:05', '2021-07-22 06:04:05', '', 'סליידר', '', 'inherit', 'open', 'closed', '', '%d7%a1%d7%9c%d7%99%d7%99%d7%93%d7%a8', '', '', '2021-07-22 09:04:05', '2021-07-22 06:04:05', '', 8, 'http://yair:8888/wp-content/uploads/2021/07/סליידר.png', 0, 'attachment', 'image/png', 0),
(169, 1, '2021-07-22 09:04:44', '2021-07-22 06:04:44', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2021-07-22 09:04:44', '2021-07-22 06:04:44', '', 8, 'http://yair:8888/?p=169', 0, 'revision', '', 0),
(170, 1, '2021-07-22 18:43:50', '2021-07-22 15:43:50', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים', 'כל הקטגוריות אצלנו', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2021-07-22 18:43:50', '2021-07-22 15:43:50', '', 16, 'http://yair:8888/?p=170', 0, 'revision', '', 0),
(171, 1, '2021-07-22 22:43:30', '2021-07-22 19:43:30', '<!-- wp:shortcode -->[yith_wcwl_wishlist]<!-- /wp:shortcode -->', 'רשימת משאלות', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2021-07-22 22:43:30', '2021-07-22 19:43:30', '', 0, 'http://yair:8888/wishlist/', 0, 'page', '', 0),
(172, 1, '2021-07-22 22:48:16', '2021-07-22 19:48:16', ' ', '', '', 'publish', 'closed', 'closed', '', '172', '', '', '2021-07-22 22:48:16', '2021-07-22 19:48:16', '', 0, 'http://yair:8888/?p=172', 1, 'nav_menu_item', '', 0),
(173, 1, '2021-07-22 22:48:16', '2021-07-22 19:48:16', ' ', '', '', 'publish', 'closed', 'closed', '', '173', '', '', '2021-07-22 22:48:16', '2021-07-22 19:48:16', '', 0, 'http://yair:8888/?p=173', 2, 'nav_menu_item', '', 0),
(174, 1, '2021-07-22 22:48:16', '2021-07-22 19:48:16', '', 'קטגוריות', '', 'publish', 'closed', 'closed', '', '%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa', '', '', '2021-07-22 22:48:16', '2021-07-22 19:48:16', '', 0, 'http://yair:8888/?p=174', 3, 'nav_menu_item', '', 0),
(175, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', ' ', '', '', 'publish', 'closed', 'closed', '', '175', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=175', 1, 'nav_menu_item', '', 0),
(176, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', ' ', '', '', 'publish', 'closed', 'closed', '', '176', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=176', 2, 'nav_menu_item', '', 0),
(177, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', ' ', '', '', 'publish', 'closed', 'closed', '', '177', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=177', 3, 'nav_menu_item', '', 0),
(178, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', ' ', '', '', 'publish', 'closed', 'closed', '', '178', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=178', 4, 'nav_menu_item', '', 0),
(179, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', ' ', '', '', 'publish', 'closed', 'closed', '', '179', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=179', 5, 'nav_menu_item', '', 0),
(180, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', ' ', '', '', 'publish', 'closed', 'closed', '', '180', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=180', 6, 'nav_menu_item', '', 0),
(181, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, נולום ארווס סאפיאן – פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק – וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה – לתכי מורגם בורק? לתיג ישבעס.', '', '', 'publish', 'closed', 'closed', '', '181', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=181', 7, 'nav_menu_item', '', 0),
(182, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', ' ', '', '', 'publish', 'closed', 'closed', '', '182', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=182', 8, 'nav_menu_item', '', 0),
(183, 1, '2021-07-22 23:21:35', '2021-07-22 19:49:30', ' ', '', '', 'publish', 'closed', 'closed', '', '183', '', '', '2021-07-22 23:21:35', '2021-07-22 20:21:35', '', 0, 'http://yair:8888/?p=183', 9, 'nav_menu_item', '', 0),
(184, 1, '2021-07-24 11:01:20', '2021-07-22 20:22:28', ' ', '', '', 'publish', 'closed', 'closed', '', '184', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=184', 5, 'nav_menu_item', '', 0),
(185, 1, '2021-07-24 11:01:20', '2021-07-22 20:22:28', ' ', '', '', 'publish', 'closed', 'closed', '', '185', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=185', 6, 'nav_menu_item', '', 0),
(186, 1, '2021-07-24 11:01:20', '2021-07-22 20:22:28', ' ', '', '', 'publish', 'closed', 'closed', '', '186', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=186', 7, 'nav_menu_item', '', 0),
(187, 1, '2021-07-24 11:01:20', '2021-07-22 20:22:28', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, נולום ארווס סאפיאן – פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק – וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה – לתכי מורגם בורק? לתיג ישבעס.', '', '', 'publish', 'closed', 'closed', '', '187', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=187', 8, 'nav_menu_item', '', 0),
(188, 1, '2021-07-24 11:01:20', '2021-07-22 20:58:55', ' ', '', '', 'publish', 'closed', 'closed', '', '188', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=188', 3, 'nav_menu_item', '', 0),
(189, 1, '2021-07-23 10:51:40', '2021-07-23 07:51:40', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'חיפוש', 'חיפוש', 'publish', 'closed', 'closed', '', 'field_60fa74f8d591a', '', '', '2021-07-23 10:51:40', '2021-07-23 07:51:40', '', 87, 'http://yair:8888/?post_type=acf-field&p=189', 9, 'acf-field', '', 0),
(190, 1, '2021-07-23 10:51:40', '2021-07-23 07:51:40', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'search_title', 'publish', 'closed', 'closed', '', 'field_60fa7504d591b', '', '', '2021-07-23 10:51:40', '2021-07-23 07:51:40', '', 87, 'http://yair:8888/?post_type=acf-field&p=190', 10, 'acf-field', '', 0),
(191, 1, '2021-07-23 11:44:26', '2021-07-23 08:44:26', 'בוחרים מזמינים ואנחנו על הקיר מדפיסים', 'מוצרים', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-07-23 11:44:26', '2021-07-23 08:44:26', '', 21, 'http://yair:8888/?p=191', 0, 'revision', '', 0),
(192, 1, '2021-07-23 12:34:23', '2021-07-23 09:34:23', '<h2>הצטרפו אלינו</h2>\r\n<h4>וקבלו במה להדפסת יצירות שלכם על הקיר</h4>\r\nחוצים. קלאצי סחטיר בלובק. איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר נפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.\r\n\r\nלפמעט מוסן מנת קולורס מונפרד אדנדום סילקוף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג', 'מכירה לאומן', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2021-07-23 12:34:23', '2021-07-23 09:34:23', '', 14, 'http://yair:8888/?p=192', 0, 'revision', '', 0),
(193, 1, '2021-07-23 12:39:58', '2021-07-23 09:39:58', '<h2>הצטרפו אלינו</h2>\r\n<h4>וקבלו במה להדפסת יצירות שלכם על הקיר</h4>\r\nחוצים. קלאצי סחטיר בלובק. איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר נפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.\r\n\r\nלפמעט מוסן מנת קולורס מונפרד אדנדום סילקוף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג', 'מכירה לאומן', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2021-07-23 12:39:58', '2021-07-23 09:39:58', '', 14, 'http://yair:8888/?p=193', 0, 'revision', '', 0),
(194, 1, '2021-07-24 11:01:20', '2021-07-23 09:41:09', ' ', '', '', 'publish', 'closed', 'closed', '', '194', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=194', 4, 'nav_menu_item', '', 0),
(195, 1, '2021-07-23 13:29:09', '2021-07-23 10:29:09', '<h2>הצטרפו אלינו</h2>\r\n<h4>וקבלו במה להדפסת יצירות שלכם על הקיר</h4>\r\nחוצים. קלאצי סחטיר בלובק. איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר נפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.\r\n\r\nלפמעט מוסן מנת קולורס מונפרד אדנדום סילקוף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג', 'מכירה לאומן', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2021-07-23 13:29:09', '2021-07-23 10:29:09', '', 14, 'http://yair:8888/?p=195', 0, 'revision', '', 0),
(196, 1, '2021-07-23 13:29:37', '2021-07-23 10:29:37', '<h3>שאלתם, ענינו, כל השאלות במקום אחד</h3>', 'שאלות נפוצות', '', 'publish', 'closed', 'closed', '', '%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa', '', '', '2021-07-24 12:36:53', '2021-07-24 09:36:53', '', 0, 'http://yair:8888/?page_id=196', 0, 'page', '', 0),
(197, 1, '2021-07-23 13:29:37', '2021-07-23 10:29:37', '', 'שאלות ותשובות', '', 'inherit', 'closed', 'closed', '', '196-revision-v1', '', '', '2021-07-23 13:29:37', '2021-07-23 10:29:37', '', 196, 'http://yair:8888/?p=197', 0, 'revision', '', 0),
(198, 1, '2021-07-23 13:30:07', '2021-07-23 10:30:07', '<h2>הצטרפו אלינו</h2>\r\n<h4>וקבלו במה להדפסת יצירות שלכם על הקיר</h4>\r\nחוצים. קלאצי סחטיר בלובק. איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר נפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.\r\n\r\nלפמעט מוסן מנת קולורס מונפרד אדנדום סילקוף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג', 'מכירה לאומן', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2021-07-23 13:30:07', '2021-07-23 10:30:07', '', 14, 'http://yair:8888/?p=198', 0, 'revision', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(199, 1, '2021-07-23 13:30:38', '2021-07-23 10:30:38', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2021-07-23 13:30:38', '2021-07-23 10:30:38', '', 8, 'http://yair:8888/?p=199', 0, 'revision', '', 0),
(200, 1, '2021-07-23 15:31:45', '2021-07-23 12:31:45', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"views/contact.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'צור קשר', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', 'publish', 'closed', 'closed', '', 'group_60fab66f1f7b5', '', '', '2021-07-23 15:50:18', '2021-07-23 12:50:18', '', 0, 'http://yair:8888/?post_type=acf-field-group&#038;p=200', 0, 'acf-field-group', '', 0),
(201, 1, '2021-07-23 15:31:45', '2021-07-23 12:31:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת של בלוק עם טלפון', 'contact_block_title', 'publish', 'closed', 'closed', '', 'field_60fab67f4dfa1', '', '', '2021-07-23 15:50:18', '2021-07-23 12:50:18', '', 200, 'http://yair:8888/?post_type=acf-field&#038;p=201', 2, 'acf-field', '', 0),
(202, 1, '2021-07-23 15:31:45', '2021-07-23 12:31:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת טופס', 'contact_form_title', 'publish', 'closed', 'closed', '', 'field_60fab6984dfa2', '', '', '2021-07-23 15:50:18', '2021-07-23 12:50:18', '', 200, 'http://yair:8888/?post_type=acf-field&#038;p=202', 3, 'acf-field', '', 0),
(203, 1, '2021-07-23 15:41:40', '2021-07-23 12:41:40', '', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2021-07-23 15:41:40', '2021-07-23 12:41:40', '', 10, 'http://yair:8888/?p=203', 0, 'revision', '', 0),
(204, 1, '2021-07-23 15:50:11', '2021-07-23 12:50:11', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'שם בפייסבוק', 'facebook_name', 'publish', 'closed', 'closed', '', 'field_60fabada89aa4', '', '', '2021-07-23 15:50:18', '2021-07-23 12:50:18', '', 200, 'http://yair:8888/?post_type=acf-field&#038;p=204', 0, 'acf-field', '', 0),
(205, 1, '2021-07-23 15:50:11', '2021-07-23 12:50:11', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'שם באינסטגרם', 'instagram_name', 'publish', 'closed', 'closed', '', 'field_60fabaee89aa5', '', '', '2021-07-23 15:50:18', '2021-07-23 12:50:18', '', 200, 'http://yair:8888/?post_type=acf-field&#038;p=205', 1, 'acf-field', '', 0),
(206, 1, '2021-07-23 15:50:53', '2021-07-23 12:50:53', '', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2021-07-23 15:50:53', '2021-07-23 12:50:53', '', 10, 'http://yair:8888/?p=206', 0, 'revision', '', 0),
(207, 1, '2021-07-23 19:28:31', '2021-07-23 16:28:31', '<h4>בוחרים מזמינים ואנחנו על הקיר מדפיסים</h4>\r\nחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג', 'דף אומנים', '', 'publish', 'closed', 'closed', '', '%d7%93%d7%a3-%d7%90%d7%95%d7%9e%d7%a0%d7%99%d7%9d', '', '', '2021-07-23 20:03:17', '2021-07-23 17:03:17', '', 0, 'http://yair:8888/?page_id=207', 0, 'page', '', 0),
(208, 1, '2021-07-23 19:28:31', '2021-07-23 16:28:31', '', 'דף אומנים', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2021-07-23 19:28:31', '2021-07-23 16:28:31', '', 207, 'http://yair:8888/?p=208', 0, 'revision', '', 0),
(209, 1, '2021-07-23 19:29:12', '2021-07-23 16:29:12', '', 'דף אומנים', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2021-07-23 19:29:12', '2021-07-23 16:29:12', '', 207, 'http://yair:8888/?p=209', 0, 'revision', '', 0),
(210, 1, '2021-07-24 11:01:20', '2021-07-23 17:01:43', ' ', '', '', 'publish', 'closed', 'closed', '', '210', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=210', 1, 'nav_menu_item', '', 0),
(211, 1, '2021-07-23 20:02:57', '2021-07-23 17:02:57', '<h3>בוחרים מזמינים ואנחנו על הקיר מדפיסים</h3>\r\nחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג', 'דף אומנים', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2021-07-23 20:02:57', '2021-07-23 17:02:57', '', 207, 'http://yair:8888/?p=211', 0, 'revision', '', 0),
(212, 1, '2021-07-23 20:03:17', '2021-07-23 17:03:17', '<h4>בוחרים מזמינים ואנחנו על הקיר מדפיסים</h4>\r\nחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג', 'דף אומנים', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2021-07-23 20:03:17', '2021-07-23 17:03:17', '', 207, 'http://yair:8888/?p=212', 0, 'revision', '', 0),
(213, 1, '2021-07-24 11:01:20', '2021-07-23 18:20:29', ' ', '', '', 'publish', 'closed', 'closed', '', '213', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=213', 9, 'nav_menu_item', '', 0),
(214, 1, '2021-07-24 11:01:20', '2021-07-24 08:01:20', ' ', '', '', 'publish', 'closed', 'closed', '', '214', '', '', '2021-07-24 11:01:20', '2021-07-24 08:01:20', '', 0, 'http://yair:8888/?p=214', 2, 'nav_menu_item', '', 0),
(215, 1, '2021-07-24 12:32:30', '2021-07-24 09:32:30', '', 'שאלות ותשובות', '', 'inherit', 'closed', 'closed', '', '196-revision-v1', '', '', '2021-07-24 12:32:30', '2021-07-24 09:32:30', '', 196, 'http://yair:8888/?p=215', 0, 'revision', '', 0),
(216, 1, '2021-07-24 12:36:42', '2021-07-24 09:36:42', '<h2>שאלות נפוצות</h2>\r\n<h3>שאלתם, ענינו, כל השאלות במקום אחד</h3>', 'שאלות ותשובות', '', 'inherit', 'closed', 'closed', '', '196-revision-v1', '', '', '2021-07-24 12:36:42', '2021-07-24 09:36:42', '', 196, 'http://yair:8888/?p=216', 0, 'revision', '', 0),
(217, 1, '2021-07-24 12:36:53', '2021-07-24 09:36:53', '<h3>שאלתם, ענינו, כל השאלות במקום אחד</h3>', 'שאלות נפוצות', '', 'inherit', 'closed', 'closed', '', '196-revision-v1', '', '', '2021-07-24 12:36:53', '2021-07-24 09:36:53', '', 196, 'http://yair:8888/?p=217', 0, 'revision', '', 0),
(218, 1, '2021-07-24 21:43:14', '2021-07-24 18:43:14', 'לורם היפסום בוחרים מזמינים ואנחנו על הקיר מדפיסים', 'גלריה', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-07-24 21:43:14', '2021-07-24 18:43:14', '', 12, 'http://yair:8888/?p=218', 0, 'revision', '', 0),
(219, 1, '2021-07-24 21:43:50', '2021-07-24 18:43:50', '', 'gallery-img-1', '', 'inherit', 'open', 'closed', '', 'gallery-img-1', '', '', '2021-07-24 21:43:50', '2021-07-24 18:43:50', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-1.png', 0, 'attachment', 'image/png', 0),
(220, 1, '2021-07-24 21:43:51', '2021-07-24 18:43:51', '', 'gallery-img-2', '', 'inherit', 'open', 'closed', '', 'gallery-img-2', '', '', '2021-07-24 21:43:51', '2021-07-24 18:43:51', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-2.png', 0, 'attachment', 'image/png', 0),
(221, 1, '2021-07-24 21:43:52', '2021-07-24 18:43:52', '', 'gallery-img-3', '', 'inherit', 'open', 'closed', '', 'gallery-img-3', '', '', '2021-07-24 21:43:52', '2021-07-24 18:43:52', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-3.png', 0, 'attachment', 'image/png', 0),
(222, 1, '2021-07-24 21:43:53', '2021-07-24 18:43:53', '', 'gallery-img-4', '', 'inherit', 'open', 'closed', '', 'gallery-img-4', '', '', '2021-07-24 21:43:53', '2021-07-24 18:43:53', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-4.png', 0, 'attachment', 'image/png', 0),
(223, 1, '2021-07-24 21:43:55', '2021-07-24 18:43:55', '', 'gallery-img-5', '', 'inherit', 'open', 'closed', '', 'gallery-img-5', '', '', '2021-07-24 21:43:55', '2021-07-24 18:43:55', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-5.png', 0, 'attachment', 'image/png', 0),
(224, 1, '2021-07-24 21:43:56', '2021-07-24 18:43:56', '', 'gallery-img-6', '', 'inherit', 'open', 'closed', '', 'gallery-img-6', '', '', '2021-07-24 21:43:56', '2021-07-24 18:43:56', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-6.png', 0, 'attachment', 'image/png', 0),
(225, 1, '2021-07-24 21:43:58', '2021-07-24 18:43:58', '', 'gallery-img-7', '', 'inherit', 'open', 'closed', '', 'gallery-img-7', '', '', '2021-07-24 21:43:58', '2021-07-24 18:43:58', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-7.png', 0, 'attachment', 'image/png', 0),
(226, 1, '2021-07-24 21:44:01', '2021-07-24 18:44:01', '', 'gallery-img-8', '', 'inherit', 'open', 'closed', '', 'gallery-img-8', '', '', '2021-07-24 21:44:01', '2021-07-24 18:44:01', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-8.png', 0, 'attachment', 'image/png', 0),
(227, 1, '2021-07-24 21:44:02', '2021-07-24 18:44:02', '', 'gallery-img-9', '', 'inherit', 'open', 'closed', '', 'gallery-img-9', '', '', '2021-07-24 21:44:02', '2021-07-24 18:44:02', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-9.png', 0, 'attachment', 'image/png', 0),
(228, 1, '2021-07-24 21:44:04', '2021-07-24 18:44:04', '', 'gallery-img-10', '', 'inherit', 'open', 'closed', '', 'gallery-img-10', '', '', '2021-07-24 21:44:04', '2021-07-24 18:44:04', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-10.png', 0, 'attachment', 'image/png', 0),
(229, 1, '2021-07-24 21:44:07', '2021-07-24 18:44:07', '', 'gallery-img-11', '', 'inherit', 'open', 'closed', '', 'gallery-img-11', '', '', '2021-07-24 21:44:07', '2021-07-24 18:44:07', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-11.png', 0, 'attachment', 'image/png', 0),
(230, 1, '2021-07-24 21:44:09', '2021-07-24 18:44:09', '', 'gallery-img-12', '', 'inherit', 'open', 'closed', '', 'gallery-img-12', '', '', '2021-07-24 21:44:09', '2021-07-24 18:44:09', '', 12, 'http://yair:8888/wp-content/uploads/2021/07/gallery-img-12.png', 0, 'attachment', 'image/png', 0),
(231, 1, '2021-07-24 21:45:40', '2021-07-24 18:45:40', 'לורם היפסום בוחרים מזמינים ואנחנו על הקיר מדפיסים', 'גלריה', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-07-24 21:45:40', '2021-07-24 18:45:40', '', 12, 'http://yair:8888/?p=231', 0, 'revision', '', 0),
(232, 1, '2021-07-25 16:38:19', '2021-07-25 13:38:19', '', 'sample-video', '', 'inherit', 'open', 'closed', '', 'sample-video', '', '', '2021-07-25 16:38:19', '2021-07-25 13:38:19', '', 8, 'http://yair:8888/wp-content/uploads/2021/07/sample-video.mp4', 0, 'attachment', 'video/mp4', 0),
(233, 1, '2021-07-25 16:38:24', '2021-07-25 13:38:24', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2021-07-25 16:38:24', '2021-07-25 13:38:24', '', 8, 'http://yair:8888/?p=233', 0, 'revision', '', 0),
(234, 1, '2021-07-25 17:20:25', '2021-07-25 14:20:25', 'לורם היפסום בוחרים מזמינים ואנחנו על הקיר מדפיסים', 'גלריה', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-07-25 17:20:25', '2021-07-25 14:20:25', '', 12, 'http://yair:8888/?p=234', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_termmeta`
--

CREATE TABLE `fmn_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_termmeta`
--

INSERT INTO `fmn_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 16, 'order', '0'),
(2, 16, 'display_type', ''),
(3, 16, 'thumbnail_id', '102'),
(4, 17, 'order', '0'),
(5, 17, 'display_type', ''),
(6, 17, 'thumbnail_id', '103'),
(7, 18, 'order', '0'),
(8, 18, 'display_type', ''),
(9, 18, 'thumbnail_id', '104'),
(10, 19, 'order', '0'),
(11, 19, 'display_type', ''),
(12, 19, 'thumbnail_id', '105'),
(13, 20, 'order', '0'),
(14, 20, 'display_type', ''),
(15, 20, 'thumbnail_id', '113'),
(16, 21, 'order', '0'),
(17, 21, 'display_type', ''),
(18, 21, 'thumbnail_id', '106'),
(19, 22, 'order', '0'),
(20, 22, 'display_type', ''),
(21, 22, 'thumbnail_id', '107'),
(22, 23, 'order', '0'),
(23, 23, 'display_type', ''),
(24, 23, 'thumbnail_id', '108'),
(25, 24, 'order', '0'),
(26, 24, 'display_type', ''),
(27, 24, 'thumbnail_id', '109'),
(28, 25, 'order', '0'),
(29, 25, 'display_type', ''),
(30, 25, 'thumbnail_id', '110'),
(31, 26, 'order', '0'),
(32, 26, 'display_type', ''),
(33, 26, 'thumbnail_id', '111'),
(34, 27, 'order', '0'),
(35, 27, 'display_type', ''),
(36, 27, 'thumbnail_id', '112'),
(37, 28, 'cat_img', '116'),
(38, 28, '_cat_img', 'field_60f864066a163'),
(39, 29, 'cat_img', '117'),
(40, 29, '_cat_img', 'field_60f864066a163'),
(41, 30, 'cat_img', '118'),
(42, 30, '_cat_img', 'field_60f864066a163'),
(43, 31, 'cat_img', '119'),
(44, 31, '_cat_img', 'field_60f864066a163'),
(45, 32, 'cat_img', '120'),
(46, 32, '_cat_img', 'field_60f864066a163'),
(47, 33, 'cat_img', '121'),
(48, 33, '_cat_img', 'field_60f864066a163'),
(49, 34, 'cat_img', '122'),
(50, 34, '_cat_img', 'field_60f864066a163'),
(51, 35, 'cat_img', '123'),
(52, 35, '_cat_img', 'field_60f864066a163'),
(53, 36, 'cat_img', '124'),
(54, 36, '_cat_img', 'field_60f864066a163'),
(55, 37, 'cat_img', '121'),
(56, 37, '_cat_img', 'field_60f864066a163'),
(57, 38, 'cat_img', '118'),
(58, 38, '_cat_img', 'field_60f864066a163'),
(59, 19, 'product_count_product_cat', '1'),
(60, 21, 'product_count_product_cat', '5'),
(61, 26, 'product_count_product_cat', '4'),
(62, 24, 'product_count_product_cat', '6'),
(63, 27, 'product_count_product_cat', '5'),
(64, 23, 'product_count_product_cat', '2'),
(65, 16, 'product_count_product_cat', '3'),
(66, 20, 'product_count_product_cat', '3'),
(67, 25, 'product_count_product_cat', '2'),
(68, 22, 'product_count_product_cat', '1'),
(69, 18, 'product_count_product_cat', '3'),
(70, 17, 'product_count_product_cat', '1'),
(71, 15, 'display_type', ''),
(72, 15, 'thumbnail_id', '113');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_terms`
--

CREATE TABLE `fmn_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_terms`
--

INSERT INTO `fmn_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'כללי', '%d7%9b%d7%9c%d7%9c%d7%99', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'כללי', 'uncategorized', 0),
(16, 'חדרי שינה', '%d7%97%d7%93%d7%a8%d7%99-%d7%a9%d7%99%d7%a0%d7%94', 0),
(17, 'עסקים', '%d7%a2%d7%a1%d7%a7%d7%99%d7%9d', 0),
(18, 'משרדים', '%d7%9e%d7%a9%d7%a8%d7%93%d7%99%d7%9d', 0),
(19, 'חדרי ילדים', '%d7%97%d7%93%d7%a8%d7%99-%d7%99%d7%9c%d7%93%d7%99%d7%9d', 0),
(20, 'סלון', '%d7%a1%d7%9c%d7%95%d7%9f', 0),
(21, 'לורם היפסום שם הקטגוריה 1', '%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-1', 0),
(22, 'לורם היפסום שם הקטגוריה 2', '%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-2', 0),
(23, 'לורם היפסום שם הקטגוריה 3', '%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-3', 0),
(24, 'לורם היפסום שם הקטגוריה 4', '%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-4', 0),
(25, 'לורם היפסום שם הקטגוריה 5', '%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-5', 0),
(26, 'לורם היפסום שם הקטגוריה 6', '%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-6', 0),
(27, 'לורם היפסום שם הקטגוריה 7', '%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-7', 0),
(28, 'שם האומן לורם היפסום 1', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', 0),
(29, 'שם האומן לורם היפסום 2', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', 0),
(30, 'שם האומן לורם היפסום 3', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', 0),
(31, 'שם האומן לורם היפסום 4', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', 0),
(32, 'שם האומן לורם היפסום 5', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', 0),
(33, 'שם האומן לורם היפסום 6', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6', 0),
(34, 'שם האומן לורם היפסום 7', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-7', 0),
(35, 'שם האומן לורם היפסום 8', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-8', 0),
(36, 'שם האומן לורם היפסום 9', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-9', 0),
(37, 'שם האומן לורם היפסום 10', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-10', 0),
(38, 'שם האומן לורם היפסום 11', '%d7%a9%d7%9d-%d7%94%d7%90%d7%95%d7%9e%d7%9f-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-11', 0),
(39, 'Header menu', 'header-menu', 0),
(40, 'Footer menu', 'footer-menu', 0),
(41, 'Dropdown Menu', 'dropdown-menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_relationships`
--

CREATE TABLE `fmn_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_relationships`
--

INSERT INTO `fmn_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(26, 1, 0),
(34, 1, 0),
(70, 1, 0),
(125, 2, 0),
(125, 21, 0),
(125, 26, 0),
(125, 28, 0),
(137, 2, 0),
(137, 24, 0),
(137, 26, 0),
(137, 27, 0),
(137, 29, 0),
(138, 2, 0),
(138, 19, 0),
(138, 21, 0),
(138, 23, 0),
(138, 30, 0),
(139, 2, 0),
(139, 16, 0),
(139, 20, 0),
(139, 24, 0),
(139, 26, 0),
(139, 31, 0),
(140, 2, 0),
(140, 23, 0),
(140, 24, 0),
(140, 25, 0),
(140, 32, 0),
(141, 2, 0),
(141, 18, 0),
(141, 20, 0),
(141, 22, 0),
(141, 27, 0),
(141, 33, 0),
(142, 2, 0),
(142, 27, 0),
(142, 34, 0),
(143, 2, 0),
(143, 18, 0),
(143, 21, 0),
(143, 24, 0),
(143, 35, 0),
(144, 2, 0),
(144, 20, 0),
(144, 25, 0),
(144, 36, 0),
(145, 2, 0),
(145, 16, 0),
(145, 38, 0),
(150, 2, 0),
(150, 21, 0),
(150, 24, 0),
(150, 27, 0),
(150, 38, 0),
(151, 2, 0),
(151, 17, 0),
(151, 24, 0),
(151, 33, 0),
(151, 35, 0),
(152, 2, 0),
(152, 21, 0),
(152, 34, 0),
(153, 2, 0),
(153, 16, 0),
(153, 26, 0),
(153, 29, 0),
(154, 2, 0),
(154, 18, 0),
(154, 27, 0),
(154, 28, 0),
(172, 39, 0),
(173, 39, 0),
(174, 39, 0),
(175, 40, 0),
(176, 40, 0),
(177, 40, 0),
(178, 40, 0),
(179, 40, 0),
(180, 40, 0),
(181, 40, 0),
(182, 40, 0),
(183, 40, 0),
(184, 41, 0),
(185, 41, 0),
(186, 41, 0),
(187, 41, 0),
(188, 41, 0),
(194, 41, 0),
(210, 41, 0),
(213, 41, 0),
(214, 41, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_taxonomy`
--

CREATE TABLE `fmn_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_taxonomy`
--

INSERT INTO `fmn_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 15),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'product_cat', '', 0, 3),
(17, 17, 'product_cat', '', 0, 1),
(18, 18, 'product_cat', '', 0, 3),
(19, 19, 'product_cat', '', 0, 1),
(20, 20, 'product_cat', '', 0, 3),
(21, 21, 'product_cat', '', 0, 5),
(22, 22, 'product_cat', '', 0, 1),
(23, 23, 'product_cat', '', 0, 2),
(24, 24, 'product_cat', '', 0, 6),
(25, 25, 'product_cat', '', 0, 2),
(26, 26, 'product_cat', '', 0, 4),
(27, 27, 'product_cat', '', 0, 5),
(28, 28, 'artist', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 0, 2),
(29, 29, 'artist', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 0, 2),
(30, 30, 'artist', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n', 0, 1),
(31, 31, 'artist', 'וסטיבולום סוליסי טידום בעליק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 0, 1),
(32, 32, 'artist', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n', 0, 1),
(33, 33, 'artist', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 0, 2),
(34, 34, 'artist', 'להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n', 0, 2),
(35, 35, 'artist', 'קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט\r\n', 0, 2),
(36, 36, 'artist', 'להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 0, 1),
(37, 37, 'artist', 'קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט\r\n', 0, 0),
(38, 38, 'artist', 'קלאצי קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט\r\n', 0, 2),
(39, 39, 'nav_menu', '', 0, 3),
(40, 40, 'nav_menu', '', 0, 9),
(41, 41, 'nav_menu', '', 0, 9);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_usermeta`
--

CREATE TABLE `fmn_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_usermeta`
--

INSERT INTO `fmn_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'dev_admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'fmn_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'fmn_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"fa124c9e7fccff2318e14c76d343f9aab70f44a5d36bbc45727279fea0de7a82\";a:4:{s:10:\"expiration\";i:1627392909;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36\";s:5:\"login\";i:1627220109;}}'),
(17, 1, 'fmn_dashboard_quick_press_last_post_id', '4'),
(18, 1, '_woocommerce_tracks_anon_id', 'woo:mC+R0jtRNhFL68AUX96qGhqV'),
(19, 1, 'last_update', '1627030698'),
(20, 1, 'woocommerce_admin_activity_panel_inbox_last_read', '1627030696838'),
(21, 1, 'woocommerce_admin_task_list_tracked_started_tasks', '{\"appearance\":1}'),
(22, 1, 'dismissed_no_secure_connection_notice', '1'),
(23, 1, 'wc_last_active', '1627171200'),
(25, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(26, 1, '_order_count', '0'),
(27, 1, 'fmn_user-settings', 'libraryContent=browse'),
(28, 1, 'fmn_user-settings-time', '1626891154'),
(29, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(30, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-product_tag\";}'),
(31, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(32, 1, 'nav_menu_recently_edited', '41');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_users`
--

CREATE TABLE `fmn_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_users`
--

INSERT INTO `fmn_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'dev_admin', '$P$BbY.pYlVrk5Nd6HG//FmhEX8RoN3mT0', 'dev_admin', 'maxf@leos.co.il', '', '2021-07-18 15:14:07', '', 0, 'dev_admin');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_admin_notes`
--

CREATE TABLE `fmn_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  `layout` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_admin_notes`
--

INSERT INTO `fmn_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`, `layout`, `image`, `is_deleted`, `icon`) VALUES
(1, 'wayflyer_q3_2021', 'marketing', 'en_US', 'Grow your revenue with Wayflyer financing and analytics', 'Flexible financing tailored to your needs by <a href=\"https://woocommerce.com/products/wayflyer/\">Wayflyer</a> – one fee, no interest rates, penalties, equity, or personal guarantees. Based on your store\'s performance, Wayflyer can provide the financing you need to grow and the analytical insights to help you spend it.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(2, 'eu_vat_changes_2021', 'marketing', 'en_US', 'Get your business ready for the new EU tax regulations', 'On July 1, 2021, new taxation rules will come into play when the <a href=\"https://ec.europa.eu/taxation_customs/business/vat/modernising-vat-cross-border-ecommerce_en\">European Union (EU) Value-Added Tax (VAT) eCommerce package</a> takes effect.<br /><br />The new regulations will impact virtually every B2C business involved in cross-border eCommerce trade with the EU.<br /><br />We therefore recommend <a href=\"https://woocommerce.com/posts/new-eu-vat-regulations\">familiarizing yourself with the new updates</a>, and consult with a tax professional to ensure your business is following regulations and best practice.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(3, 'paypal_ppcp_gtm_2021', 'marketing', 'en_US', 'Offer more options with the new PayPal', 'Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(4, 'facebook_pixel_api_2021', 'marketing', 'en_US', 'Improve the performance of your Facebook ads', 'Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(5, 'facebook_ec_2021', 'marketing', 'en_US', 'Sync your product catalog with Facebook to help boost sales', 'A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(6, 'ecomm-need-help-setting-up-your-store', 'info', 'en_US', 'Need help setting up your Store?', 'Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(7, 'woocommerce-services', 'info', 'en_US', 'WooCommerce Shipping & Tax', 'WooCommerce Shipping &amp; Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(8, 'ecomm-unique-shopping-experience', 'info', 'en_US', 'For a shopping experience as unique as your customers', 'Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(9, 'wc-admin-getting-started-in-ecommerce', 'info', 'en_US', 'Getting Started in eCommerce - webinar', 'We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:52', NULL, 0, 'plain', '', 0, 'info'),
(10, 'your-first-product', 'info', 'en_US', 'Your first product', 'That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br /><br />Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.', '{}', 'unactioned', 'woocommerce.com', '2021-07-21 18:22:01', NULL, 0, 'plain', '', 0, 'info'),
(11, 'wc-square-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(12, 'wc-square-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with Square and Apple Pay ', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(13, 'wcpay-apple-pay-is-now-available', 'marketing', 'en_US', 'Apple Pay is now available with WooCommerce Payments!', 'Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(14, 'wcpay-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(15, 'wcpay-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with WooCommerce Payments and Apple Pay', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(16, 'wc-admin-optimizing-the-checkout-flow', 'info', 'en_US', 'Optimizing the checkout flow', 'It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(17, 'wc-admin-first-five-things-to-customize', 'info', 'en_US', 'The first 5 things to customize in your store', 'Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.', '{}', 'unactioned', 'woocommerce.com', '2021-07-21 14:04:16', NULL, 0, 'plain', '', 0, 'info'),
(18, 'wc-payments-qualitative-feedback', 'info', 'en_US', 'WooCommerce Payments setup - let us know what you think', 'Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(19, 'share-your-feedback-on-paypal', 'info', 'en_US', 'Share your feedback on PayPal', 'Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(20, 'wcpay_instant_deposits_gtm_2021', 'marketing', 'en_US', 'Get paid within minutes – Instant Deposits for WooCommerce Payments', 'Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(21, 'google_listings_and_ads_install', 'marketing', 'en_US', 'Drive traffic and sales with Google', 'Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(22, 'wc-subscriptions-security-update-3-0-15', 'info', 'en_US', 'WooCommerce Subscriptions security update!', 'We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br /><br />Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br /><br />We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br /><br />If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(23, 'woocommerce-core-update-5-4-0', 'info', 'en_US', 'Update to WooCommerce 5.4.1 now', 'WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(24, 'wcpay-promo-2020-11', 'marketing', 'en_US', 'wcpay-promo-2020-11', 'wcpay-promo-2020-11', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(25, 'wcpay-promo-2020-12', 'marketing', 'en_US', 'wcpay-promo-2020-12', 'wcpay-promo-2020-12', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(26, 'wcpay-promo-2021-6-incentive-1', 'marketing', 'en_US', 'Simplify the payments process for you and your customers with WooCommerce Payments', 'With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies. \n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br /><br />\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">Terms of Service</a> \n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(27, 'wcpay-promo-2021-6-incentive-2', 'marketing', 'en_US', 'Simplify the payments process for you and your customers with WooCommerce Payments', 'With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies. \n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br /><br />\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">Terms of Service</a> \n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(28, 'ppxo-pps-upgrade-paypal-payments-1', 'info', 'en_US', 'Get the latest PayPal extension for WooCommerce', 'Heads up! There\'s a new PayPal on the block!<br /><br />Now is a great time to upgrade to our latest <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension</a> to continue to receive support and updates with PayPal.<br /><br />Get access to a full suite of PayPal payment methods, extensive currency and country coverage, and pay later options with the all-new PayPal extension for WooCommerce.', '{}', 'unactioned', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(29, 'ppxo-pps-upgrade-paypal-payments-2', 'info', 'en_US', 'Upgrade your PayPal experience!', 'We\'ve developed a whole new <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension for WooCommerce</a> that combines the best features of our many PayPal extensions into just one extension.<br /><br />Get access to a full suite of PayPal payment methods, extensive currency and country coverage, offer subscription and recurring payments, and the new PayPal pay later options.<br /><br />Start using our latest PayPal today to continue to receive support and updates.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(30, 'woocommerce-core-sqli-july-2021-need-to-update', 'update', 'en_US', 'Action required: Critical vulnerabilities in WooCommerce', 'In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br />Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br /><br />For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(31, 'woocommerce-blocks-sqli-july-2021-need-to-update', 'update', 'en_US', 'Action required: Critical vulnerabilities in WooCommerce Blocks', 'In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br />Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br /><br />For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(32, 'woocommerce-core-sqli-july-2021-store-patched', 'update', 'en_US', 'Solved: Critical vulnerabilities patched in WooCommerce', 'In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br /><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(33, 'woocommerce-blocks-sqli-july-2021-store-patched', 'update', 'en_US', 'Solved: Critical vulnerabilities patched in WooCommerce Blocks', 'In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br /><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(34, 'wc-admin-onboarding-email-marketing', 'info', 'en_US', 'Sign up for tips, product updates, and inspiration', 'We\'re here for you - get tips, product updates and inspiration straight to your email box', '{}', 'unactioned', 'woocommerce-admin', '2021-07-19 14:03:53', NULL, 0, 'plain', '', 0, 'info'),
(35, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-19 14:03:54', NULL, 0, 'plain', '', 0, 'info'),
(36, 'wc-admin-choosing-a-theme', 'marketing', 'en_US', 'רוצה לבחור ערכת עיצוב?', 'כדאי לעיין בערכות העיצוב שתואמות ל-WooCommerce ולבחור ערכת עיצוב שמתאימה לצרכים של המותג והעסק שלך.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-21 07:34:52', NULL, 0, 'plain', '', 0, 'info'),
(37, 'wc-admin-insight-first-product-and-payment', 'survey', 'en_US', 'תובנות', 'מעל 80% מהסוחרים החדשים מוסיפים את המוצר הראשון שלהם ומגדירים לפחות אמצעי תשלום אחד במהלך השבוע הראשון.<br><br>האם התובנות האלה מועילות לך?', '{}', 'unactioned', 'woocommerce-admin', '2021-07-21 07:34:52', NULL, 0, 'plain', '', 0, 'info'),
(38, 'wc-admin-mobile-app', 'info', 'en_US', 'להתקין את האפליקציה של Woo לנייד', 'כדאי להתקין את האפליקציה של WooCommerce לנייד כדי לנהל הזמנות, לקבל הודעות על מכירות ולהציג מדדים חשובים – בכל מקום.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-21 14:04:16', NULL, 0, 'plain', '', 0, 'info'),
(39, 'wc-admin-add-first-product-note', 'email', 'en_US', 'להוסיף את המוצר הראשון שלך', '{greetings}<br /><br />Nice one; יצרת חנות ב-WooCommerce! כעת אפשר להוסיף את המוצר הראשון שלך ולהתכונן למכירה הראשונה.<br /><br />קיימות שלוש דרכים להוסיף מוצרים: אפשר <strong>ליצור את המוצרים באופן ידני, לייבא מספר מוצרים במקביל דרך קובץ CSV‏</strong> או <strong>להעביר אותם משירות אחר</strong>.<br /><br /><a href=\"https://docs.woocommerce.com/document/managing-products/?utm_source=help_panel\">כדאי לעיין במסמכים שלנו</a> למידע נוסף, או פשוט להתחיל כעת!', '{\"role\":\"administrator\"}', 'unactioned', 'woocommerce-admin', '2021-07-21 14:04:16', NULL, 0, 'plain', 'http://yair:8888/wp-content/plugins/woocommerce/packages/woocommerce-admin/images/admin_notes/dashboard-widget-setup.png', 0, 'info'),
(40, 'wc-admin-learn-more-about-variable-products', 'info', 'en_US', 'למידע נוסף בנושא מוצרים עם סוגים', 'מוצרים עם סוגים הם מוצרים עוצמתיים שמאפשרים לך להציע ללקוחות מגוון של סוגים בכל מוצר ולשלוט במחיר, במלאי, בתמונה ובמאפיינים נוספים של כל סוג. אפשר להשתמש באפשרות זאת במוצרים כגון חולצות, ולהציע ללקוחות מידה גדולה, בינונית או קטנה וצבעים שונים.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-21 18:21:10', NULL, 0, 'plain', '', 0, 'info'),
(41, 'wc-admin-customizing-product-catalog', 'info', 'en_US', 'איך להתאים אישית את קטלוג המוצרים שלך', 'כדאי ליצור קטלוג מוצרים ותמונות מוצר שנראים נהדר ומתאימים למותג שלך. המדריך הזה מספק את כל העצות שחשוב לדעת כדי להציג את המוצרים שלך בצורה מושכת בחנות.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-23 16:07:13', NULL, 0, 'plain', '', 0, 'info'),
(42, 'wc-admin-onboarding-payments-reminder', 'info', 'en_US', 'להתחיל לקבל תשלומים בחנות שלך!', 'לגבות תשלומים באמצעות הספק שמתאים לך – ניתן לבחור בין מעל 100 ספקי תשלום שונים ב-WooCommerce.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-24 18:40:28', NULL, 0, 'plain', '', 0, 'info'),
(43, 'wc-admin-marketing-intro', 'info', 'en_US', 'להתחבר לקהל שלך', 'באפשרותך להרחיב את קהל הלקוחות שלך ולהגדיל את המכירות בעזרת כלי שיווק שנועדו לשימוש ב-WooCommerce.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-24 18:40:28', NULL, 0, 'plain', '', 0, 'info');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_admin_note_actions`
--

CREATE TABLE `fmn_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `actioned_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonce_action` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `nonce_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_admin_note_actions`
--

INSERT INTO `fmn_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`, `actioned_text`, `nonce_action`, `nonce_name`) VALUES
(37, 34, 'yes-please', 'Yes please!', 'https://woocommerce.us8.list-manage.com/subscribe/post?u=2c1434dc56f9506bf3c3ecd21&amp;id=13860df971&amp;SIGNUPPAGE=plugin', 'actioned', 0, '', NULL, NULL),
(74, 35, 'connect', 'Connect', '?page=wc-addons&section=helper', 'unactioned', 0, '', NULL, NULL),
(147, 36, 'visit-the-theme-marketplace', 'מעבר לחנות של ערכות העיצוב', 'https://woocommerce.com/product-category/themes/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(148, 37, 'affirm-insight-first-product-and-payment', 'כן', '', 'actioned', 0, 'תודה על המשוב', NULL, NULL),
(149, 37, 'affirm-insight-first-product-and-payment', 'לא', '', 'actioned', 0, 'תודה על המשוב', NULL, NULL),
(186, 38, 'learn-more', 'מידע נוסף', 'https://woocommerce.com/mobile/', 'actioned', 0, '', NULL, NULL),
(187, 39, 'add-first-product', 'להוסיף מוצר', 'http://yair:8888/wp-admin/admin.php?page=wc-admin&task=products', 'actioned', 0, '', NULL, NULL),
(224, 40, 'learn-more', 'מידע נוסף', 'https://docs.woocommerce.com/document/variable-product/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(909, 41, 'day-after-first-product', 'מידע נוסף', 'https://docs.woocommerce.com/document/woocommerce-customizer/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(982, 42, 'view-payment-gateways', 'מידע נוסף', 'https://woocommerce.com/product-category/woocommerce-extensions/payment-gateways/', 'actioned', 1, '', NULL, NULL),
(983, 43, 'open-marketing-hub', 'לפתוח את מרכז השיווק', 'http://yair:8888/wp-admin/admin.php?page=wc-admin&path=/marketing', 'actioned', 0, '', NULL, NULL),
(984, 1, 'wayflyer_q3_2021', 'Get funded', 'https://woocommerce.com/products/wayflyer/', 'actioned', 1, '', NULL, NULL),
(985, 2, 'eu_vat_changes_2021', 'Learn more about the EU tax regulations', 'https://woocommerce.com/posts/new-eu-vat-regulations', 'actioned', 1, '', NULL, NULL),
(986, 3, 'open_wc_paypal_payments_product_page', 'Learn more', 'https://woocommerce.com/products/woocommerce-paypal-payments/', 'actioned', 1, '', NULL, NULL),
(987, 4, 'upgrade_now_facebook_pixel_api', 'Upgrade now', 'plugin-install.php?tab=plugin-information&plugin=&section=changelog', 'actioned', 1, '', NULL, NULL),
(988, 5, 'learn_more_facebook_ec', 'Learn more', 'https://woocommerce.com/products/facebook/', 'unactioned', 1, '', NULL, NULL),
(989, 6, 'set-up-concierge', 'Schedule free session', 'https://wordpress.com/me/concierge', 'actioned', 1, '', NULL, NULL),
(990, 7, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(991, 8, 'learn-more-ecomm-unique-shopping-experience', 'Learn more', 'https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(992, 9, 'watch-the-webinar', 'Watch the webinar', 'https://youtu.be/V_2XtCOyZ7o', 'actioned', 1, '', NULL, NULL),
(993, 10, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(994, 11, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales', 'actioned', 1, '', NULL, NULL),
(995, 12, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business', 'actioned', 1, '', NULL, NULL),
(996, 13, 'add-apple-pay', 'Add Apple Pay', '/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments', 'actioned', 1, '', NULL, NULL),
(997, 13, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay', 'actioned', 1, '', NULL, NULL),
(998, 14, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales', 'actioned', 1, '', NULL, NULL),
(999, 15, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business', 'actioned', 1, '', NULL, NULL),
(1000, 16, 'optimizing-the-checkout-flow', 'Learn more', 'https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(1001, 17, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(1002, 18, 'qualitative-feedback-from-new-users', 'Share feedback', 'https://automattic.survey.fm/wc-pay-new', 'actioned', 1, '', NULL, NULL),
(1003, 19, 'share-feedback', 'Share feedback', 'http://automattic.survey.fm/paypal-feedback', 'unactioned', 1, '', NULL, NULL),
(1004, 20, 'learn-more', 'Learn about Instant Deposits eligibility', 'https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits', 'actioned', 1, '', NULL, NULL),
(1005, 21, 'get-started', 'Get started', 'https://woocommerce.com/products/google-listings-and-ads', 'actioned', 1, '', NULL, NULL),
(1006, 22, 'update-wc-subscriptions-3-0-15', 'View latest version', 'http://yair:8888/wp-admin/admin.php?page=wc-admin&page=wc-addons&section=helper', 'actioned', 1, '', NULL, NULL),
(1007, 23, 'update-wc-core-5-4-0', 'How to update WooCommerce', 'https://docs.woocommerce.com/document/how-to-update-woocommerce/', 'actioned', 1, '', NULL, NULL),
(1008, 26, 'get-woo-commerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(1009, 27, 'get-woocommerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(1010, 28, 'ppxo-pps-install-paypal-payments-1', 'View upgrade guide', 'https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/', 'actioned', 1, '', NULL, NULL),
(1011, 29, 'ppxo-pps-install-paypal-payments-2', 'View upgrade guide', 'https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/', 'actioned', 1, '', NULL, NULL),
(1012, 30, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(1013, 30, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(1014, 31, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(1015, 31, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(1016, 32, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(1017, 32, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(1018, 33, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(1019, 33, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_category_lookup`
--

CREATE TABLE `fmn_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_category_lookup`
--

INSERT INTO `fmn_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_customer_lookup`
--

CREATE TABLE `fmn_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_download_log`
--

CREATE TABLE `fmn_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_coupon_lookup`
--

CREATE TABLE `fmn_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_product_lookup`
--

CREATE TABLE `fmn_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_stats`
--

CREATE TABLE `fmn_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_tax_lookup`
--

CREATE TABLE `fmn_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_product_meta_lookup`
--

CREATE TABLE `fmn_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_product_meta_lookup`
--

INSERT INTO `fmn_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`, `tax_status`, `tax_class`) VALUES
(125, '', 0, 0, '100.0000', '100.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(137, '', 0, 0, '140.0000', '140.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(138, '', 0, 0, '77.0000', '77.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(139, '', 0, 0, '200.0000', '200.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(140, '', 0, 0, '180.0000', '180.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(141, '', 0, 0, '250.0000', '250.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(142, '', 0, 0, '160.0000', '160.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(143, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(144, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(145, '', 0, 0, '150.0000', '150.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(150, '', 0, 0, '270.0000', '270.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(151, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(152, '', 0, 0, '150.0000', '150.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(153, '', 0, 0, '200.0000', '200.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(154, '', 0, 0, '180.0000', '180.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', '');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_reserved_stock`
--

CREATE TABLE `fmn_wc_reserved_stock` (
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `stock_quantity` double NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_tax_rate_classes`
--

CREATE TABLE `fmn_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_tax_rate_classes`
--

INSERT INTO `fmn_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_webhooks`
--

CREATE TABLE `fmn_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_api_keys`
--

CREATE TABLE `fmn_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_attribute_taxonomies`
--

CREATE TABLE `fmn_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `fmn_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_log`
--

CREATE TABLE `fmn_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_order_itemmeta`
--

CREATE TABLE `fmn_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_order_items`
--

CREATE TABLE `fmn_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_payment_tokenmeta`
--

CREATE TABLE `fmn_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_payment_tokens`
--

CREATE TABLE `fmn_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_sessions`
--

CREATE TABLE `fmn_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_woocommerce_sessions`
--

INSERT INTO `fmn_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(1, '1', 'a:9:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:419:\"a:1:{s:32:\"3def184ad8f4755ff269862ea77393dd\";a:11:{s:3:\"key\";s:32:\"3def184ad8f4755ff269862ea77393dd\";s:10:\"product_id\";i:125;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:333;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:33300;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:33300;s:8:\"line_tax\";i:0;}}\";s:8:\"customer\";s:729:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2021-07-23T11:58:18+03:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"IL\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"IL\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:15:\"maxf@leos.co.il\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:10:\"wc_notices\";N;s:21:\"chosen_payment_method\";s:0:\"\";}', 1627244811);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zones`
--

CREATE TABLE `fmn_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zone_locations`
--

CREATE TABLE `fmn_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zone_methods`
--

CREATE TABLE `fmn_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_tax_rates`
--

CREATE TABLE `fmn_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_tax_rate_locations`
--

CREATE TABLE `fmn_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yith_wcwl`
--

CREATE TABLE `fmn_yith_wcwl` (
  `ID` bigint(20) NOT NULL,
  `prod_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `wishlist_id` bigint(20) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `original_price` decimal(9,3) DEFAULT NULL,
  `original_currency` char(3) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `on_sale` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yith_wcwl_lists`
--

CREATE TABLE `fmn_yith_wcwl_lists` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `wishlist_slug` varchar(200) NOT NULL,
  `wishlist_name` text,
  `wishlist_token` varchar(64) NOT NULL,
  `wishlist_privacy` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fmn_yith_wcwl_lists`
--

INSERT INTO `fmn_yith_wcwl_lists` (`ID`, `user_id`, `session_id`, `wishlist_slug`, `wishlist_name`, `wishlist_token`, `wishlist_privacy`, `is_default`, `dateadded`, `expiration`) VALUES
(1, 1, NULL, '', '', 'D9UFNGFMUYV7', 0, 1, '2021-07-23 04:11:15', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fmn_actionscheduler_actions`
--
ALTER TABLE `fmn_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`),
  ADD KEY `claim_id_status_scheduled_date_gmt` (`claim_id`,`status`,`scheduled_date_gmt`);

--
-- Indexes for table `fmn_actionscheduler_claims`
--
ALTER TABLE `fmn_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `fmn_actionscheduler_groups`
--
ALTER TABLE `fmn_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `fmn_actionscheduler_logs`
--
ALTER TABLE `fmn_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `fmn_links`
--
ALTER TABLE `fmn_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `fmn_options`
--
ALTER TABLE `fmn_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `fmn_term_relationships`
--
ALTER TABLE `fmn_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_users`
--
ALTER TABLE `fmn_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `fmn_wc_admin_notes`
--
ALTER TABLE `fmn_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `fmn_wc_admin_note_actions`
--
ALTER TABLE `fmn_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `fmn_wc_category_lookup`
--
ALTER TABLE `fmn_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `fmn_wc_customer_lookup`
--
ALTER TABLE `fmn_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `fmn_wc_order_coupon_lookup`
--
ALTER TABLE `fmn_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_order_product_lookup`
--
ALTER TABLE `fmn_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_order_stats`
--
ALTER TABLE `fmn_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `fmn_wc_order_tax_lookup`
--
ALTER TABLE `fmn_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_product_meta_lookup`
--
ALTER TABLE `fmn_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `fmn_wc_reserved_stock`
--
ALTER TABLE `fmn_wc_reserved_stock`
  ADD PRIMARY KEY (`order_id`,`product_id`);

--
-- Indexes for table `fmn_wc_tax_rate_classes`
--
ALTER TABLE `fmn_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `fmn_wc_webhooks`
--
ALTER TABLE `fmn_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fmn_woocommerce_api_keys`
--
ALTER TABLE `fmn_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `fmn_woocommerce_attribute_taxonomies`
--
ALTER TABLE `fmn_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `fmn_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `fmn_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `fmn_woocommerce_log`
--
ALTER TABLE `fmn_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `fmn_woocommerce_order_itemmeta`
--
ALTER TABLE `fmn_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `fmn_woocommerce_order_items`
--
ALTER TABLE `fmn_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `fmn_woocommerce_payment_tokenmeta`
--
ALTER TABLE `fmn_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `fmn_woocommerce_payment_tokens`
--
ALTER TABLE `fmn_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fmn_woocommerce_sessions`
--
ALTER TABLE `fmn_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `fmn_woocommerce_shipping_zones`
--
ALTER TABLE `fmn_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `fmn_woocommerce_shipping_zone_locations`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `fmn_woocommerce_shipping_zone_methods`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `fmn_woocommerce_tax_rates`
--
ALTER TABLE `fmn_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `fmn_woocommerce_tax_rate_locations`
--
ALTER TABLE `fmn_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `fmn_yith_wcwl`
--
ALTER TABLE `fmn_yith_wcwl`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Indexes for table `fmn_yith_wcwl_lists`
--
ALTER TABLE `fmn_yith_wcwl_lists`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `wishlist_token` (`wishlist_token`),
  ADD KEY `wishlist_slug` (`wishlist_slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_actions`
--
ALTER TABLE `fmn_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_claims`
--
ALTER TABLE `fmn_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_groups`
--
ALTER TABLE `fmn_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_logs`
--
ALTER TABLE `fmn_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_links`
--
ALTER TABLE `fmn_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_options`
--
ALTER TABLE `fmn_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1727;

--
-- AUTO_INCREMENT for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1644;

--
-- AUTO_INCREMENT for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;

--
-- AUTO_INCREMENT for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `fmn_users`
--
ALTER TABLE `fmn_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_wc_admin_notes`
--
ALTER TABLE `fmn_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `fmn_wc_admin_note_actions`
--
ALTER TABLE `fmn_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1020;

--
-- AUTO_INCREMENT for table `fmn_wc_customer_lookup`
--
ALTER TABLE `fmn_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_wc_tax_rate_classes`
--
ALTER TABLE `fmn_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_wc_webhooks`
--
ALTER TABLE `fmn_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_api_keys`
--
ALTER TABLE `fmn_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_attribute_taxonomies`
--
ALTER TABLE `fmn_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `fmn_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_log`
--
ALTER TABLE `fmn_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_order_itemmeta`
--
ALTER TABLE `fmn_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_order_items`
--
ALTER TABLE `fmn_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_payment_tokenmeta`
--
ALTER TABLE `fmn_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_payment_tokens`
--
ALTER TABLE `fmn_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_sessions`
--
ALTER TABLE `fmn_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zones`
--
ALTER TABLE `fmn_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zone_locations`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zone_methods`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_tax_rates`
--
ALTER TABLE `fmn_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_tax_rate_locations`
--
ALTER TABLE `fmn_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_yith_wcwl`
--
ALTER TABLE `fmn_yith_wcwl`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fmn_yith_wcwl_lists`
--
ALTER TABLE `fmn_yith_wcwl_lists`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  ADD CONSTRAINT `fk_fmn_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `fmn_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
