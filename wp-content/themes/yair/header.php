<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="drop-menu">
		<nav class="drop-nav">
			<?php getMenu('dropdown-header-menu', '2', '', ''); ?>
		</nav>
	</div>
    <div class="container">
		<div class="row align-items-center">
			<div class="col header-col-menus">
				<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<nav id="MainNav" class="h-100">
					<div id="MobNavBtn">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
				</nav>
			</div>
			<?php if ($logo = opt('logo')) : ?>
				<div class="col-md-2 col-5 header-logo-col">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				</div>
			<?php endif; ?>
			<div class="col header-col-links">
				<div class="search-trigger">
					<img src="<?= ICONS ?>search.png" alt="search-trigger">
				</div>
				<?php if(defined( 'YITH_WCWL' )): ?>
					<a href="<?= get_permalink(get_option('yith_wcwl_wishlist_page_id')); ?>" class="header-btn">
						<?= svg_simple(THEMEPATH . '/assets/iconsall/heart.svg') ?>
						<span class="count-circle" id="yith-w-count">
							<?= esc_html(yith_wcwl_count_all_products()) ?>
						</span>
					</a>
				<?php endif; ?>
				<a href="<?= wc_get_cart_url(); ?>" class="header-btn">
					<img src="<?= ICONS ?>basket.png" alt="cart">
					<span class="count-circle" id="cart-count">
						<?= WC()->cart->get_cart_contents_count(); ?>
					</span>
				</a>
			</div>
		</div>
	</div>
</header>

<section class="pop-search">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-search">
					<span class="close-search">
						<?= svg_simple(ICONS.'close.svg'); ?>
					</span>
					<?php if ($title_search = opt('search_title')) : ?>
						<h3 class="search-title"><?= $title_search; ?></h3>
					<?php endif;
					get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
