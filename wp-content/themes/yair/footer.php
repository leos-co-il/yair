<?php $facebook = opt('facebook'); ?>
<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top">
			<h4 class="foo-form-subtitle mt-2">למעלה</h4>
		</a>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row justify-content-between">
						<div class="col-lg col-sm-6 foo-menu main-footer-menu">
							<h3 class="foo-title">ניווט</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-menu', '2', '',
										'main_menu h-100 text-right'); ?>
							</div>
						</div>
						<div class="col-lg col-sm-6 col-12 foo-menu">
							<h3 class="foo-title"><?= opt('foo_title'); ?></h3>
							<h4 class="foo-form-subtitle"><?= opt('foo_subtitle'); ?></h4>
							<?php getForm('6'); ?>
						</div>
						<?php if ($facebook) : ?>
							<div class="col-lg col-sm-6 facebook-widget foo-menu d-flex flex-column">
								<h3 class="foo-title">עשו לנו לייק</h3>
								<div class="menu-border-top">
									<iframe src="https://www.facebook.com/plugins/page.php?href=<?= $facebook; ?>&tabs=timeline&width=300px&height=200px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
											width="250px" height="150px" style="border:none;overflow:hidden" scrolling="no"
											frameborder="0" allowTransparency="true" allow="encrypted-media">
									</iframe>
								</div>
							</div>
						<?php endif;
						if ($logo = opt('logo')) : ?>
							<div class="col-lg col-sm-6 col-12 foo-menu">
								<a href="/" class="logo">
									<img src="<?= $logo['url'] ?>" alt="logo">
								</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
