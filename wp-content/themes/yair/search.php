<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block mb-5">
	<div class="container pt-5">
		<?php
		$s = get_search_query();
		$args = array(
			's' => $s
		);
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) { ?>
		<h4 class="base-title-red my-3"><?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?></h4>
		<div class="row justify-content-center">
			<?php  while ( $the_query->have_posts() ) { $the_query->the_post(); $hey = 1; $type = get_post_type();
				$link = get_the_permalink(); ?>
				<div class="col-xl-3 col-lg-4 col-sm-6 col-12 mb-3">
					<div class="card-product more-prod">
						<div class="prod-img-wrap">
							<div class="prod-content-overlay">
								<a class="prod-card-title" href="<?= $link; ?>">
									<?php the_title(); ?>
									<img src="<?= ICONS ?>prod-plus.png" alt="view-product" class="prod-plus mt-2">
								</a>
							</div>
							<a class="card-prod-img"
									<?php if (has_post_thumbnail()) : ?>
										style="background-image: url('<?= postThumb(); ?>')"
									<?php endif; ?>
							   href="<?= $link; ?>">
								<img src="<?= ICONS ?>prod-arrow.png" alt="name-of-product" class="prod-arrow">
							</a>
						</div>
					</div>
				</div>
			<?php }
			} else{ ?>
				<div class="text-center pt-5">
					<h4 class="base-block-title text-center">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
