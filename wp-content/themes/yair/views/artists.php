<?php
/*
Template Name: דף אומנים
*/

get_header();
$fields = get_fields();
$artists = get_terms( [
		'taxonomy' => 'artist',
		'hide_empty' => false,
		'number' => 9,
]);
$artists_all = get_terms( [
		'taxonomy' => 'artist',
		'hide_empty' => false,
]);
?>

<article class="black-back page-body">
	<div class="container mb-4">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-7 col-md-9 col-sm-11 col-12">
				<div class="artists-page-content">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['artists_link_page']) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<a href="<?= $fields['artists_link_page']['url']; ?>" class="be-artist-link">
						<?= (isset($fields['artists_link_page']) || $fields['artists_link_page']['title']) ?
								$fields['artists_link_page']['title'] : 'הצטרפו לאומנים שלנו'; ?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($artists) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11 col-12">
					<div class="row justify-content-center align-items-stretch put-here-artists">
						<?php foreach ($artists as $artist) {
							get_template_part('views/partials/card', 'artist',
									[
											'artist' => $artist,
									]);
						} ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif;
	if (count($artists_all) > 9) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="load-more-artists more-products-style">
						טען עוד אומנים
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_footer(); ?>
