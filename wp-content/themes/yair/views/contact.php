<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$whatsapp = opt('whatsapp');
$mail = opt('mail');
$facebook = opt('facebook');
$instagram = opt('instagram');


?>
<article class="page-body contact-page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="page-content-centered">
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-xl-9 col-md-10 col-12 col-contacts">
				<div class="row align-items-stretch justify-content-center">
					<div class="col-xl-6 contact-info-wrap">
						<?php if ($fields['contact_block_title']) : ?>
							<h3 class="form-offer-title"><?= $fields['contact_block_title']; ?></h3>
						<?php endif; ?>
						<div class="contacts-list-block">
							<ul class="contact-list">
								<?php if ($tel) : ?>
									<li class="contact-item col-auto">
										<div class="contact-info-link">
											<a href="https://api.whatsapp.com/send?phone=<?= $tel; ?>" class="contact-icon-wrap"
											target="_blank">
												<img src="<?= ICONS ?>whatsapp.png">
											</a>
											<span class="contact-title"><?= $tel; ?></span>
											<a href="tel:<?= $tel; ?>" class="contact-icon-wrap mr-3">
												<img src="<?= ICONS ?>tel.png">
											</a>
										</div>
									</li>
								<?php endif;
								if ($mail) : ?>
									<li class="contact-item col-auto">
										<a href="mailto:<?= $mail; ?>" class="contact-info-link">
											<span class="contact-icon-wrap">
												<img src="<?= ICONS ?>mail.png">
											</span>
											<span class="contact-title mail-title"><?= $mail; ?></span>
										</a>
									</li>
								<?php endif;
								if ($facebook) : ?>
									<li class="contact-item col-auto">
										<a href="<?= $facebook; ?>"
										   class="contact-info-link" target="_blank">
											<span class="contact-icon-wrap">
												<img src="<?= ICONS ?>facebook.png">
											</span>
											<?php if ($fields['facebook_name']) : ?>
												<span class="contact-title"><?= $fields['facebook_name']; ?></span>
											<?php endif; ?>
										</a>
									</li>
								<?php endif;
								if ($instagram) : ?>
									<li class="contact-item col-auto">
										<a href="<?= $instagram; ?>"
										   class="contact-info-link" target="_blank">
											<span class="contact-icon-wrap">
												<img src="<?= ICONS ?>instagram.png">
											</span>
											<?php if ($fields['instagram_name']) : ?>
												<span class="contact-title"><?= $fields['instagram_name']; ?></span>
											<?php endif; ?>
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
					<div class="col-xl-6 form-yellow-wrap">
						<?php if ($fields['contact_form_title']) : ?>
							<h3 class="form-offer-title"><?= $fields['contact_form_title']; ?></h3>
						<?php endif;
						getForm('7'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>

