<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
$logo = opt('logo');
?>

<section class="home-main" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="container">
		<?php if ($logo) : ?>
		<div class="row justify-content-center">
			<div class="col-xl-4 col-lg-6 col-8">
				<a href="/" class="logo">
					<img src="<?= $logo['url'] ?>" alt="logo">
				</a>
			</div>
		</div>
		<?php endif;
		if ($fields['home_about_text'] || $fields['home_about_link']) : ?>
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="row justify-content-start">
					<div class="col-lg-8 col-md-10 col-12 d-flex flex-column align-items-start">
						<div class="home-about-text">
							<?= $fields['home_about_text']; ?>
						</div>
						<?php if ($fields['home_about_link']) : ?>
							<a href="<?= $fields['home_about_link']['url']; ?>" class="base-link">
								<?= (isset($fields['home_about_link']) || $fields['home_about_link']['title']) ?
										$fields['home_about_link']['title'] : 'עוד עלינו '; ?>
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>
<section class="home-video-block">
	<?php if ($fields['home_video_back']) : ?>
		<video muted autoplay="autoplay" loop="loop">
			<source src="<?= $fields['home_video_back']['url']; ?>" type="video/mp4">
		</video>
	<?php endif; ?>
	<div class="container video-block-content-container">
		<?php if ($fields['home_video_block_title'] || $fields['home_video_block_text']) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="giant-title"><?= $fields['home_video_block_title'] ; ?></h2>
					<p class="block-text"><?= $fields['home_video_block_text'] ; ?></p>
				</div>
			</div>
		<?php endif;
		if ($fields['home_video_block_img']) : ?>
			<div class="row justify-content-center row-img-video">
				<div class="col-xl-6 col-lg-7 col-sm-9 col-12 wow zoomIn">
					<img src="<?= $fields['home_video_block_img']['url']; ?>" alt="our-work" class="w-100">
				</div>
			</div>
		<?php endif;
		if ($fields['home_video_block_links']) : ?>
			<div class="row justify-content-center">
				<div class="col-xl-9 col-lg-10 col-12">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['home_video_block_links'] as $x => $link) :
							if (isset($link['video_block_link']['url'])) : ?>
							<div class="col-6 wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
								<a href="<?= $link['video_block_link']['url'];?>"
								   class="home-offer-link <?= ($x === 1) ? 'second-link' : ''; ?>">
									<span class="offer-link-text">
										<?php $text_link = ($x === 1) ? 'להעלת קובץ תמונה אישי' : 'לבחירה מהירה של תמונה';
										echo (isset($link['video_block_link']) || $link['video_block_link']['title']) ?
												$link['video_block_link']['title'] : $text_link; ?>
									</span>
								</a>
							</div>
						<?php endif; endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<section class="cats-home-block black-back">
	<div class="container">
		<?php if ($fields['home_cats_title'] || $fields['home_cats_text']) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title"><?= $fields['home_cats_title'] ; ?></h2>
					<p class="block-text"><?= $fields['home_cats_text'] ; ?></p>
				</div>
			</div>
		<?php endif;
		if ($fields['home_cats_chosen']) : $last = array_pop($fields['home_cats_chosen']); ?>
			<div class="row justify-content-center">
				<?php get_template_part('views/partials/layout', 'categories', [
						'cats' => $fields['home_cats_chosen'],
				]); ?>
				<div class="col-12 mt-sm-2">
					<div class="row">
						<div class="col-lg-8 col-12 col-home-single">
							<a class="cat-item-img" <?php $thumbnail_id = get_term_meta( $last->term_id, 'thumbnail_id', true );
							$image = wp_get_attachment_url( $thumbnail_id );
							if ($image) : ?>
								style="background-image: url('<?= $image; ?>')"
							<?php endif; ?> href="<?= get_category_link($last); ?>">
									<span class="cat-name-wrap">
										<?= $last->name; ?>
									</span>
							</a>
						</div>
						<?php if ($fields['home_cats_link']) : ?>
							<div class="col-lg-4 cat-link-col">
								<a href="<?= $fields['home_cats_link']['url']; ?>" class="home-cats-link">
									<?= (isset($fields['home_cats_link']) || $fields['home_cats_link']['title']) ?
											$fields['home_cats_link']['title'] : 'לכל הקטגוריות'; ?>
								</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php get_template_part('views/partials/repeat', 'process'); ?>
</section>
<?php if ($fields['home_offer_title'] || $fields['home_offer_text'] || $fields['home_offer_link']) : ?>
	<section class="home-offer-block">
		<div class="container">
			<div class="row align-items-center justify-content-xl-start justify-content-center">
				<div class="col-xl col-lg-4 col-md-6 col-8">
					<?php if ($logo) : ?>
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					<?php endif; ?>
				</div>
				<div class="col-xl-6 col-12 offer-block-col">
					<h2 class="giant-title"><?= $fields['home_video_block_title'] ; ?></h2>
					<p class="block-text"><?= $fields['home_video_block_text'] ; ?></p>
					<?php if ($fields['home_offer_link']) : ?>
						<a href="<?= $fields['home_offer_link']['url']; ?>" class="base-link">
							<?= (isset($fields['home_offer_link']) || $fields['home_offer_link']['title']) ?
									$fields['home_offer_link']['title'] : 'לבחירת תמונה לחצו'; ?>
						</a>
					<?php endif; ?>
				</div>
				<div class="col"></div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) : ?>
	<section class="base-slider-block black-back">
		<?php if ($fields['slider_img']) : ?>
			<div class="slider-image-wrap">
				 <img src="<?= $fields['slider_img']['url']; ?>')">
			</div>
		<?php endif; ?>
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-8 col-12">
					<div class="slider-text-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($fields['single_slider_seo'] as $content) : ?>
								<div>
									<div class="home-about-text slider-text-output">
										<?= $content['content']; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_prods']) : ?>
	<section class="products-slider-block black-back">
		<?php if ($fields['home_prods_title']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="glida-block-title"><?= $fields['home_prods_title']; ?></h2>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="products-slider-wrap">
			<div class="prod-slider" dir="rtl">
				<?php foreach ($fields['home_prods'] as $product) : ?>
					<div>
						<?php get_template_part('views/partials/card', 'product_simple', [
								'product' => $product,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_gallery']) : $last_gallery = array_pop($fields['home_gallery']); ?>
	<section class="home-gallery">
		<div class="container">
			<?php if ($fields['home_gallery_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="glida-block-title"><?= $fields['home_gallery_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-12">
					<div class="col-12 cat-big-col">
						<div class="row align-items-stretch justify-content-center">
							<div class="col-lg-8 col-12 col-3-chunks col-3-chunks-gallery">
								<?php foreach ($fields['home_gallery'] as $item_gallery) : ?>
									<a class="cat-item-img" style="background-image: url('<?= $item_gallery['url']; ?>')"
									   href="<?= $item_gallery['url']; ?>" data-lightbox="homepage-gallery">
									</a>
								<?php endforeach; ?>
							</div>
							<?php if ($last_gallery || $fields['home_gallery_link']) : ?>
								<div class="col-lg-4 col-12 single-chunk gallery-single-chunk">
									<?php if ($last_gallery) : ?>
									<a class="cat-item-img gallery-img" style="background-image: url('<?= $last_gallery['url']; ?>')"
									href="<?= $last_gallery['url']; ?>" data-lightbox="homepage-gallery">
									</a>
									<?php endif;
									if ($fields['home_gallery_link']) : ?>
										<a href="<?= $fields['home_gallery_link']['url']; ?>" class="home-cats-link gallery-link">
											<?= (isset($fields['home_gallery_link']) || $fields['home_gallery_link']['title']) ?
													$fields['home_gallery_link']['title'] : 'לכל הגלריה'; ?>
										</a>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'reviews');
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_desc' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
					'link' => $fields['faq_link']
			]);
endif;
get_footer(); ?>
