<?php
/*
Template Name: מכירה לאומן
*/

get_header();
$fields = get_fields();

?>

<article class="page-body offer-page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-10 col-12">
				<div class="offer-page-content">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-xl-5 col-lg-6 col-md-8 col-11">
				<div class="form-yellow-wrap wow zoomIn" data-wow-delay="0.3s">
					<?php if ($fields['team_form_title']) : ?>
						<h3 class="form-offer-title"><?= $fields['team_form_title']; ?></h3>
					<?php endif;
					getForm('7'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'process');
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_desc' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
					'link' => $fields['faq_link']
			]);
endif;
get_footer(); ?>
