<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$cats = get_terms( [
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'number' => 8,
]);
$cats_ids = [];
foreach ($cats as $cat) {
	$cats_ids[] = $cat->term_id;
}
$cats_other = get_terms( [
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'exclude' => $cats_ids,
]);
$cats_chunks = array_chunk($cats, 4);
?>

<article class="page-body cats-page-body black-back">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="page-content-centered">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<div class="row row-page-cats">
			<?php foreach ($cats_chunks as $num => $chunk) {
				get_template_part('views/partials/layout', 'categories', [
						'cats' => $chunk,
				]);
			}
			get_template_part('views/partials/layout', 'categories_simple', [
					'cats' => $cats_other,
			]); ?>
		</div>
	</div>
</article>


<?php get_footer(); ?>
