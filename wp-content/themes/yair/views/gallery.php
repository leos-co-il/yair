<?php
/*
Template Name: גלריה
*/

get_header();
$fields = get_fields();
$count = '';
?>

<article class="page-body black-back pt-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="page-content-centered">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['single_gallery']) : $images = array_chunk($fields['single_gallery'], 5);
	$count = count($images); ?>
		<div class="gallery-body-output" id="gallery-block">
			<?php foreach ($images as $x => $curr_image) :?>
				<div class="page-home-gallery <?= ($x < 3) ? 'show' : ''; ?>" dir="rtl">
					<?php foreach ($curr_image as $img_link) : ?>
						<div class="home-gallery-item">
							<a class="bg-img img-cover gallery-image" href="<?= $img_link['url']; ?>" data-lightbox="item-of-gallery"
							   style="background-image: url('<?= $img_link['url']; ?>')">
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif;
	if ($count > 3) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<a class="more-products-style load-more" id="load-more-items-gallery">
						<span>טען עוד תמונות</span>
					</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_footer(); ?>
