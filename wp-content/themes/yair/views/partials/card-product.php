<?php if (isset($args['product'])) : $prod_item = $args['product'];
	$product_all = wc_get_product($prod_item);
	$link = get_permalink($product_all->get_id());
	?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 mb-3">
		<div class="card-product more-prod" data-id="<?= $product_all->get_id(); ?>">
			<div class="prod-img-wrap">
				<?php echo do_shortcode( '[yith_wcwl_add_to_wishlist product_id="'.$product_all->get_id().'"]' ) ?>
				<div class="prod-content-overlay">
					<a class="prod-card-title" href="<?= $link; ?>">
						<?= $product_all->get_name(); ?>
						<img src="<?= ICONS ?>prod-plus.png" alt="view-product" class="prod-plus mt-2">
					</a>
				</div>
				<a class="card-prod-img"
					<?php if (has_post_thumbnail($prod_item)) : ?>
						style="background-image: url('<?= postThumb($prod_item); ?>')"
					<?php endif; ?>
				   href="<?= $link; ?>">
					<img src="<?= ICONS ?>prod-arrow.png" alt="name-of-product" class="prod-arrow">
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
