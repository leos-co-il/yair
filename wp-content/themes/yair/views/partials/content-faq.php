<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title">
						<?= (isset($args['block_title']) && $args['block_title']) ? $args['block_title'] : 'שאלות נפוצות'; ?>
					</h2>
					<?php if (isset($args['block_desc']) && $args['block_desc']) : ?>
						<p class="block-text"><?= $args['block_desc']; ?></p>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-8 col-lg-10 col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="btn question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<?= $item['faq_question']; ?>
										<span class="arrow-top"></span>
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="answer-body base-output base-output-white">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if (isset($args['link']) && $args['link']) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $args['link']['url']; ?>" class="base-link">
							<?= (isset($args['link']) || $args['link']['title']) ?
								$args['link']['title'] : 'לכל השאלות'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
