<?php if (isset($args['cats']) && $args['cats']) : ?>
	<div class="col-12 simple-big-col-cat">
		<div class="row align-items-stretch justify-content-center">
			<?php foreach ($args['cats'] as $cat) : ?>
				<div class="col-lg-4 col-md-6 col-12 simple-cat-col">
					<a class="cat-item-img" <?php $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
					$image = wp_get_attachment_url( $thumbnail_id );
					if ($image) : ?>
						style="background-image: url('<?= $image; ?>')"
					<?php endif; ?> href="<?= $link = get_category_link($cat); ?>">
						<span class="overlay-cat" href="<?= $link; ?>">
							<span class="cat-name-overlay"><?= $cat->name; ?></span>
						</span>
						<span class="cat-name-wrap">
							<?= $cat->name; ?>
						</span>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
