<?php if ($process = opt('process_item')) : ?>
	<div class="process-block">
		<div class="container">
			<?php if ($title = opt('process_block_title')) : ?>
				<div class="row justify-content-center">
					<div class="col-auto mb-5">
						<h2 class="giant-title"><?= $title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($process as $key => $step) : ?>
					<div class="col-sm-3 col-6 benefit-col mb-3">
						<div class="step-item wow zoomInUp" data-wow-delay="0.<?= $key; ?>s">
							<div class="step-icon">
								<?php if ($step['process_icon']) : ?>
									<img src="<?= $step['process_icon']['url']; ?>">
								<?php endif; ?>
								<span class="step-number">
									<?= ($key + 1).'.'; ?>
								</span>
							</div>
							<h3 class="base-title">
								<?= $step['process_title']; ?>
							</h3>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
