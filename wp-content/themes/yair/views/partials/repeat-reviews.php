<?php if ($reviews = opt('review_item')) : ?>
<section class="reviews-block">
	<div class="container">
		<?php if ($title = opt('reviews_block_title')) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="glida-block-title"><?= $title; ?></h2>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center">
			<div class="col-xl-7 col-lg-8 col-md-10 col-11">
				<div class="base-slider" dir="rtl">
					<?php foreach ($reviews as $x => $review) : ?>
						<div class="review-slide">
							<div class="review-item">
								<img src="<?= ICONS ?>quote-top.png" alt="quotes" class="quote quote-top">
								<img src="<?= ICONS ?>quote-bottom.png" alt="quotes" class="quote quote-bottom">
								<div class="rev-pop-trigger">
									<div class="hidden-review">
										<div class="slider-output">
											<?= $review['review_text']; ?>
										</div>
									</div>
								</div>
								<div class="rev-content">
									<h3 class="review-title"><?= $review['review_name']; ?></h3>
									<div class="review-prev">
										<?= text_preview($review['review_text'], '30'); ?>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif; ?>
