<?php if (isset($args['artist']) && $args['artist']) :
	$link = get_term_link($args['artist']); ?>
	<div class="col-xl-4 col-md-6 col-12 mb-3">
		<div class="artist-card more-artist-card" data-id="<?= $args['artist']->term_id; ?>">
			<div class="card-prod-img artist-image"
					<?php if ($img = get_field('cat_img', $args['artist'])) : ?>
						style="background-image: url('<?= $img['url']; ?>')"
					<?php endif; ?>>
				<div class="artist-card-overlay">
					<a class="prod-card-title" href="<?= $link; ?>">
						<?= $args['artist']->name; ?>
					</a>
				</div>
				<div class="artist-big-overlay">
					<a class="prod-card-title" href="<?= $link; ?>">
						<?= $args['artist']->name; ?>
					</a>
					<p class="small-text">
						<?= text_preview(category_description($args['artist']), '20'); ?>
					</p>
					<a class="base-link artist-link" href="<?= $link; ?>">
						ליצירות האומן
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
