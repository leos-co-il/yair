<?php if (isset($args['cats']) && $args['cats']) :
$chunked = array_chunk($args['cats'], 4);
	foreach ($chunked as $chunk) : $last = array_pop($chunk); ?>
		<div class="col-12 cat-big-col">
			<div class="row align-items-stretch justify-content-center">
				<div class="col-lg-8 col-12 col-3-chunks">
					<?php foreach ($chunk as $cat) : ?>
						<a class="cat-item-img" <?php $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
						$image = wp_get_attachment_url( $thumbnail_id );
						if ($image) : ?>
							style="background-image: url('<?= $image; ?>')"
						<?php endif; ?> href="<?= $link = get_category_link($cat); ?>">
							<span class="overlay-cat" href="<?= $link; ?>">
								<span class="cat-name-overlay"><?= $cat->name; ?></span>
							</span>
						<span class="cat-name-wrap">
							<?= $cat->name; ?>
						</span>
						</a>
					<?php endforeach; ?>
				</div>
				<?php if ($last) : ?>
				<div class="col-lg-4 col-12 single-chunk">
					<a class="cat-item-img" <?php $thumbnail_id = get_term_meta( $last->term_id, 'thumbnail_id', true );
					$image = wp_get_attachment_url( $thumbnail_id );
					if ($image) : ?>
					   style="background-image: url('<?= $image; ?>')"
					<?php endif; ?> href="<?= $link_last = get_category_link($last); ?>">
						<span class="overlay-cat" href="<?= $link_last; ?>">
							<span class="cat-name-overlay"><?= $last->name; ?></span>
						</span>
						<span class="cat-name-wrap">
							<?= $last->name; ?>
						</span>
					</a>
				</div>
				<?php endif; ?>
			</div>
		</div>
<?php endforeach; endif; ?>
