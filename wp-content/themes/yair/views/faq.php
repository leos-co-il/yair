<?php
/*
Template Name: שאלות ותשובות
*/

get_header();
$fields = get_fields();

?>

<article class="page-body faq">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="faq-content-centered">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['page_faq_block']) : foreach ($fields['page_faq_block'] as $x => $faq_part) : ?>
			<div class="row faq-block-item">
				<?php if ($faq_part['faq_page_block_title']) : ?>
					<div class="col-auto">
						<h3 class="faq-block-title"><?= $faq_part['faq_page_block_title']; ?></h3>
					</div>
				<?php endif; ?>
				<div class="col-12">
					<?php if ($faq_part['faq_item_page']) : ?>
						<div id="accordion-<?= $x; ?>" class="accordion-faq">
							<?php foreach ($faq_part['faq_item_page'] as $num => $item) : ?>
								<div class="card question-card">
									<div class="question-header" id="heading_<?= $x.$num; ?>">
										<button class="btn question-title" data-toggle="collapse"
												data-target="#faqChild<?= $x.$num; ?>"
												aria-expanded="false" aria-controls="collapseOne">
											<?= $item['faq_question']; ?>
											<span class="arrow-top"></span>
										</button>
										<div id="faqChild<?= $x.$num; ?>" class="collapse faq-item"
											 aria-labelledby="heading_<?= $x.$num; ?>" data-parent="#accordion-<?= $x; ?>">
											<div class="answer-body base-output">
												<?= $item['faq_answer']; ?>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; endif; ?>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'process');
get_template_part('views/partials/repeat', 'reviews');
get_footer(); ?>
