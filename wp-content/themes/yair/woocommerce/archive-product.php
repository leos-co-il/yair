<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
$post_link = get_the_permalink();
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
//do_action( 'woocommerce_before_main_content' );
global $wp_query;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_pages = $wp_query->max_num_pages;
?>
<article class="page-body black-back">
	<div class="container pt-4">
		<div class="row justify-content-center">
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<div class="col-12">
					<h1 class="woocommerce-products-header__title block-title">
						<?php woocommerce_page_title(); ?>
					</h1>
				</div>
			<?php endif; ?>
			<div class="col-12">
				<div class="page-content-centered">
					<?php
					/**
					 * Hook: woocommerce_archive_description.
					 *
					 * @hooked woocommerce_taxonomy_archive_description - 10
					 * @hooked woocommerce_product_archive_description - 10
					 */
					do_action( 'woocommerce_archive_description' );

					?>
				</div>
			</div>
			<div class="col-12">
				<div class="shop-nav-wrap">
					<?php
//					do_action('woocommerce_before_main_content');
					do_action('woocommerce_before_shop_loop');
					?>
					<div class="share-block">
						<div class="share-trigger share-link share-archive">
							<img src="<?= ICONS ?>share.png" alt="share">
							<span>שתפו</span>
						</div>
						<div class="social-links">
							<a href="mailto:?&subject=&body=<?= $post_link; ?>" target="_blank"
							   class="share-link share-mail" role="link">
								<img src="<?= ICONS ?>share-mail.png" alt="share-mail">
							</a>
							<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
							   class="share-link share-link-facebook">
								<img src="<?= ICONS ?>share-facebook.png" alt="share-facebook">
							</a>
							<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $post_link; ?>" target="_blank"
							   class="share-link" role="link">
								<i class="fab fa-whatsapp"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-parent">
				<?php if (woocommerce_product_loop()): ?>
					<div class="row justify-content-center put-here-prods">
						<?php if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();
								/**
								 * Hook: woocommerce_shop_loop.
								 */
								do_action( 'woocommerce_shop_loop' );
								get_template_part('views/partials/card', 'product',
										[
												'product' => get_the_ID(),
										]);
							}
						}
						wp_reset_postdata(); ?>
					</div>
					<?php if( $paged < $max_pages ) : ?>
						<div class="row justify-content-center mt-4 mb-5 ">
							<div class="col-auto">
								<div id="loadmore" data-load="טען עוד מוצרים" data-loading="טוען">
									<a href="#" data-max_pages="' . $max_pages . '" data-paged="' . $paged . '"
									   class="more-products-style more-link">
										טען עוד מוצרים
									</a>
								</div>
							</div>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<?php

					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
					?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer( 'shop' );
