<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
$post_link = get_the_permalink();
if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$item_terms = wp_get_post_terms($product->get_id(), [
		'artist'
]);
?>
<div id="product-<?php the_ID(); ?>" class="black-back pt-4 product-page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-11 col-12">
				<div class="row justify-content-xl-between justify-content-center">
					<div class="col-12 small-hidden-title">
						<?php the_title( '<h1 class="product_title entry-title">', '</h1>' ); ?>
					</div>
					<div class="col-xl-5 col-lg-6 col-12 mb-lg-0 mb-4 order-third">
						<?php
						the_title( '<h1 class="product_title entry-title entry-hidden-title">', '</h1>' );
						if (isset($item_terms['0'])) : ?>
						<h3 class="artist-title">
							<?= $item_terms['0']->name; ?>
						</h3>
						<?php endif;
						/**
						 * Hook: woocommerce_single_product_summary.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						do_action( 'woocommerce_single_product_summary' ); ?>
						<div class="share-block">
							<h3 class="share-text">
								שתפו את העמוד:</h3>
							<div class="share-trigger share-link">
								<img src="<?= ICONS ?>share.png" alt="share">
							</div>
							<div class="social-links">
								<a href="mailto:?&subject=&body=<?= $post_link; ?>" target="_blank"
								   class="share-link share-mail" role="link">
									<img src="<?= ICONS ?>share-mail.png" alt="share-mail">
								</a>
								<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
								   class="share-link">
									<img src="<?= ICONS ?>share-facebook.png" alt="share-facebook">
								</a>
								<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $post_link; ?>" target="_blank"
								   class="share-link" role="link">
									<i class="fab fa-whatsapp"></i>
								</a>
							</div>
						</div>
<!--						--><?php //do_action('woocommerce_single_product_cart_custom'); ?>

						<?php if ( ! $product->is_purchasable() ) {
							return;
						}
						echo wc_get_stock_html( $product ); // WPCS: XSS ok.

						if ( $product->is_in_stock() ) : ?>

							<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
							<h4 class="price-former-title">הכנס פרמטרים לחישוב מחיר </h4>
							<div class="inputs-custom-wrapper">
								<input type="number" placeholder="גובה המקום לציור :" class="input-count" id="input-count-1">
								<input type="number" placeholder="רוחב המקום לציור:" class="input-count" id="input-count-2">
							</div>
							<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
								<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

										<?php
										do_action( 'woocommerce_before_add_to_cart_quantity' );

										woocommerce_quantity_input(
												array(
														'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
														'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
														'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
												)
										);

										do_action( 'woocommerce_after_add_to_cart_quantity' );
								$id = esc_attr( $product->get_id() );
										?>
								<h4 class="final-price-title">מחיר מחושב לורם</h4>
								<div class="d-flex align-items-center">
									<div class="price-final-sale final-price-title ml-3" data-price_sale="<?= $product->get_sale_price(); ?>">

									</div>
									<div class="price-final final-price-title" data-price="<?= $product->get_regular_price(); ?>">

									</div>
								</div>
								<span class="buttons-wrapper-product">
									<a class="buy-now-custom buy-now-style" data-url="<?= site_url(); ?>"
									data-id="<?= $id; ?>">
									קנה עכשיו
									</a>
								<button type="submit" name="add-to-cart" value="<?= $id; ?>" class="single_add_to_cart_button add-to-cart-custom">
									הוסיפו לסל
								</button>
								</span>

								<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
							</form>

							<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

						<?php endif; ?>
					</div>
					<div class="col-lg-6 col-12 order-second">
						<?php
						/**
						 * Hook: woocommerce_before_single_product_summary.
						 *
						 * @hooked woocommerce_show_product_sale_flash - 10
						 * @hooked woocommerce_show_product_images - 20
						 */
						do_action( 'woocommerce_before_single_product_summary' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
/**
 * Hook: woocommerce_after_single_product_summary.
 *
 * @hooked woocommerce_output_product_data_tabs - 10
 * @hooked woocommerce_upsell_display - 15
 * @hooked woocommerce_output_related_products - 20
 */
do_action( 'woocommerce_after_single_product_summary' );
?>
<?php do_action( 'woocommerce_after_single_product' ); ?>
