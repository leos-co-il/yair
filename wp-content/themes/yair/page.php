<?php

the_post();
get_header();
$fields = get_fields();

?>
<div class="container">
	<div class="row">
		<div class="col">
			<?php the_content(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
