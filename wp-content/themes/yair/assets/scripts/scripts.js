(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	//Load more items in gallery
	var sizeLi = $('#gallery-block .page-home-gallery').size();
	var x = 1;
	$('#load-more-items-gallery').click(function () {
		x = (x + 3 <= sizeLi) ? x + 3 : sizeLi;
		if (x === sizeLi) {
			$('#load-more-items-gallery').addClass('hide');
		}
		$('#gallery-block .page-home-gallery:lt('+x+')').addClass('show');
	});
	function intToFloat(num, decPlaces) { return num.toFixed(decPlaces); }
	$('.input-count').on('change keyup paste', function() {
		var ret = parseInt($('#input-count-1').val()) * parseInt($('#input-count-2').val() || '0');
		var sale = $('.price-final-sale');
		var priceSale = sale.data('price_sale');
		var final = $('.price-final');
		var priceFinal = final.data('price');
		var priceReg = intToFloat(ret * priceFinal, 2);
		var priceSaleTotal = intToFloat(ret * priceSale, 2);
		$('.qty').val(ret);
		sale.val(priceSaleTotal);
		sale.html(priceSaleTotal);
		final.val(priceReg);
		final.html(priceReg);
		var buyLink = $('.buy-now-custom');
		var siteUrl = buyLink.data('url');
		var prodId = buyLink.data('id');
		var finalLink = siteUrl+'/checkout/?add-to-cart='+prodId+'&quantity='+ret;
		buyLink.attr('href', finalLink);
		console.log(buyLink);
	});
	$( document ).ready(function() {
		$('.add-to-cart-btn').click(function (e) {
			e.preventDefault();

			var cartEl = $('.cart');
			var btn = $(this);
			btn.addClass('loading');
			$(this).addClass('adding-cart');
			var productId = $(this).data('id');
			var quant = $('.final-quantity').data('quantity');
			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'add_product_to_cart',
					product_id: productId,
					quantity: quant,
				},

				success: function (results) {
					btn.removeClass('loading').addClass('clicked');
					cartEl.html(results);
					// update_cart_count();
				}
			});
		});
		$('.share-block').hover(function(){ // задаем функцию при наведении курсора на элемент
			$(this).children('.social-links').addClass('show');
		}, function(){ // задаем функцию, которая срабатывает, когда указатель выходит из элемента
			$(this).children('.social-links').removeClass('show');
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$('.search-trigger').click(function () {
			$('.pop-search').addClass('show-search');
			$('.float-search').addClass('show-float-search');
		});
		$('.close-search').click(function () {
			$('.pop-search').removeClass('show-search');
			$('.float-search').removeClass('show-float-search');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		//Gallery
		$('.page-home-gallery').each(function(i, obj) {
			var padNum = 50 * (i + 1);
			var pad = padNum+'px';
			$(obj).slick ({
				rtl:true,
				slidesToShow: 3,
				pauseOnHover:false,
				draggable:true,
				autoplay:true,
				dots:false,
				focusOnSelect:true,
				centerMode: true,
				centerPadding: pad,
				fade:false,
				speed: 5000,
				autoplaySpeed: 3000,
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 2,
							centerPadding: pad,
						}
					},
					{
						breakpoint: 576,
						settings: {
							slidesToShow: 1,
							centerPadding: pad,
						}
					},
				]
			});
		});
		$('.prod-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
			show.parent().children('.question-title').addClass('active-faq');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
			collapsed.parent().children('.question-title').removeClass('active-faq');
		});
		$('.accordion-faq').each(function(i, obj) {
			$(obj).on('shown.bs.collapse', function () {
				var show = $( '.show' );
				show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
				show.parent().children('.question-title').addClass('active-faq');
			});
			$(obj).on('hidden.bs.collapse', function () {
				var collapsed = $( '.collapse' );
				collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
				collapsed.parent().children('.question-title').removeClass('active-faq');
			});
		});

		function update_cart_count(){
			var count = $('#cart-count');
			count.html('<i class="far fa-smile-beam fa-spin" style="color: #fff"></i>');

			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'update_cart_count',
				},
				success: function (results) {
					count.html(results/10);
				}
			});

		}
	});

	$(document).on('added_to_wishlist removed_from_wishlist', function () {
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'post',
			dataType: "json",
			data: {
				action: 'yith_wcwl_update_wishlist_count',
			},
			success: function (data) {
				$('#yith-w-count').html(data.count);
			}
		});
	});
	//More products
	var button = $( '#loadmore a' ),
		paged = button.data( 'paged' ),
		maxPages = button.data( 'max_pages' );
	var textLoad = button.data('loading');
	var textLoadMore = button.data('load');

	button.click( function( event ) {

		event.preventDefault(); // предотвращаем клик по ссылке
		var ids = '';
		$('.more-prod').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		$.ajax({
			type : 'POST',
			dataType: 'json',
			url: '/wp-admin/admin-ajax.php',
			data : {
				paged : paged,
				ids: ids,
				action : 'loadmore'
			},
			beforeSend : function( xhr ) {
				button.text(textLoad);
			},
			success : function( data ){

				paged++; // инкремент номера страницы
				$('.put-here-prods').append(data.html);
				button.text(textLoadMore);

				// если последняя страница, то удаляем кнопку
				if( paged === maxPages ) {
					button.remove();
				}
				if (!data.html) {
					$('.more-link').addClass('hide');
				}

			}

		});

	} );
	//More artists
	$('.load-more-artists').click(function() {
		var ids = '';
		$('.more-artist-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				ids: ids,
				action: 'get_more_function',
			},
			success: function (data) {
				console.log(data);
				if (!data.html) {
					$('.load-more-artists').addClass('hide');
				}
				$('.put-here-artists').append(data.html);
			}
		});
	});
})( jQuery );
